FROM node:8-alpine
RUN mkdir -p /etc/pmi
WORKDIR /etc/pmi
COPY . /etc/pmi/
RUN apk update && apk upgrade && \
    apk add --no-cache bash git openssh
RUN npm install && npm install -g forever && ./bin/pmi --prod
EXPOSE 80
CMD ["forever", "-v", "./src/bin/www"]
