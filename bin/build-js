#!/usr/bin/env node

const fs = require("fs");
const path = require("path");
const browserify = require("browserify");
const envify = require("envify/custom");
const collapse = require("bundle-collapser/plugin");
const UglifyJS = require("uglify-js");

const entry = path.resolve(__dirname, "../src/client/js/main.js");
const output = path.resolve(__dirname, "../src/public/js/main.min.js");

console.log("Starting app.js build. Environment: " + process.env.NODE_ENV);

const browserifyOptions = {
  basedir: process.env.CLIENT_PATH
};

if (process.env.NODE_ENV === "development") {
  browserifyOptions.debug = true;
  browserifyOptions.sourceMaps = true;
  browserifyOptions.cache = {};
  browserifyOptions.packageCache = {};
  browserifyOptions.plugin = [watchify];
}

if (process.env.NODE_ENV === "production") {
  browserifyOptions.plugin = [collapse];
}

const b = browserify(entry, browserifyOptions);

b.transform("babelify", {
  presets: ["es2015", "react"]
});

if (process.env.NODE_ENV === "production") {
  b.transform(
    envify({
      NODE_ENV: process.env.NODE_ENV
    })
  );

  b.transform("uglifyify", {
    global: true
  });
}

function bundle() {
  const begin = Date.now();
  const stream = fs.createWriteStream(output);

  b
    .bundle()
    .on("error", function(err) {
      console.error(err);
      this.emit("end");
    })
    .pipe(stream);

  stream.on("finish", function() {
    console.log("Bundle complete in " + (Date.now() - begin) / 1000 + "secs");

    if (process.env.NODE_ENV === "production") {
      minify(output);
    }
  });
}

// This is the final hardcore uglify-js of the entire file.
function minify(src) {
  console.info("Beginning to minify JS.");
  let data = "";
  const begin = Date.now();
  const stream = fs.createReadStream(output, {
    encoding: "utf8"
  });
  stream.on("error", e => console.error(e));
  stream.on("data", d => (data += d));
  stream.on("close", () => {
    fs.writeFileSync(src, UglifyJS.minify(data).code, {
      encoding: "utf8"
    });
    console.log(
      "Finished uglification in " + (Date.now() - begin) / 1000 + "secs"
    );
  });
}

bundle();

if (process.env.WATCH) {
  b.on("update", bundle);
}
