# Project MapIt

[![build status](https://dev.lev-interactive.com/project-map-it/project-map-it/badges/production/build.svg)](https://dev.lev-interactive.com/project-map-it/project-map-it/commits/production)

## Running

1.  `npm install`
2.  `npm start` for running

## User Roles

 | Account MGMT | User MGMT | Project Read | Project Create | Project Update | Project Delete
--- | --- | --- | --- | --- | --- | ---
Owner | x | x | x | x | x | x
Admin | | | x | x | x | x
Editor | | | x | x | x |
Guest (not logged in)  |  | | x | | |

## API

### Endpoints

-   `/api/users` CRUD (restricted to SUPERADMIN)
-   `/api/organizations` CRUD (restricted to SUPERADMIN)
-   `/api/teams` CRUD (restricted to OWNER)
-   `/api/project-categories` CRUD
-   `/api/projects` CRUD

#### USER

- `GET /api/user/:id`: Get a single user.
    - Returns: List Object
- `GET /api/user`: Gets users.
    - Returns: List Object
    - Note: Will return only users user has access to. Owner will only see their org.
- `PUT /api/user/:id`: Updates user.
    - Returns: User Object
    - Note: Handles special `orgname` prop and `password` update.
- `DELETE /api/user/:id`: Deletes user.
    - Returns: User Object
- `POST /api/user/`: Invites new user.
    - Returns: User Object
- `POST /api/user/invite/:id`: Re-invites unconfirmed user.
    - Returns: User Object

#### GET all

-   `GET /api/users`
-   `GET /api/users?page=2` => pagination
-   `GET /api/users?filter={"email": "rafaelgou@gmail.com"}`
    => filter, any MongoDB query works
-   `GET /api/users?sort={"email": "asc"}` => sort, Mongoose or MongoDB
    syntax (allows both: `{"email": "asc/desc", "name": 1/-1}`)
-   `GET /api/users?limit=50` => changes `perPage/skip` for pagination
-   `GET /api/users?page=2&sort={"email": "asc"}&filter={"email": "rafaelgou@gmail.com"}` => any combination

```javascript
{
  page: 1,        // actual page
  pages: 3,       // total pages
  perPage: 20,    // actual limit/skip = per page records
  totalCount: 49, // total records of this
  results: []     // array of records
}
```

#### GET one

-   `GET /api/users/573e2b16a69132947969fc39` => specific user

```javascript
{
  "_id": "573e2b16a69132947969fc39",
  "name": "Rafael Goulart",
  "email": "rafaelgou@gmail.com",
  // ... rest of fields
}
```

Not found user renders a 404 page.

#### POST

-   `POST /api/users` => returns saved record

Body:

```javascript
{
  "name": "Rafael Goulart",
  "email": "rafaelgou@gmail.com",
  // ... rest of fields
}
```

On error sends validation mongoose validation errors.

```javascript
{
  "message": "User validation failed",
  "name": "ValidationError",
  "errors": {
    "password": {
      "message": "Password required",
      "name": "ValidatorError",
      "properties": {
        "type": "required",
        "message": "Password required",
        "path": "password"
      },
      "kind": "required",
      "path": "password"
    },
    "email": {
      "message": "Invalid email",
      "name": "ValidatorError",
      "properties": {
        "type": "user defined",
        "message": "Invalid email",
        "path": "email",
        "value": "rafaelgou"
      },
      "kind": "user defined",
      "path": "email",
      "value": "rafaelgou"
    }
  }
}
```

#### PUT

-   `PUT /api/users/573e2b16a69132947969fc39` => returns saved record

Body:

```javascript
{
  "_id": "573e2b16a69132947969fc39",
  "name": "Rafael Goulart",
  "email": "rafaelgou@gmail.com",
  // ... rest of records
}
```

On error sends validation mongoose validation errors (same as POST).

#### DELETE

-   `DELETE /api/users/573e2b16a69132947969fc39` => returns empty object

### Middlewares

#### ApiAuth

`src/middlewares/api-auth.js`

Checks if user has access to some enpoint. Configured on
`src/config/apiAuth.js`.
Each route receives an array of allowed roles (`[0, 1]` allows superadmin and
owner) or minimum role (`2` allows superadmin, owner and admin).

```javascript
const apiAuth = {
  // This creates matches for crud routes:
  // '/api/users': { GET: [0, 1, 2], POST: [0, 1, 2], PUT: [0, 1, 2], DEL: [0, 1] }
  crud: {
    users: {
      GET: [0, 1, 2],
      POST: [0, 1, 2],
      PUT: [0, 1, 2],
      DEL: 1, // <= generate GET, POST, PUT, DEL for [0, 1] (1 is minimum)
    },
    organizations: 1, // <= generate GET, POST, PUT, DEL for [0, 1] (1 is minimum)
    projects: [0, 1, 2], // <= generate GET, POST, PUT, DEL for all roles
  },
  // any non crud enpoint matchs here
  // they are matched BEFORE crud
  static: {
    '/api/users/mypath': { GET: [0,1] }
  }
}
```

Use:

```javascript
const apiAuth = require('../middlewares/api-auth');
router.use('/api/users', apiAuth, require('./users'));
```

#### ApiProtect

`src/middlewares/api-protect.js`

Allows multiple forms of protection for an endpoint based on a role or forcing
filters.

##### exact

Must match exact role.

```javascript
const protect = require('../middlewares/api-protect');
router.get('/', protect.exact('owner'), protect.forceUserOrganization, crud.get);
// or
router.get('/', protect.exact(1), protect.forceUserOrganization, crud.get);
// Allows only owner (role = 1)
```

##### min

Must match at least the role.

```javascript
const protect = require('../middlewares/api-protect');
router.get('/', protect.min('owner'), crud.get);
// or
router.get('/', protect.min(1), crud.get);
// Allows only owner (role = 1) or superadmin (role = 0)
```

##### forceUserOrganization

Inject req.user.organization into the filter - user can only see his
organization records.

```javascript
const protect = require('../middlewares/api-protect');
router.get('/', protect.forceUserOrganization, crud.get);
// Filters records for logged in user organization
```

##### forceUser

Ininject req.user.id into the filter - user can only see his records
(related to himself `user: user_id`).

```javascript
const protect = require('../middlewares/api-protect');
router.get('/', protect.forceUser, crud.get);
// Filters records related to logged in user
```

#### Multiple use

As middlewares, they can be used togheter:

```javascript
const protect = require('../middlewares/api-protect');
router.get('/', protect.min('owner'), protect.forceUserOrganization, crud.get);
```

### New Endpoints

-   Create a file mongoose model in `src/models/books.js`
-   Create a controller in `src/controllers/books.js` as the following:

```javascript
'use strict';

const express = require('express');
const router  = express.Router();
const Model   = require('../models/book');
const protect = require('../middlewares/api-protect');

const options = {
  // select: {}
  // filter: {}
  // sort: { name: 'asc' }
  // page: 1
  // perPage: 20
  // population: null
};
const crud    = require('../helpers/crud')(Model, options);

// Sample using protection
router.get('/', protect.forceUserOrganization, crud.get);
router.get('/:id', protect.forceUserOrganization, crud.getOne);
router.post('/', protect.forceUserOrganization, crud.post);
router.put('/:id', protect.forceUserOrganization, crud.put);
router.delete('/:id', protect.forceUserOrganization, crud.delete);

module.exports = router;
```

*options* allows you to set initial MongoDB/Mongoose query:

-   *select*: enable/disable fields on the GET.
    MongoDB syntax: `{ name: 1 }`
    will only display: `{ _id: '', name: ''}` while  `{ name: 0 }`
    will display all fields _BUT_ `name`.
    Mongoose syntax also allowed: `'-name +sex'`.
    *CAN NOT BE OVERRIDE BY API REQUEST*.
    Default to `{}` (all fields sent by Mongoose model).
-   *filter*: injects a filter to create MongoDB query. Any MongoDB query
    syntax here. *CAN NOT BE OVERRIDE BY API REQUEST, WILL BE MERGED AS THE LAST*
    Default to `{}` (all records).
-   *sort*: initial sort, Mongoose or MongoDB syntax
    (allows both: `{"email": "asc/desc", "name": 1/-1}`).
    Default to  `{ "name": 1 }` - sort by `name` field.
    Can be override by an API request.
-   *page*: initial page. Default to `1`. `-1` disables pagination.
    Can be override by an API request.
-   *perPage*: pagination offset (limit/skip).
    Default to `20`. Can be override by an API request using `&limit=50`.
    Ignored if `page = -1`.
-   *population*:  merges refs to the record (like a SQL "join").
    Mongoose syntax. Default to `null`.

Add it to `src/controller/index.js`:

```javascript
router.use('/api/books', apiAuth, require('./books'));
```

Add it to apiAuth config (`src/config/apiAuth.js`):

```javascript
// ...
const apiAuth = {
  crud: {
    // ...
    // see instructions on this readme above
    'project-categories': 2,
  },
  // ...
}
// ...
```
