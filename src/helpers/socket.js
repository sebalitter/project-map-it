const config = require("config");
const redisAdapter = require("socket.io-redis");

let io;

module.exports = function(server) {
  if (!io) {
    if (!server) {
      throw new Error("Server param must be set for socket.io.");
    }
    io = require("socket.io")(server);
    io.adapter(redisAdapter(config.redis));
  }
  return io;
};
