const _ = require("lodash");

const errorFormat = function(err) {
  const obj = { errors: [] };

  if (_.isString(err)) {
    obj.errors.push(err);
  } else if (err.code === 1201988) {
    // Need to upgrade plan.
    obj.errors.push(err.message);
  } else if (err && err.raw && err.raw.message) {
    obj.errors.push(err.raw.message); // Stripe.
  } else if (err.code === 11000) {
    // Because duplicate key errors are ugly.
    obj.errors.push("This user already exists in the system.");
  } else if (err.code) {
    obj.errors.push(err.errmsg);
  } else if (_.isObject(err.errors) || _.isArray(err.errors)) {
    _.each(err.errors, function(e, k) {
      obj.errors.push(e.message);
    });
  } else if (_.isString(err.message)) {
    obj.errors.push(err.message);
  } else {
    obj.errors.push("Unknown error.");
  }

  return obj;
};

module.exports = errorFormat;
