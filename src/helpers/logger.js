const winston = require("winston");

exports.warn = winston.warn;
exports.error = winston.error;
exports.info = winston.info;
exports.debug = winston.debug;
