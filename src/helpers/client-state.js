const config = require("config");
const _ = require("lodash");

module.exports = function(user) {
  if (user === false) {
    return false;
  }

  function isDisabled() {
    return !user.organization.active;
  }

  function hideIfNot(roles) {
    if (!user) {
      return true;
    }

    return !user.inRoles(roles);
  }

  return JSON.stringify({
    user: user ? user : false,
    plans: config.plans,
    roles: [
      {
        name: "Super Admin",
        constant: "SUPERADMIN",
        id: 0
      },
      {
        name: "Owner",
        constant: "OWNER",
        id: 1
      },
      {
        name: "Admin",
        constant: "ADMIN",
        id: 2
      },
      {
        name: "Editor",
        constant: "EDITOR",
        id: 3
      },
      {
        name: "Guest",
        constant: "GUEST",
        id: 4
      }
    ],
    menu: {
      sidebar: [
        {
          path: "/",
          title: '<i class="fa fa-globe"></i><span>&nbsp;Map</span>',
          external: false,
          disabled: false,
          hidden: hideIfNot(["SUPERADMIN", "OWNER", "ADMIN", "EDITOR", "GUEST"])
        },
        {
          path: "/login",
          title: "Sign In",
          external: true,
          disabled: false,
          hidden: user ? true : false
        },
        {
          path: "/register",
          title: "Create an Account",
          external: true,
          disabled: false,
          hidden: user ? true : false
        },
        {
          path: "/projects/manage",
          title: '<i class="fa fa-map-marker"></i><span>&nbsp;Projects</span>',
          external: false,
          disabled: isDisabled(),
          hidden: hideIfNot(["SUPERADMIN", "OWNER", "ADMIN", "EDITOR"])
        },
        {
          path: "/survey",
          title: '<i class="fa fa-edit"></i><span>&nbsp;Survey</span>',
          external: false,
          disabled: isDisabled(),
          hidden: hideIfNot(["SUPERADMIN", "OWNER", "ADMIN", "EDITOR"])
        },
        {
          path: "/team/manage",
          title: '<i class="fa fa-users"></i><span>&nbsp;Team</span>',
          external: false,
          disabled: isDisabled(),
          hidden: hideIfNot(["SUPERADMIN", "OWNER", "ADMIN"])
        },
        {
          path: "/account",
          title: '<i class="fa fa-cogs"></i><span>&nbsp;Account</span>',
          external: false,
          disabled: false,
          hidden: user ? false : true
        },
        {
          path: "http://support.projectmap.it/",
          title: '<i class="fa fa-info-circle"></i><span>&nbsp;Support</span>',
          external: true,
          disabled: false,
          hidden: false
        },
        {
          path: "/logout",
          title: '<i class="fa fa-power-off"></i><span>&nbsp;Logout</span>',
          external: true,
          disabled: false,
          hidden: user ? false : true
        }
      ],
      survey: [
        {
          path: "/survey",
          title: "Survey",
          external: false,
          disabled: isDisabled(),
          hidden: hideIfNot(["SUPERADMIN", "OWNER", "ADMIN", "EDITOR"])
        }
      ],
      projects: [
        {
          path: "/projects/manage",
          title: "Projects",
          external: false,
          disabled: isDisabled(),
          hidden: hideIfNot(["SUPERADMIN", "OWNER", "ADMIN", "EDITOR", "GUEST"])
        },
        {
          path: "/projects/categories",
          title: "Manage Project Categories",
          external: false,
          disabled: isDisabled(),
          hidden: hideIfNot(["SUPERADMIN", "OWNER", "ADMIN", "EDITOR"])
        }
      ],
      team: [
        {
          path: "/team/manage",
          title: "Team",
          external: false,
          disabled: false,
          hidden: hideIfNot(["SUPERADMIN", "OWNER", "ADMIN"])
        }
      ],
      account: [
        {
          path: "/account/manage",
          title: "Account",
          external: false,
          disabled: false,
          hidden: user ? false : true
        },
        {
          path: "/account/organization",
          title: "Organization",
          external: false,
          disabled: false,
          hidden: hideIfNot(["SUPERADMIN", "OWNER", "ADMIN"])
        },
        {
          path: "/account/subscription",
          title: "Subscription",
          external: false,
          disabled: false,
          hidden: hideIfNot(["SUPERADMIN", "OWNER"])
        },
        {
          path: "/account/embed",
          title: "Embed Your Map",
          external: false,
          disabled: isDisabled(),
          hidden: hideIfNot(["SUPERADMIN", "OWNER", "ADMIN", "EDITOR", "GUEST"])
        }
      ]
    }
  }).replace(/<\//g, "<\\/");
};
