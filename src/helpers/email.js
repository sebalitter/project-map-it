const path = require("path");
const nodemailer = require("nodemailer");
const logger = require("../helpers/logger");
const config = require("config");
const jade = require("jade");
const striptags = require("striptags");
const _ = require("lodash");
const EmailTemplate = require("email-templates").EmailTemplate;
const templatesDir = path.join(__dirname, "../", "views", "emails");
const transporter = nodemailer.createTransport(config.smtp);
const queue = require("./redis-queue");

queue.process("email", 2, async function(job, done) {
  transporter.sendMail(job.data.settings, function(err, info) {
    if (err) {
      job.log("There was an error sending: " + job.message);
      done(err);
      return logger.error("There was an error sending an email.", err);
    }

    job.log(info);
    done();
  });
});

/**
 * @param {string} type  This needs to be a name of a file in emails/ view.
 * @param {object} emailArgs  This is used to send the email. Per nodemail args.
 * @param {object} viewArgs  This will be passed to the view.
 */
exports.send = function(type, emailArgs, viewArgs) {
  return new Promise((resolve, reject) => {
    const template = new EmailTemplate(path.join(templatesDir, type));

    const readOpts = {
      encoding: "utf8"
    };

    const emailSettings = _.defaults(emailArgs || {}, {
      subject: "",
      to: "",
      from: config.sysFromAddress
    });

    template.render(viewArgs, function(err, results) {
      if (err) {
        reject(err);
        return logger.error("There was an error formating an email.", err);
      }

      emailSettings.html = results.html;
      emailSettings.text = results.text;

      queue
        .create("email", {
          title: "Sending email to " + emailSettings.to,
          settings: emailSettings
        })
        .attempts(3)
        .save();

      resolve();
    });
  });
};
