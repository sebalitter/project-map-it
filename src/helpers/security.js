/**
 * Password functions powered by crypto
 */
const crypto = require("crypto");
const _ = require("lodash");
const LEN = 128;
const SALT_LEN = 128;
const ITERATIONS = 10000;
const DIGEST = "sha256";

/**
 * Hash password using salt or generates a random salt
 */
function hashPassword(password, salt) {
  if (!salt) {
    salt = generateSalt();
  }
  const len = LEN / 2;
  const hashedPassword = crypto
    .pbkdf2Sync(password, salt, ITERATIONS, len, DIGEST)
    .toString("hex");
  return {
    hashedPassword: hashedPassword,
    salt: salt
  };
}

function verifyPassword(plainPassword, hashedPassword, salt) {
  return hashPassword(plainPassword, salt).hashedPassword === hashedPassword;
}

function generateSalt() {
  return crypto.randomBytes(SALT_LEN / 2).toString("hex");
}

const utils = {
  has: function(v) {
    return (
      v && v.first && v.first.length > 0 && v.second && v.second.length > 0
    );
  },
  match: function(v) {
    return v.first === v.second;
  },
  min: function(v) {
    return v.first.length >= 6;
  },
  isValid: function(v) {
    return this.has(v) && this.match(v) && this.min(v);
  }
};

function slugify(str) {
  return _.toLower(_.deburr(str))
    .replace(/\s+/g, "-") // Replace spaces with -
    .replace(/[^\w\-]+/g, "") // Remove all non-word chars
    .replace(/\-\-+/g, "-") // Replace multiple - with single -
    .replace(/^-+/, "") // Trim - from start of text
    .replace(/-+$/, ""); // Trim - from end of text
}

// A quick random string generator.
function rand() {
  var text = "";
  var possible =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (var i = 0; i < 5; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }

  return text;
}

module.exports = {
  hashPassword: hashPassword,
  verifyPassword: verifyPassword,
  generateSalt: generateSalt,
  slugify: slugify,
  utils: utils,
  rand
};
