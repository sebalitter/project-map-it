/**
 * This module handles async geolocating addresses to lat/lngs.
 */

const https = require("https");
const { get } = require("lodash");
const config = require("config");
const { Project } = require("../models");
const queue = require("./redis-queue");
const logger = require("./logger");

/**
 * Get the bound box for any amount of points.
 *
 * @param coords [ [lat, lng], [lat, lng], ... ]
 * @return {object} bounds
 */
function getBBoxExtent(coords) {
  const limits = [
    Number.POSITIVE_INFINITY,
    Number.POSITIVE_INFINITY,
    Number.NEGATIVE_INFINITY,
    Number.NEGATIVE_INFINITY
  ];

  const bbox = coords.reduce(function(prev, coord) {
    return [
      Math.min(coord[0], prev[0]),
      Math.min(coord[1], prev[1]),
      Math.max(coord[0], prev[2]),
      Math.max(coord[1], prev[3])
    ];
  }, limits);

  return {
    south: bbox[0],
    west: bbox[1],
    north: bbox[2],
    east: bbox[3]
  };
}

/**
 * Use google geolocator to reverse locate a address string.
 * .
 * @TODO Clean - This is pretty messy.
 * @param key - deprecated.
 */
function getGeoGMaps(search, callback) {
  let query = "";

  if (typeof search === "object" && search.lat && search.lng) {
    query = `latlng=${search.lat},${search.lng}&location_type=ROOFTOP`;
  } else {
    query = encodeURIComponent(search);
    query = `address=${query}`;
  }

  const u = `/maps/api/geocode/json?${query}&key=${config.googleServerApiKey}`;

  logger.info(`Attemtping to geolocation: ${u}`);

  const options = {
    hostname: "maps.google.com",
    port: 443,
    path: u,
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      charset: "UTF-8"
    }
  };

  https
    .get(options, res => {
      let str = "";
      const contentType = res.headers["content-type"];

      res.on("data", function(chunk) {
        str += chunk;
      });

      res.on("end", () => {
        if (res.statusCode !== 200) {
          logger.error("Bad http request geolocation: " + res.statusCode);
          return callback(
            new Error(
              "Failed at geolocating. Potential http issue: " + res.statusCode
            )
          );
        }

        if (
          contentType !== undefined &&
          contentType.indexOf("application/json") >= 0
        ) {
          try {
            str = JSON.parse(str);
          } catch (err) {
            logger.warn(
              "There was an error when parsing returned json string:",
              err.message,
              str
            );
            return callback(new Error("Returned bad JSON."));
          }
        }

        if (str.status === "ZERO_RESULTS") {
          logger.warn("Found zero results for address: " + query, str);
          return callback(new Error(str.status));
        }

        if (str.status !== "OK") {
          logger.warn(
            "Geolocation: got a bad request from the Google API: " + u,
            str
          );
          return callback(new Error("Could not find coordinates for address."));
        }

        const address = {};
        const lat = get(str, "results[0].geometry.location.lat", null);
        const lng = get(str, "results[0].geometry.location.lng", null);
        const components = get(str, "results[0].address_components", []);

        components.forEach(obj => {
          if (get(obj, "types[0]", "") === "administrative_area_level_1") {
            address.state = obj.short_name;
            return false;
          }
        });

        if (lat && lng) {
          address.lat = lat;
          address.lng = lng;
        }

        callback(null, address);
      });
    })
    .on("error", e => {
      callback(e);
    });
}

queue.process("geolocate", 10, async function(job, done) {
  getGeoGMaps(job.data.search, async (err, address) => {
    if (err) {
      job.log("Geolocation failed for project: " + err.message);
      logger.warn("Geolocation Job: failed.");
      return done(err);
    }

    try {
      const doc = await Project.findById(job.data.docId);

      if (!doc) {
        job.log(
          "It looks like this project (%s) was removed. Moving on.",
          job.data.docId
        );
        logger.info(
          "Geolocation Job: A job must have been removed before it geolocated. Not restarting. Project ID: " +
            job.data.docId
        );
        return done();
      }

      job.log(
        "Completed geolocation with lat (%s) and lng (%s).",
        String(address.lat),
        String(address.lng)
      );

      // Careful not to save() because it will cause a never ending loop of
      // geolocation. Not good!
      const saved = await Project.updateOne(
        {
          _id: doc._id
        },
        {
          $set: {
            location: [address.lng, address.lat],
            state: address.state
          }
        }
      );
    } catch (err) {
      job.log("There was a problem saving the job: " + err.message);
      logger.warn("Geolocation job: Problem saving job. " + err.message);
      return done(err);
    }

    done();
  });
});

module.exports = {
  getBBoxExtent: getBBoxExtent,
  getGeoGMaps: getGeoGMaps,
  queue: queue
};
