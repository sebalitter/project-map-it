const mongoose = require("mongoose");
const config = require("config");
const db = mongoose.connection;
const logger = require("./logger");

const ObjectID = mongoose.Types.ObjectId;

mongoose.Promise = global.Promise;

if (process.env.NODE_ENV === "development") {
  mongoose.set("debug", true);
}

mongoose.connection
  .openUri(config.mongoConnectionString)
  .once("open", () => logger.info("MongoDB Connected."))
  .on("error", err => logger.error("MongoDB connection issue:", err));

/**
 * Regex to check MongoID
 *
 * @type {RegExp}
 */
const checkForHexRegExp = new RegExp("^[0-9a-fA-F]{24}$");

/**
 * Check to see if object is a valid mongo id.
 *
 * @param  {string} str
 * @return {boolean}
 */
function validMongoId(str) {
  return checkForHexRegExp.test(str) && ObjectID.isValid(str);
}

exports.validMongoId = validMongoId;

/**
 * Attempt to convert a string to a MongoID.
 *
 * @param  {string} str
 * @return {ObjectID|null}
 */
function mongoId(str) {
  if (typeof str !== "string" && ObjectID.isValid(str)) {
    return str;
  } else if (typeof str === "string" && validMongoId(str)) {
    return new ObjectID(str);
  } else if (str === null || str === false) {
    return null;
  }
  // error('mongo:mongoId method was passed an bad ID value:', str, new Error().stack);
  throw new Error("Invalid mongoId passed: " + str);
}

exports.mongoId = mongoId;

exports.db = mongoose.connection;
