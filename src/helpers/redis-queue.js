const kue = require("kue");
const config = require("config");
const logger = require("./logger");

const queue = kue.createQueue({
  redis: config.redis
});

const MAX = 10000;

queue.on("error", function(err) {
  logger.error("There was an error in the redis queue:", err.message);
});

queue.completeCount(function(err, total) {
  if (total > MAX) {
    logger.info(`Complete count is over ${MAX}, cleaning up.`);

    kue.Job.rangeByState("complete", 0, MAX - 100, "asc", function(err, jobs) {
      jobs.forEach(function(job) {
        job.remove();
      });
    });
  }
});

module.exports = queue;
