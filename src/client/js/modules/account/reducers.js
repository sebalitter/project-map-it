import {remoteReducer} from '../core';
import {assign} from 'lodash';
import * as c from './constants';

export function deactivatedAccount(state, action) {
  return remoteReducer('ACCOUNT_DEACTIVATE', state, action);
}

const _deactivateState = {
  modalShowing: false
};

export function accountDeactivateState(state = _deactivateState, action) {
  switch (action.type) {
    case c.TOGGLE_ACCOUNT_DEACTIVATION_MODAL:
      return assign({}, state,
        {
          modalShowing: !state.modalShowing
        }
      );
  }
  return state;
}
