import * as c from './constants';
import {
  actionCurry,
  request,
  ajaxSettings
} from '../core';

export function deactivateAccount(reason = '', successMessage) {
  const endpoint = `/api/users/deactivate`;
  const _actions = {
    start: actionCurry(c.ACCOUNT_DEACTIVATE_START),
    success: actionCurry(c.ACCOUNT_DEACTIVATE_SUCCESS),
    error: actionCurry(c.ACCOUNT_DEACTIVATE_ERROR)
  };

  return (dispatch) => {
    dispatch(_actions.start());
    return request(
      dispatch,
      endpoint,
      _actions.success,
      _actions.error,
      ajaxSettings({
        method: 'POST',
        body: {
          reason
        }
      }),
      successMessage
    );
  };
}

export function toggleAccountDeactivateModal() {
  return {
    type: c.TOGGLE_ACCOUNT_DEACTIVATION_MODAL
  };
}
