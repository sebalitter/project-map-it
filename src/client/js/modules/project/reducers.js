import {assign} from 'lodash';
import {remoteReducer} from '../core';
import * as c from './constants';

export function fetchedProjects(state, action) {
  return remoteReducer('PROJECTS_GET', state, action);
}

export function fetchedMapDefaults(state, action) {
  return remoteReducer('PROJECTS_MAP_DEFAULTS_GET', state, action);
}

export function fetchedGeoProjects(state, action) {
  return remoteReducer('PROJECTS_SPATIAL_GET', state, action, true);
}

export function fetchedProject(state, action) {
  return remoteReducer('PROJECT_GET', state, action);
}

export function fetchedProjectLocation(state, action) {
  return remoteReducer('PROJECT_LOCATION_GET', state, action);
}

export function createdProject(state, action) {
  return remoteReducer('PROJECT_CREATE', state, action);
}

export function createdBulkProjects(state, action) {
  return remoteReducer('PROJECTS_BULK_CREATE', state, action);
}

export function updatedProject(state, action) {
  return remoteReducer('PROJECT_UPDATE', state, action);
}

export function deletedProject(state, action) {
  return remoteReducer('PROJECT_DELETE', state, action);
}

export function deletedAllProjects(state, action) {
  return remoteReducer('PROJECTS_ALL_DELETE', state, action);
}

export function sendAllSurveys(state, action) {
  return remoteReducer('PROJECT_ALL_SURVEY_EMAIL', state, action);
}

export function uploadedProjectPhoto(state, action) {
  return remoteReducer('PROJECT_PHOTO_UPLOAD', state, action);
}

export function deletedProjectPhoto(state, action) {
  return remoteReducer('PROJECT_PHOTO_DELETE', state, action);
}

const _surveyModal = {
  showing: false,
  emails: [],
  survey: []
};
export function surveyModal(state = _surveyModal, action) {
  switch (action.type) {
    case c.SHOW_SURVEY_MODAL:
      return assign({}, state, {
        showing: true,
        emails: action.emails,
        survey: action.survey,
        id: action.id
      });
    case c.HIDE_SURVEY_MODAL:
      return assign({}, state, {
        showing: false
      });
    case c.PROJECT_SURVEY_EMAIL_SUCCESS:
      return _surveyModal;
  }
  return state;
}

export function emailedSurvey(state, action) {
  return remoteReducer('PROJECT_SURVEY_EMAIL', state, action);
}

const _projectModalState = {
  showProjectImportModal: false,
  showProjectExportModal: false,
  showProjectReviewExportModal: false,
  importInfo: {
    errors: [],
    headers: {},
    data: []
  }
};

export function projectModalState(state = _projectModalState, action) {
  switch (action.type) {
    case c.TOGGLE_PROJECT_IMPORT_MODAL:
      return assign({}, state, {
        showProjectImportModal: !state.showProjectImportModal,
        importInfo: assign({}, _projectModalState.importInfo, action.info)
      });
    case c.TOGGLE_PROJECT_EXPORT_MODAL:
      return assign({}, state, {
        showProjectExportModal: !state.showProjectExportModal
      });
    case c.TOGGLE_PROJECT_REVIEW_EXPORT_MODAL:
      return assign({}, state, {
        showProjectReviewExportModal: !state.showProjectReviewExportModal
      });
    case c.PROJECTS_GET_SUCCESS:
    case c.PROJECTS_GET_ERROR:
      return assign({}, state, {
        showProjectImportModal: false
      });
    case c.PROJECTS_DOWNLOAD_GET_START:
      return assign({}, state, {
        exportProcessing: true
      });
    case c.PROJECTS_DOWNLOAD_GET_SUCCESS:
    case c.PROJECTS_DOWNLOAD_GET_ERROR:
      return assign({}, state, {
        exportProcessing: false,
        showProjectExportModal: false
      });
  }
  return state;
}
