import * as c from "./constants";
import fetch from "isomorphic-fetch";
import download from "downloadjs";
import { each, trim, map, assign } from "lodash";
import {
  actionCurry,
  request,
  ajaxSettings,
  filterSortQueryStr
} from "../core";

export function fetchProjects(page = -1, filter = {}, sort = {}) {
  const endpoint = `/api/projects/${filterSortQueryStr(page, filter, sort)}`;
  const _actions = {
    start: actionCurry(c.PROJECTS_GET_START),
    success: actionCurry(c.PROJECTS_GET_SUCCESS),
    error: actionCurry(c.PROJECTS_GET_ERROR)
  };

  return dispatch => {
    dispatch(_actions.start());
    return request(
      dispatch,
      endpoint,
      _actions.success,
      _actions.error,
      ajaxSettings({ method: "GET" })
    );
  };
}

export function fetchMapDefaults(orgId) {
  const endpoint = `/api/projects/map-defaults/${orgId}`;
  const _actions = {
    start: actionCurry(c.PROJECTS_MAP_DEFAULTS_GET_START),
    success: actionCurry(c.PROJECTS_MAP_DEFAULTS_GET_SUCCESS),
    error: actionCurry(c.PROJECTS_MAP_DEFAULTS_GET_ERROR)
  };

  return dispatch => {
    dispatch(_actions.start());
    return request(
      dispatch,
      endpoint,
      _actions.success,
      _actions.error,
      ajaxSettings({ method: "GET" })
    );
  };
}

export function fetchGeoProjects(
  org,
  zoom,
  bllng,
  bllat,
  urlng,
  urlat,
  categories = [],
  photos = 0,
  surveys = 0
) {
  const cat = categories.length ? categories : "none";
  const endpoint =
    `/api/projects/spatial/${org}/${zoom}/${bllng}/` +
    `${bllat}/${urlng}/${urlat}/${cat}/${photos}/${surveys}`;

  const _actions = {
    start: actionCurry(c.PROJECTS_SPATIAL_GET_START),
    success: actionCurry(c.PROJECTS_SPATIAL_GET_SUCCESS),
    error: actionCurry(c.PROJECTS_SPATIAL_GET_ERROR)
  };

  return dispatch => {
    // dispatch(_actions.start());
    return request(
      dispatch,
      endpoint,
      _actions.success,
      _actions.error,
      ajaxSettings({ method: "GET" })
    );
  };
}

export function fetchProjectsForDownload(from, to) {
  let endpoint = `/api/projects/csv`;

  if (from && to) {
    const f = from.hour(0).format();
    const t = to.hour(23).format();
    endpoint += `?filter={"created": {"$gte": "${f}", "$lte": "${t}"}}`;
  }

  const _actions = {
    start: actionCurry(c.PROJECTS_DOWNLOAD_GET_START),
    success: actionCurry(c.PROJECTS_DOWNLOAD_GET_SUCCESS),
    error: actionCurry(c.PROJECTS_DOWNLOAD_GET_ERROR)
  };

  const fetchOpts = {
    headers: {
      "Content-Type": "text"
    },
    method: "GET",
    credentials: "same-origin"
  };

  const url = `${window.location.protocol}//${window.location.host}`;

  return dispatch => {
    _actions.start();
    return fetch(url + endpoint, fetchOpts).then(res => {
      res
        .text()
        .then(data => {
          if (res.status >= 200 && res.status < 300) {
            dispatch(_actions.success());
            return download(data, "projects.csv", "text/csv");
          }
          dispatch(_actions.error());
          alert("There was an error with your request. Please try again.");
        })
        .catch(err => {
          dispatch(_actions.error());
          alert("There was an error with your request. Please try again.");
        });
    });
  };
}

export function fetchProject(id) {
  const endpoint = `/api/projects/${id}`;
  const _actions = {
    start: actionCurry(c.PROJECT_GET_START),
    success: actionCurry(c.PROJECT_GET_SUCCESS),
    error: actionCurry(c.PROJECT_GET_ERROR)
  };

  return dispatch => {
    dispatch(_actions.start());
    return request(
      dispatch,
      endpoint,
      _actions.success,
      _actions.error,
      ajaxSettings({ method: "GET" })
    );
  };
}

export function fetchProjectLocation(id) {
  const endpoint = `/api/projects/location/${id}`;
  const _actions = {
    start: actionCurry(c.PROJECT_LOCATION_GET_START),
    success: actionCurry(c.PROJECT_LOCATION_GET_SUCCESS),
    error: actionCurry(c.PROJECT_LOCATION_GET_ERROR)
  };

  return dispatch => {
    dispatch(_actions.start());
    return request(
      dispatch,
      endpoint,
      _actions.success,
      _actions.error,
      ajaxSettings({ method: "GET" })
    );
  };
}

export function createProject(vals, successMessage) {
  const endpoint = `/api/projects/`;
  const _actions = {
    start: actionCurry(c.PROJECT_CREATE_START),
    success: actionCurry(c.PROJECT_CREATE_SUCCESS),
    error: actionCurry(c.PROJECT_CREATE_ERROR)
  };

  return dispatch => {
    dispatch(_actions.start());
    return request(
      dispatch,
      endpoint,
      _actions.success,
      _actions.error,
      ajaxSettings({ method: "POST", body: vals }),
      successMessage
    );
  };
}

export function createProjectsBulk(vals, successMessage) {
  const endpoint = `/api/projects/import`;
  const _actions = {
    start: actionCurry(c.PROJECTS_BULK_CREATE_START),
    success: actionCurry(c.PROJECTS_BULK_CREATE_SUCCESS),
    error: actionCurry(c.PROJECTS_BULK_CREATE_ERROR)
  };

  return dispatch => {
    dispatch(_actions.start());
    return request(
      dispatch,
      endpoint,
      _actions.success,
      _actions.error,
      ajaxSettings({ method: "POST", body: vals }),
      successMessage
    );
  };
}

export function updateProject(id, vals, successMessage) {
  const endpoint = `/api/projects/${id}`;
  const _actions = {
    start: actionCurry(c.PROJECT_UPDATE_START),
    success: actionCurry(c.PROJECT_UPDATE_SUCCESS),
    error: actionCurry(c.PROJECT_UPDATE_ERROR)
  };

  return dispatch => {
    dispatch(_actions.start());
    return request(
      dispatch,
      endpoint,
      _actions.success,
      _actions.error,
      ajaxSettings({ method: "PUT", body: vals }),
      successMessage
    );
  };
}

export function updateProjectPhotoOrder(ids, successMessage) {
  const endpoint = `/api/files/order`;
  const _actions = {
    start: actionCurry(c.PHOTO_ORDER_UPDATE_START),
    success: actionCurry(c.PHOTO_ORDER_UPDATE_SUCCESS),
    error: actionCurry(c.PHOTO_ORDER_UPDATE_ERROR)
  };

  return dispatch => {
    dispatch(_actions.start());
    return request(
      dispatch,
      endpoint,
      _actions.success,
      _actions.error,
      ajaxSettings({
        method: "PUT",
        body: ids
      }),
      successMessage
    );
  };
}

export function deleteProject(id, successMessage) {
  const endpoint = `/api/projects/${id}`;
  const _actions = {
    start: actionCurry(c.PROJECT_DELETE_START),
    success: actionCurry(c.PROJECT_DELETE_SUCCESS),
    error: actionCurry(c.PROJECT_DELETE_ERROR)
  };

  return dispatch => {
    dispatch(_actions.start());
    return request(
      dispatch,
      endpoint,
      _actions.success,
      _actions.error,
      ajaxSettings({ method: "DELETE" }),
      successMessage
    );
  };
}

export function removeAllProjects(successMessage) {
  const endpoint = `/api/projects/all`;
  const _actions = {
    start: actionCurry(c.PROJECTS_ALL_DELETE_START),
    success: actionCurry(c.PROJECTS_ALL_DELETE_SUCCESS),
    error: actionCurry(c.PROJECTS_ALL_DELETE_ERROR)
  };

  return dispatch => {
    dispatch(_actions.start());
    return request(
      dispatch,
      endpoint,
      _actions.success,
      _actions.error,
      ajaxSettings({ method: "DELETE" }),
      successMessage
    );
  };
}

export function sendAllSurveys(successMessage) {
  const endpoint = `/api/projects/survey`;
  const _actions = {
    start: actionCurry(c.PROJECT_ALL_SURVEY_EMAIL_START),
    success: actionCurry(c.PROJECT_ALL_SURVEY_EMAIL_SUCCESS),
    error: actionCurry(c.PROJECT_ALL_SURVEY_EMAIL_ERROR)
  };

  return dispatch => {
    dispatch(_actions.start());
    return request(
      dispatch,
      endpoint,
      _actions.success,
      _actions.error,
      ajaxSettings({
        method: "POST",
        body: {
          all: true
        }
      }),
      successMessage
    );
  };
}

export function uploadProjectPhoto(projectId, images, successMessage) {
  const endpoint = `/api/files/upload/${projectId}`;
  const _actions = {
    start: actionCurry(c.PROJECT_PHOTO_UPLOAD_START),
    success: actionCurry(c.PROJECT_PHOTO_UPLOAD_SUCCESS),
    error: actionCurry(c.PROJECT_PHOTO_UPLOAD_ERROR)
  };

  const data = new FormData();
  each(images, (img, idx) => {
    if (idx < 21) {
      data.append(idx, img);
    }
  });

  return dispatch => {
    dispatch(_actions.start());
    return request(
      dispatch,
      endpoint,
      _actions.success,
      _actions.error,
      {
        credentials: "same-origin",
        method: "POST",
        body: data
      },
      successMessage
    );
  };
}

export function deleteProjectPhoto(id, successMessage) {
  const endpoint = `/api/files/${id}`;
  const _actions = {
    start: actionCurry(c.PROJECT_PHOTO_DELETE_START),
    success: actionCurry(c.PROJECT_PHOTO_DELETE_SUCCESS),
    error: actionCurry(c.PROJECT_PHOTO_DELETE_ERROR)
  };

  return dispatch => {
    dispatch(_actions.start());
    return request(
      dispatch,
      endpoint,
      _actions.success,
      _actions.error,
      ajaxSettings({ method: "DELETE" }),
      successMessage
    );
  };
}

export function sendSurvey(projectId, emails, successMessage) {
  const endpoint = `/api/projects/survey/${projectId}`;
  const _actions = {
    start: actionCurry(c.PROJECT_SURVEY_EMAIL_START),
    success: actionCurry(c.PROJECT_SURVEY_EMAIL_SUCCESS),
    error: actionCurry(c.PROJECT_SURVEY_EMAIL_ERROR)
  };

  return dispatch => {
    dispatch(_actions.start());
    return request(
      dispatch,
      endpoint,
      _actions.success,
      _actions.error,
      ajaxSettings({
        method: "PUT",
        body: {
          commaDelimitedEmails: emails
        }
      }),
      successMessage
    );
  };
}

export function showSurveyModal(projectId, emails, survey) {
  return {
    type: c.SHOW_SURVEY_MODAL,
    id: projectId,
    emails,
    survey
  };
}

export function hideSurveyModal() {
  return {
    type: c.HIDE_SURVEY_MODAL
  };
}

export function toggleProjectImportModal(info) {
  return {
    type: c.TOGGLE_PROJECT_IMPORT_MODAL,
    info
  };
}

export function toggleProjectExportModal() {
  return {
    type: c.TOGGLE_PROJECT_EXPORT_MODAL
  };
}

export function toggleProjectReviewExportModal() {
  return {
    type: c.TOGGLE_PROJECT_REVIEW_EXPORT_MODAL
  };
}
