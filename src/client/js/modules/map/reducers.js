/*global google */
import * as c from './constants';
import {PROJECTS_MAP_DEFAULTS_GET_SUCCESS} from '../project/constants';
import {assign, merge, map, filter, each, uniqueId} from 'lodash';
import mapStyle from '../../utils/mapStyle';

const _mapOptions = {
  center: {
    lat: 38.8992646,
    lng: -77.1549957
  },
  zoomControlOptions: {
    position: google.maps.ControlPosition.RIGHT_CENTER
  },
  zoom: 10,
  maxZoom: 18,
  scaleControl: true,
  scrollwheel: false,
  streetViewControl: false,
  rotateControl: false,
  fullscreenControl: false,
  mapTypeControl: false,
  styles: mapStyle
};

const _mapFilters = {
  showing: false,
  activeFilters: [
    // {
    //   type: 'photo',
    //   value: null
    // },
    // {
    //   type: 'survey',
    //   value: null
    // }
  ]
};

const _userLocation = {
  isLoading: false
};

const _projectModal = {
  launched: false
};

const _mapChangeHash = uniqueId();

export function mapFilters(state = _mapFilters, action) {
  let newFilters;
  switch (action.type) {
    case c.SHOW_MAP_FILTER:
      return assign({}, state, {
        showing: true
      });
    case c.HIDE_MAP_FILTER:
      return assign({}, state, {
        showing: false
      });
    case c.REGISTER_MAP_FILTER:
      newFilters = map(state.activeFilters, a => a);
      newFilters.push({
        type: action.filterType,
        value: action.val
      });
      return assign({}, state, {
        activeFilters: newFilters
      });
    case c.UNREGISTER_MAP_FILTER:
      newFilters = filter(state.activeFilters, (obj) => {
        if (obj.type === action.filterType && obj.value === action.val) {
          return false;
        }
        return true;
      });
      return assign({}, state, {
        activeFilters: newFilters
      });
    default:
      return state;
  }
}

export function mapChangeHash(state = _mapChangeHash, action) {
  switch(action.type) {
    case c.UPDATE_MAP_ZOOM:
    case c.UPDATE_MAP_POSITION:
      state = uniqueId();
      return state;
    default:
      return state;
  }
}

export function mapOptions(state = _mapOptions, action) {
  switch (action.type) {
    case PROJECTS_MAP_DEFAULTS_GET_SUCCESS:
      return assign({}, state, {
        bounds: action.response.bbox
      });
    case c.UPDATE_MAP_ZOOM:
      return assign({}, state, {
        zoom: action.zoom
      });
    case c.UPDATE_MAP_POSITION:
      return assign({}, state, {
        center: {
          lat: action.lat,
          lng: action.lng
        }
      });
  }
  return state;
}

export function userLocation(state = _userLocation, action) {
  switch (action.type) {
    case c.FIND_USER_LOCATION:
      return assign({}, state, {
        isLoading: true
      });
    case c.USER_LOCATION_NOT_FOUND:
      return assign({}, state, {
        isLoading: false,
        error: action.error
      });
    case c.UPDATE_MAP_POSITION:
      return assign({}, state, {
        isLoading: false
      });
  }
  return state;
}

export function projectModal(state = _projectModal, action) {
  switch (action.type) {
    case c.LAUNCH_PROJECT_MODAL:
      return assign({}, state, {
        launched: true
      });
    case c.DISMISS_PROJECT_MODAL:
      return assign({}, state, {
        launched: false
      });
  }
  return state;
}
