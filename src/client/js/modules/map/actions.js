import * as c from './constants';
import {
  actionCurry,
  request,
  ajaxSettings,
  filterSortQueryStr
} from '../core';

export function showMapFilters() {
  return {
    type: c.SHOW_MAP_FILTER
  };
}

export function hideMapFilters() {
  return {
    type: c.HIDE_MAP_FILTER
  };
}

export function findUserLocation() {
  return (dispatch) => {
    dispatch({
      type: c.FIND_USER_LOCATION
    });
    navigator.geolocation.getCurrentPosition(
      (position) => {
        dispatch(updateMapPosition(
          position.coords.latitude,
          position.coords.longitude
        ));
        dispatch(updateMapZoom(12));
      },
      (error) => {
        return dispatch({
          type: c.USER_LOCATION_NOT_FOUND,
          error
        });
      }
    );
  };
}

export function registerProjectFilter(filterType, val) {
  return {
    type: c.REGISTER_MAP_FILTER,
    filterType,
    val
  };
}
export function unregisterProjectFilter(filterType, val) {
  return {
    type: c.UNREGISTER_MAP_FILTER,
    filterType,
    val
  };
}

export function updateMapPosition(lat, lng) {
  return {
    type: c.UPDATE_MAP_POSITION,
    lat,
    lng
  };
}

export function updateMapZoom(zoom) {
  return {
    type: c.UPDATE_MAP_ZOOM,
    zoom
  };
}

export function launchProjectModal() {
  return {
    type: c.LAUNCH_PROJECT_MODAL
  };
}

export function dismissProjectModal() {
  return {
    type: c.DISMISS_PROJECT_MODAL
  };
}
