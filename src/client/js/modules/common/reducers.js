import {assign} from 'lodash';
import * as c from './constants';

const USER_ADMIN = 0;
const OWNER = 1;
const ADMIN = 2;
const EDITOR = 3;
const GUEST = 4;

export function user(state = false, action) {
  return state;
}
export function menu(state = {}, action) {
  return state;
}
export function roles(state = [], action) {
  return state;
}
export function permissions(state = {}, action) {
  return {
    CAN_ADD_MEMBERS: [USER_ADMIN, OWNER, ADMIN],
    CAN_DELETE_MEMBERS: [USER_ADMIN, OWNER, ADMIN],
    CAN_DELETE_PROJECTS: [USER_ADMIN, OWNER, ADMIN],
    CAN_ADD_PROJECTS: [USER_ADMIN, OWNER, ADMIN, EDITOR],
    CAN_UPDATE_PROJECTS: [USER_ADMIN, OWNER, ADMIN, EDITOR],
    CAN_UPDATE_ORGNAME: [USER_ADMIN, OWNER, ADMIN],
    CAN_EXPORT_IMPORT_PROJECTS: [USER_ADMIN, OWNER, ADMIN],
  };
}
export function plans(state = [], action) {
  return state;
}
export function growler(state = {}, action) {
  switch (action.type) {
    case c.SHOW_GROWLER:
      return assign({}, state, {
        showing: true,
        kind: action.kind,
        message: action.message,
      });
    case c.HIDE_GROWLER:
      return assign({}, state, {
        showing: false,
      });
  }
  return state;
}
