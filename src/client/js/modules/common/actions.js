import * as c from './constants';

export function hideGrowler() {
  return {
    type: c.HIDE_GROWLER
  };
};

export function showGrowler(kind, message) {
  return {
    type: c.SHOW_GROWLER,
    kind,
    message
  };
};
