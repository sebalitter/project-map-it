import * as c from './constants';
import {
  actionCurry,
  request,
  ajaxSettings,
  filterSortQueryStr
} from '../core';

export function toggleUpgradeModal(plan) {
  return {
    type: c.SUBSCRIPTION_TOGGLE_UPGRADE_FORM_MODAL,
    plan
  };
}

export function subscribeToPlan(card = {}, plan = 'free') {
  const endpoint = `/api/subscriptions/subscribe`;
  const _actions = {
    start: actionCurry(c.PLAN_SUBSCRIBE_START),
    success: actionCurry(c.PLAN_SUBSCRIBE_SUCCESS),
    error: actionCurry(c.PLAN_SUBSCRIBE_ERROR)
  };
  const data = {
    card,
    plan
  };

  return (dispatch) => {
    dispatch(_actions.start());
    return request(
      dispatch,
      endpoint,
      _actions.success,
      _actions.error,
      ajaxSettings({ method: 'POST', body: data })
    );
  };
}

export function unsubscribeToPlan() {
  const endpoint = `/api/subscriptions/unsubscribe`;
  const _actions = {
    start: actionCurry(c.PLAN_UNSUBSCRIBE_START),
    success: actionCurry(c.PLAN_UNSUBSCRIBE_SUCCESS),
    error: actionCurry(c.PLAN_UNSUBSCRIBE_ERROR)
  };

  return (dispatch) => {
    dispatch(_actions.start());
    return request(
      dispatch,
      endpoint,
      _actions.success,
      _actions.error,
      ajaxSettings({ method: 'POST' })
    );
  };
}

export function fetchSubscriptionInfo() {
  const endpoint = `/api/subscriptions/info`;
  const _actions = {
    start: actionCurry(c.SUBSCRIPTION_INFO_START),
    success: actionCurry(c.SUBSCRIPTION_INFO_SUCCESS),
    error: actionCurry(c.SUBSCRIPTION_INFO_ERROR)
  };

  return (dispatch) => {
    dispatch(_actions.start());
    return request(
      dispatch,
      endpoint,
      _actions.success,
      _actions.error,
      ajaxSettings({ method: 'GET' })
    );
  };
}

export function submitPromo(promo, successMessage) {
  const endpoint = `/api/subscriptions/promo`;
  const _actions = {
    start: actionCurry(c.SUBMIT_PROMO_START),
    success: actionCurry(c.SUBMIT_PROMO_SUCCESS),
    error: actionCurry(c.SUBMIT_PROMO_ERROR)
  };

  const data = {
    promo
  };

  return (dispatch) => {
    dispatch(_actions.start());
    return request(
      dispatch,
      endpoint,
      _actions.success,
      _actions.error,
      ajaxSettings({ method: 'POST', body: data }),
      successMessage
    );
  };
}
