import {assign} from 'lodash';
import {LOCATION_CHANGE} from 'react-router-redux'
import * as c from './constants';
import {remoteReducer} from '../core';

export function subscribedPlan(state, action) {
  return remoteReducer('PLAN_SUBSCRIBE', state, action);
}

export function unsubscribedPlan(state, action) {
  return remoteReducer('PLAN_UNSUBSCRIBE', state, action);
}

export function updatedCard(state, action) {
  return remoteReducer('CARD_UPDATE', state, action);
}

export function fetchedSubscriptionInfo(state, action) {
  return remoteReducer('SUBSCRIPTION_INFO', state, action);
}

export function fetchedSubmittedPromo(state, action) {
  return remoteReducer('SUBMIT_PROMO', state, action);
}

const _subscriptionState = {
  showPaymentModal: false,
  toPlan: ''
};

export function subscriptionState(state = _subscriptionState, action) {
  switch (action.type) {
    case c.LOCATION_CHANGE:
      return _subscriptionState;
    case c.PLAN_SUBSCRIBE_SUCCESS:
      return assign({}, state, {
        showPaymentModal: false
      });
    case c.SUBSCRIPTION_TOGGLE_UPGRADE_FORM_MODAL:
      return assign({}, state, {
        showPaymentModal: !state.showPaymentModal,
        toPlan: action.plan
      });
    default:
      return state
  }
}
