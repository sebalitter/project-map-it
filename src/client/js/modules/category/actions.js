import * as c from './constants';
import {
  actionCurry,
  request,
  ajaxSettings,
  filterSortQueryStr
} from '../core';

const slug = 'project-categories';

export function fetchCategories(page = -1, filter = {}, sort = {}) {
  const endpoint = `/api/${slug}/${filterSortQueryStr(page, filter, sort)}`;
  const _actions = {
    start: actionCurry(c.CATEGORIES_GET_START),
    success: actionCurry(c.CATEGORIES_GET_SUCCESS),
    error: actionCurry(c.CATEGORIES_GET_ERROR)
  };

  return (dispatch) => {
    dispatch(_actions.start());
    return request(
      dispatch,
      endpoint,
      _actions.success,
      _actions.error,
      ajaxSettings({method: 'GET'})
    );
  };
}

export function fetchCategory(id) {
  const endpoint = `/api/${slug}/${id}`;
  const _actions = {
    start: actionCurry(c.CATEGORY_GET_START),
    success: actionCurry(c.CATEGORY_GET_SUCCESS),
    error: actionCurry(c.CATEGORY_GET_ERROR)
  };

  return (dispatch) => {
    dispatch(_actions.start());
    return request(
      dispatch,
      endpoint,
      _actions.success,
      _actions.error,
      ajaxSettings({method: 'GET'})
    );
  };
}

export function createCategory(vals, successMessage) {
  const endpoint = `/api/${slug}/`;
  const _actions = {
    start: actionCurry(c.CATEGORY_CREATE_START),
    success: actionCurry(c.CATEGORY_CREATE_SUCCESS),
    error: actionCurry(c.CATEGORY_CREATE_ERROR)
  };

  return (dispatch) => {
    dispatch(_actions.start());
    return request(
      dispatch,
      endpoint,
      _actions.success,
      _actions.error,
      ajaxSettings({method: 'POST', body: vals}),
      successMessage
    );
  };
}

export function updateCategory(id, vals, successMessage) {
  const endpoint = `/api/${slug}/${id}`;
  const _actions = {
    start: actionCurry(c.CATEGORY_UPDATE_START),
    success: actionCurry(c.CATEGORY_UPDATE_SUCCESS),
    error: actionCurry(c.CATEGORY_UPDATE_ERROR)
  };

  return (dispatch) => {
    dispatch(_actions.start());
    return request(
      dispatch,
      endpoint,
      _actions.success,
      _actions.error,
      ajaxSettings({method: 'PUT', body: vals}),
      successMessage
    );
  };
}

export function deleteCategory(id, successMessage) {
  const endpoint = `/api/${slug}/${id}`;
  const _actions = {
    start: actionCurry(c.CATEGORY_DELETE_START),
    success: actionCurry(c.CATEGORY_DELETE_SUCCESS),
    error: actionCurry(c.CATEGORY_DELETE_ERROR)
  };

  return (dispatch) => {
    dispatch(_actions.start());
    return request(
      dispatch,
      endpoint,
      _actions.success,
      _actions.error,
      ajaxSettings({method: 'DELETE'}),
      successMessage
    );
  };
}

export function addLocalCategory(name, parent = false) {
  return {
    type: c.ADD_CATEGORY,
    name,
    parent
  };
}

export function removeLocalCategory(uid) {
  return {
    type: c.REMOVE_CATEGORY,
    uid
  };
}

export function renameLocalCategory(uid, name) {
  return {
    type: c.RENAME_CATEGORY,
    uid,
    name
  };
}

export function selectTopLevelCategory(_id) {
  return {
    type: c.SELECT_TOP_LEVEL_CATEGORY,
    _id
  };
}
