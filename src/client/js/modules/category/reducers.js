import {remoteReducer} from '../core';
import * as c from './constants';
import {map, filter, assign, uniqueId, cloneDeep} from 'lodash';

export function fetchedCategories(state, action) {
  return remoteReducer('CATEGORIES_GET', state, action);
}

export function fetchedCategory(state, action) {
  return remoteReducer('CATEGORY_GET', state, action);
}

export function createdCategory(state, action) {
  return remoteReducer('CATEGORY_CREATE', state, action);
}

export function updatedCategory(state, action) {
  return remoteReducer('CATEGORY_UPDATE', state, action);
}

export function deletedCategory(state, action) {
  return remoteReducer('CATEGORY_DELETE', state, action);
}

const _editingCategories = {
  selectedCategoryId: false,
  categories: []
};

export function editingCategories(state = _editingCategories, action) {
  switch (action.type) {
    case c.SELECT_TOP_LEVEL_CATEGORY:
      return assign({}, state, {
        selectedCategoryId: action._id
      });
    case c.ADD_CATEGORY:
      const newCategories = cloneDeep(state.categories);
      newCategories.push({
        uid: uniqueId('cat'),
        _id: false,
        name: action.name,
        parent: action.parent ? action.parent : null
      });
      return assign({}, state, {
        categories: newCategories
      });
    case c.REMOVE_CATEGORY:
      const selected = state.selectedCategoryId;
      return assign({}, state, {
        selectedCategoryId: selected === action._id ? false : selected,
        categories: filter(state.categories, cat => cat.uid !== action.uid)
      });
    case c.RENAME_CATEGORY:
      return assign({}, state, {
        categories: map(state.categories, (cat) => {
          if (cat.uid === action.uid) {
            return assign({}, cat, {
              name: action.name
            });
          }
          return cat;
        })
      });
    case c.CATEGORIES_GET_SUCCESS: // Replenish cache.
      return assign({}, state, {
        categories: map(action.response.results, (cat) => {
          return {
            _id: cat._id,
            uid: uniqueId('savedcat'),
            name: cat.name,
            parent: cat.parent ? cat.parent : null
          };
        })
      });
    default:
      return state;
  }
}
