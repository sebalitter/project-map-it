import * as c from "./constants";
import {
  actionCurry,
  request,
  ajaxSettings,
  filterSortQueryStr
} from "../core";

export function fetchQuestions(questions) {
  const endpoint = `/api/questions/${filterSortQueryStr()}`;
  const _actions = {
    start: actionCurry(c.QUESTIONNAIRE_GET_START),
    success: actionCurry(c.QUESTIONNAIRE_GET_SUCCESS),
    error: actionCurry(c.QUESTIONNAIRE_GET_ERROR)
  };
  return dispatch => {
    dispatch(_actions.start());
    return request(
      dispatch,
      endpoint,
      _actions.success,
      _actions.error,
      ajaxSettings({ method: "GET" })
    );
  };
}

export function updateQuestions(questions, successMessage) {
  const endpoint = `/api/questions/`;
  const _actions = {
    start: actionCurry(c.QUESTIONNAIRE_UPDATE_START),
    success: actionCurry(c.QUESTIONNAIRE_UPDATE_SUCCESS),
    error: actionCurry(c.QUESTIONNAIRE_UPDATE_ERROR)
  };
  return dispatch => {
    dispatch(_actions.start());
    return request(
      dispatch,
      endpoint,
      _actions.success,
      _actions.error,
      ajaxSettings({ method: "PUT", body: questions }),
      successMessage
    );
  };
}
