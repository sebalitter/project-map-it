import {remoteReducer} from '../core';

export function fetchedQuestions(state, action) {
  return remoteReducer('QUESTIONNAIRE_GET', state, action);
}

export function updatedQuestions(state, action) {
  return remoteReducer('QUESTIONNAIRE_UPDATE', state, action);
}
