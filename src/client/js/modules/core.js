"use strict";

import fetch from "isomorphic-fetch";
import { assign, debounce } from "lodash";
import { routerReducer } from "react-router-redux";
import { reducer as formReducer } from "redux-form";
import { LOCATION_CHANGE } from "react-router-redux";
import { showGrowler, hideGrowler } from "./common/actions";
import * as commonReducers from "./common/reducers";
import * as memberReducers from "./member/reducers";
import * as mapReducers from "./map/reducers";
import * as subscriptionReducers from "./subscription/reducers";
import * as questionnaireReducers from "./questionnaire/reducers";
import * as projectReducers from "./project/reducers";
import * as categoryReducers from "./category/reducers";
import * as accountReducers from "./account/reducers";
import * as organizationReducers from "./organization/reducers";

/**
 * Used by reducers.
 */
export const reducers = assign(
  memberReducers,
  commonReducers,
  mapReducers,
  subscriptionReducers,
  questionnaireReducers,
  projectReducers,
  categoryReducers,
  accountReducers,
  organizationReducers,
  {
    routing: routerReducer,
    form: formReducer
  }
);

export function serverResponse(props = {}) {
  return assign(
    {},
    {
      isLoading: false,
      errors: [],
      response: {},
      type: null
    },
    props
  );
}

export function remoteReducer(typePart, state, action, persist) {
  const START = `${typePart}_START`;
  const SUCCESS = `${typePart}_SUCCESS`;
  const ERROR = `${typePart}_ERROR`;

  switch (action.type) {
    case START:
      if (persist) {
        return serverResponse({
          response: state.response,
          isLoading: action.suppressLoading ? false : true,
          type: action.type
        });
      }
      return serverResponse({
        isLoading: action.suppressLoading ? false : true,
        type: action.type
      });
    case SUCCESS:
      return serverResponse({
        response: action.response,
        type: action.type
      });
    case ERROR:
      return serverResponse({
        errors: action.errors,
        type: action.type
      });
    case LOCATION_CHANGE:
      switch (action.payload.action) {
        case "PUSH":
        case "POP":
          return serverResponse();
      }
  }
  if (!state) {
    return serverResponse();
  }
  return state;
}

/**
 * Used by actions.
 */
export function actionCurry(type) {
  return (obj = {}) => {
    const action = {
      type
    };
    return assign({}, action, obj);
  };
}

export function ajaxSettings(settings = {}) {
  return assign(
    {},
    {
      credentials: "same-origin",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      method: "GET"
    },
    settings
  );
}

export function filterSortQueryStr(page = -1, filter = {}, sort = {}) {
  const parts = [];
  const f = JSON.stringify(filter);
  const s = JSON.stringify(sort);
  return `?page=${page}&filter=${f}&sort=${s}`;
}

const _hideGrowler = debounce(function(dispatch) {
  dispatch(hideGrowler());
}, 5000);

export function request(
  dispatch,
  endpoint,
  receive,
  error,
  fetchOpts = {},
  successMessage,
  suppressLoading
) {
  const contentType =
    fetchOpts.headers && fetchOpts.headers["Content-Type"]
      ? fetchOpts.headers["Content-Type"]
      : false;

  if (fetchOpts.body && contentType === "application/json") {
    fetchOpts.body = JSON.stringify(fetchOpts.body);
  }

  let statusCode;

  const handleResponse = function(res) {
    statusCode = res.status;
    return res;
  };

  const parseJson = function(res) {
    return res.json();
  };

  const handleJson = function(json) {
    if (statusCode >= 200 && statusCode < 300) {
      dispatch(
        receive({
          response: json
        })
      );

      if (successMessage) {
        dispatch(showGrowler("info", successMessage));
        _hideGrowler(dispatch);
      }
    } else if (json.errors) {
      dispatch(
        error({
          errors: json.errors
        })
      );

      dispatch(showGrowler("error", json.errors));
      _hideGrowler(dispatch);
    } else {
      throw new Error();
    }
  };

  const loc = window.location;
  return fetch(`${loc.protocol}//${loc.host}${endpoint}`, fetchOpts)
    .then(handleResponse)
    .then(parseJson)
    .then(handleJson)
    .catch(err => {
      if (window.console) {
        console.warn("There was a parsing error from the server. Error:", err);
      }

      dispatch(
        error({
          errors: [
            "There may be a internet connection issue. Please try again."
          ]
        })
      );
      dispatch(
        showGrowler(
          "error",
          "There may be a internet connection issue. Please try again."
        )
      );
      _hideGrowler(dispatch);
    });
}
