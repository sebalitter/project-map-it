import * as c from "./constants";
import {
  actionCurry,
  request,
  ajaxSettings,
  filterSortQueryStr
} from "../core";

export function fetchMembers() {
  const endpoint = `/api/users/${filterSortQueryStr()}`;
  const _actions = {
    start: actionCurry(c.USERS_GET_START),
    success: actionCurry(c.USERS_GET_SUCCESS),
    error: actionCurry(c.USERS_GET_ERROR)
  };

  return dispatch => {
    dispatch(_actions.start());
    return request(
      dispatch,
      endpoint,
      _actions.success,
      _actions.error,
      ajaxSettings({ method: "GET" })
    );
  };
}

export function fetchMember(id) {
  const endpoint = `/api/users/${id}`;
  const _actions = {
    start: actionCurry(c.USER_GET_START),
    success: actionCurry(c.USER_GET_SUCCESS),
    error: actionCurry(c.USER_GET_ERROR)
  };

  return dispatch => {
    dispatch(_actions.start());
    return request(
      dispatch,
      endpoint,
      _actions.success,
      _actions.error,
      ajaxSettings({ method: "GET" })
    );
  };
}

export function createMember(vals, successMessage) {
  const endpoint = `/api/users/`;
  const _actions = {
    start: actionCurry(c.USER_CREATE_START),
    success: actionCurry(c.USER_CREATE_SUCCESS),
    error: actionCurry(c.USER_CREATE_ERROR)
  };

  return dispatch => {
    dispatch(_actions.start());
    return request(
      dispatch,
      endpoint,
      _actions.success,
      _actions.error,
      ajaxSettings({ method: "POST", body: vals }),
      successMessage
    );
  };
}

export function updateMember(id, vals, successMessage) {
  const endpoint = `/api/users/${id}`;
  const _actions = {
    start: actionCurry(c.USER_UPDATE_START),
    success: actionCurry(c.USER_UPDATE_SUCCESS),
    error: actionCurry(c.USER_UPDATE_ERROR)
  };

  return dispatch => {
    dispatch(_actions.start());
    return request(
      dispatch,
      endpoint,
      _actions.success,
      _actions.error,
      ajaxSettings({ method: "PUT", body: vals }),
      successMessage
    );
  };
}

export function deleteMember(id, successMessage) {
  const endpoint = `/api/users/${id}`;
  const _actions = {
    start: actionCurry(c.USER_DELETE_START),
    success: actionCurry(c.USER_DELETE_SUCCESS),
    error: actionCurry(c.USER_DELETE_ERROR)
  };

  return dispatch => {
    dispatch(_actions.start());
    return request(
      dispatch,
      endpoint,
      _actions.success,
      _actions.error,
      ajaxSettings({ method: "DELETE" }),
      successMessage
    );
  };
}

export function inviteMember(id, successMessage) {
  const endpoint = `/api/users/invite/${id}`;
  const _actions = {
    start: actionCurry(c.USER_INVITE_START),
    success: actionCurry(c.USER_INVITE_SUCCESS),
    error: actionCurry(c.USER_INVITE_ERROR)
  };

  return dispatch => {
    dispatch(_actions.start());
    return request(
      dispatch,
      endpoint,
      _actions.success,
      _actions.error,
      ajaxSettings({ method: "POST" }),
      successMessage
    );
  };
}
