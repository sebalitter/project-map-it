import {remoteReducer} from '../core';
import {LOCATION_CHANGE} from 'react-router-redux'
import * as c from './constants';

export function fetchedMembers(state, action) {
  return remoteReducer('USERS_GET', state, action);
}

export function fetchedMember(state, action) {
  return remoteReducer('USER_GET', state, action);
}

export function createdMember(state, action) {
  return remoteReducer('USER_CREATE', state, action);
}

export function updatedMember(state, action) {
  return remoteReducer('USER_UPDATE', state, action);
}

export function deletedMember(state, action) {
  return remoteReducer('USER_DELETE', state, action);
}

export function invitedMember(state, action) {
  return remoteReducer('USER_INVITE', state, action);
}
