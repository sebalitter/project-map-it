import { remoteReducer } from "../core";

export function fetchedOrganization(state, action) {
  return remoteReducer("ORG_GET", state, action);
}

export function updatedOrganization(state, action) {
  return remoteReducer("ORG_UPDATE", state, action);
}
