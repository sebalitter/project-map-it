import * as c from "./constants";
import {
  actionCurry,
  request,
  ajaxSettings,
  filterSortQueryStr
} from "../core";

export function fetchOrganization(id) {
  const endpoint = `/api/organizations/${id}`;
  const _actions = {
    start: actionCurry(c.ORG_GET_START),
    success: actionCurry(c.ORG_GET_SUCCESS),
    error: actionCurry(c.ORG_GET_ERROR)
  };

  return dispatch => {
    dispatch(_actions.start());
    return request(
      dispatch,
      endpoint,
      _actions.success,
      _actions.error,
      ajaxSettings({ method: "GET" })
    );
  };
}

export function updateOrganization(vals, successMessage) {
  const endpoint = `/api/organizations`;
  const _actions = {
    start: actionCurry(c.ORG_UPDATE_START),
    success: actionCurry(c.ORG_UPDATE_SUCCESS),
    error: actionCurry(c.ORG_UPDATE_ERROR)
  };

  return dispatch => {
    dispatch(_actions.start());
    return request(
      dispatch,
      endpoint,
      _actions.success,
      _actions.error,
      ajaxSettings({ method: "PUT", body: vals }),
      successMessage
    );
  };
}
