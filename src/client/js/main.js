import "babel-polyfill";

import React from "react";
import { createBrowserHistory } from "history";
import { render } from "react-dom";
import { Provider } from "react-redux";
import { Router, Route, Link, browserHistory, Redirect } from "react-router";
import { syncHistoryWithStore } from "react-router-redux";
import configureStore from "./configureStore";

import injectTapEventPlugin from "react-tap-event-plugin";
injectTapEventPlugin();

import Map from "./containers/Map";
import Projects from "./containers/Projects";
import EditUser from "./containers/EditUser";
import CreateUser from "./containers/CreateUser";
import EditOrg from "./containers/EditOrg";
import EditProject from "./containers/EditProject";
import CreateProject from "./containers/CreateProject";
import Subscription from "./containers/Subscription";
import EditCategories from "./containers/EditCategories";
import QuestionnaireMgmt from "./containers/QuestionnaireMgmt";
import Team from "./containers/Team";
import Account from "./containers/Account";
import Embed from "./containers/Embed";
import Four0Four from "./components/Four0Four";

const store = configureStore(window.initialState || {});
const history = syncHistoryWithStore(browserHistory, store);

// Old IE. Maybe not necessary?
if (!window.location.origin) {
  window.location.origin =
    window.location.protocol +
    "//" +
    window.location.hostname +
    (window.location.port ? ":" + window.location.port : "");
}

render(
  <Provider store={store}>
    <Router history={history}>
      <Route path="/" component={Map} />
      <Route path="public/:slug" component={Map} />

      <Redirect from="projects" to="projects/manage" />
      <Redirect from="team" to="team/manage" />
      <Redirect from="account" to="account/manage" />

      <Route path="survey" component={QuestionnaireMgmt} />

      <Route path="projects">
        <Route path="manage" component={Projects} />
        <Route path="create" component={CreateProject} />
        <Route path="edit/:projectId" component={EditProject} />
        <Route path="categories" component={EditCategories} />
      </Route>

      <Route path="team">
        <Route path="manage" component={Team} />
        <Route path="create" component={CreateUser} />
        <Route path="edit/:userId" component={EditUser} />
      </Route>

      <Route path="account">
        <Route path="manage" component={Account} />
        <Route path="organization" component={EditOrg} />
        <Route path="subscription" component={Subscription} />
        <Route path="embed" component={Embed} />
      </Route>

      <Route path="*" component={Four0Four} />
    </Router>
  </Provider>,
  document.getElementById("app")
);
