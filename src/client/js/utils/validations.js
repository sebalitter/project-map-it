import { isEmpty, isInteger } from "lodash";

export function email(value) {
  if (
    !isEmpty(value) &&
    !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
  ) {
    return "Invalid email address";
  }
}

export function validEmail(val) {
  if (!isEmpty(val) && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(val)) {
    return false;
  }
  return true;
}

export function containsNumbers(str) {
  if (str.match(/\d+/g)) {
    return true;
  }
  return false;
}

export function required(value) {
  if (isEmpty(value)) {
    return "Required";
  }
}

export function minLength(min) {
  return value => {
    if (!isEmpty(value) && value.length < min) {
      return `Must be at least ${min} characters`;
    }
  };
}

export function maxLength(max) {
  return value => {
    if (!isEmpty(value) && value.length > max) {
      return `Must be no more than ${max} characters`;
    }
  };
}

export function integer(value) {
  if (!isInteger(Number(value))) {
    return "Must be an integer";
  }
}

export function Card(cardNumber) {
  this.num = cardNumber + "";
  this.type = "default";
  this.useLuhn = true;
}

Card.prototype = {
  _cards: {
    amex: {
      prefix: /^3[47]/,
      size: [15]
    },
    dinersclub: {
      prefix: /^(3[6-9]|30([0-5]|9))/,
      size: [14, 16]
    },
    discover: {
      prefix: /^(6(5|011|4[4-9]|22))/,
      size: [16]
    },
    jcb: {
      prefix: /^35(2[89]|[3-8][0-9])/,
      size: [15, 16]
    },
    maestro: {
      prefix: /^(50(18|2|3)|5[68]|6(304|7))/,
      size: [12, 19]
    },
    mastercard: {
      prefix: /^5[1-5]/,
      size: [16]
    },
    unionpay: {
      prefix: /^62/,
      size: [16, 19],
      luhn: false
    },
    visa: {
      prefix: /^4/,
      size: [13, 16]
    },
    default: {
      prefix: /^\d{1,4}/g,
      size: [13, 19]
    }
  },

  _luhnlookup: [0, 2, 4, 6, 8, 1, 3, 5, 7, 9],

  getType: function() {
    for (var c in this._cards) {
      if (this.num.match(this._cards[c].prefix)) {
        if ("luhn" in this._cards[c]) {
          this.useLuhn = this._cards[c].luhn;
        }
        this.type = c;
        break;
      }
    }
    return this.type;
  },

  isValid: function() {
    var len = this._cards[this.getType()].size;

    if (this.num.length >= len[0] && this.num.length <= len[len.length - 1]) {
      return this.useLuhn ? this._luhnCheck() : true;
    }

    return false;
  },

  _luhnCheck: function() {
    var sum = 0,
      i = this.num.length,
      odd = true;

    while (i--) {
      sum += (odd = !odd)
        ? this._luhnlookup[this.num.charAt(i)]
        : this.num.charAt(i) | 0;
    }

    return sum % 10 == 0;
  }
};
