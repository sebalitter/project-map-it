/* global google */
const LatLngBounds = google.maps.LatLngBounds;
const Point = google.maps.Point;

export function getBoundsZoomLevel(mapDims, bounds) {
  const WORLD_DIM = { height: 256, width: 256 };
  const ZOOM_MAX = 21;

  function latRad(lat) {
    const sin = Math.sin(lat * Math.PI / 180);
    const radX2 = Math.log((1 + sin) / (1 - sin)) / 2;
    return Math.max(Math.min(radX2, Math.PI), -Math.PI) / 2;
  }

  function zoom(mapPx, worldPx, fraction) {
    return Math.floor(Math.log(mapPx / worldPx / fraction) / Math.LN2);
  }

  const ne = bounds.getNorthEast();
  const sw = bounds.getSouthWest();
  const latFraction = (latRad(ne.lat()) - latRad(sw.lat())) / Math.PI;
  const lngDiff = ne.lng() - sw.lng();
  const lngFraction = ((lngDiff < 0) ? (lngDiff + 360) : lngDiff) / 360;
  const latZoom = zoom(mapDims.height, WORLD_DIM.height, latFraction);
  const lngZoom = zoom(mapDims.width, WORLD_DIM.width, lngFraction);

  return Math.min(latZoom, lngZoom, ZOOM_MAX);
}

export function paddedBounds(map, padding) {
  const b = map.getBounds();
  const zoom = Math.pow(2, map.getZoom());
  const proj = map.getProjection();
  const swLatLng = b.getSouthWest();
  const neLatLng = b.getNorthEast();
  const swPoint = proj.fromLatLngToPoint(swLatLng);
  const nePoint = proj.fromLatLngToPoint(neLatLng);
  const bounds = new LatLngBounds(
    swLatLng,
    neLatLng
  );
  bounds.extend(
    proj.fromPointToLatLng(new Point(
      swPoint.x - padding / zoom,
      swPoint.y + padding / zoom
    ))
  );
  bounds.extend(
    proj.fromPointToLatLng(new Point(
      nePoint.x + padding / zoom,
      nePoint.y - padding / zoom
    ))
  );
  return bounds;
}
