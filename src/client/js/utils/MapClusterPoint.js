/*global google */
const OverlayView = google.maps.OverlayView;

export default class MapClusterPoint extends OverlayView {
  constructor(map, position, content) {
    super();
    this.setValues({
      position: new google.maps.LatLng(position[0], position[1]),
      container: null,
      content: content,
      map: map
    });
    this.__onClick = () => google.maps.event.trigger(this, "click");
  }
  onAdd() {
    const div = document.createElement("div");
    const span = document.createElement("span");
    const num = parseInt(this.get("content"), 10);
    div.style.position = "absolute";
    div.className = "cluster-point";

    if (num > 0 && num <= 5) {
      div.className += ` size_small`;
    } else if (num > 5 && num <= 13) {
      div.className += ` size_medium`;
    } else if (num > 13 && num <= 22) {
      div.className += ` size_large`;
    } else if (num > 22) {
      div.className += ` size_huge`;
    }

    span.innerHTML = this.get("content");
    div.appendChild(span);
    this.set("container", div);

    google.maps.event.addDomListener(div, "click", () =>
      google.maps.event.trigger(this, "click")
    );
    google.maps.event.addDomListener(div, "touchstart", () =>
      google.maps.event.trigger(this, "click")
    );

    this.getPanes().overlayMouseTarget.appendChild(div);
  }
  draw() {
    var pos = this.getProjection().fromLatLngToDivPixel(this.get("position"));
    this.get("container").style.left = pos.x + "px";
    this.get("container").style.top = pos.y + "px";
  }
  onRemove() {
    // Clear dom listeners from memory.
    google.maps.event.clearListeners(this.get("container"), "click");
    google.maps.event.clearListeners(this.get("container"), "touchstart");

    // Remove div from the dom.
    this.get("container").parentNode.removeChild(this.get("container"));
    this.set("container", null);
  }
}
