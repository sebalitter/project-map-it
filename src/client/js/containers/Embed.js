import React, { Component } from "react";
import { findDOMNode } from "react-dom";
import { isEmpty, concat, assign, defer } from "lodash";
import { connect } from "react-redux";
import BasePage from "../components/BasePage";
import Loader from "../components/Loader";
import { fetchMember } from "../modules/member/actions";

class Embed extends Component {
  render() {
    const { user, isLoading, embedCode, iframeUrl } = this.props;

    const onClick = e => {
      defer(() => findDOMNode(this.refs.embed).select());
    };

    return (
      <BasePage menu="account">
        <h1>Embed Code</h1>
        <p>Use the code below to copy and paste in to a blog or website.</p>
        <Loader isLoading={isLoading}>
          <div>
            <div className="embed-code">
              <textarea
                onClick={onClick}
                ref="embed"
                value={embedCode}
                readOnly
              />
            </div>
            <iframe
              src={iframeUrl}
              style={{ width: "100%" }}
              height={650}
              allowFullScreen={true}
            />
          </div>
        </Loader>
      </BasePage>
    );
  }
}

const mapStateToProps = function(state, ownProps) {
  const url = `${window.location.origin}/public/${
    state.user.organization.slug
  }`;
  let embedCode = `<iframe src="${url}" `;
  embedCode +=
    'style="width:100%" frameborder="0" height="650" allowfullscreen></iframe>';

  return {
    iframeUrl: url,
    isLoading: state.fetchedMember.isLoading,
    embedCode
  };
};

export default connect(mapStateToProps)(Embed);
