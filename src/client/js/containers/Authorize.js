import React, { Component, createElement } from "react";
import { connect } from "react-redux";
import { assign, indexOf, omit } from "lodash";
import PropTypes from "prop-types";

/**
 * This class can be used to authorize a specific component. It should not be
 * used to authorize an entire page container for redirect.
 *
 * <Authorize roles={[]} Component={MyComponent} {...componentOpts} />
 *  - or -
 * <Authorize roles={[]}>Other child components</Authorize>
 */
class Authorize extends Component {
  render() {
    if (indexOf(this.props.roles, this.props._userRole) !== -1) {
      if (this.props.Component) {
        return createElement(
          this.props.Component,
          omit(this.props, ["roles", "Component", "_userRole"])
        );
      } else {
        return this.props.children;
      }
    }
    return null;
  }
}

Authorize.propTypes = {
  Component: PropTypes.func,
  roles: PropTypes.array.isRequired,
  _userRole: PropTypes.number.isRequired
};

const mapStateToProps = function(state, ownProps) {
  return assign({}, ownProps, {
    _userRole: state.user ? state.user.role : -1 // Default to none.
  });
};

export default connect(mapStateToProps)(Authorize);
