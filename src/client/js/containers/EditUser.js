import React, { Component } from "react";
import { isEmpty, isArray } from "lodash";
import { connect } from "react-redux";
import UserForm from "./forms/UserForm";
import Loader from "../components/Loader";
import BasePage from "../components/BasePage";
import {
  fetchMember,
  updateMember,
  deleteMember
} from "../modules/member/actions";

class EditUser extends Component {
  componentDidMount() {
    this.props.load();
  }
  render() {
    const { isLoading, onSubmit, userFormProps } = this.props;
    return (
      <BasePage menu="team">
        <h1>{userFormProps.newUser ? "Create Member" : "Edit Member"}</h1>

        <Loader isLoading={isLoading}>
          <UserForm
            isOwner={userFormProps.initialValues.role === 1}
            onSubmit={onSubmit}
            {...userFormProps}
          />
        </Loader>
      </BasePage>
    );
  }
}

const mapStateToProps = function(state, ownProps) {
  const memberIsLoading = state.fetchedMember.isLoading;
  const loadedMember = state.fetchedMember.response;

  const requesting = state.updatedMember.isLoading;
  const updatedMember = state.updatedMember.response;

  const props = {
    isLoading: memberIsLoading,
    userFormProps: {
      requesting: requesting,
      initialValues: loadedMember,
      newUser: false
    }
  };

  return props;
};

const mapDispatchToProps = function(dispatch, ownProps) {
  return {
    load: () => dispatch(fetchMember(ownProps.params.userId)),
    onSubmit: values => {
      dispatch(updateMember(ownProps.params.userId, values, "Member updated."));
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(EditUser);
