import React, { Component } from "react";
import { isEmpty, isArray } from "lodash";
import { connect } from "react-redux";
import UserForm from "./forms/UserForm";
import { browserHistory } from "react-router";
import Loader from "../components/Loader";
import BasePage from "../components/BasePage";
import { createMember } from "../modules/member/actions";

class CreateUser extends Component {
  render() {
    const { isLoading, errors, info, onSubmit, userFormProps } = this.props;

    return (
      <BasePage menu="team">
        <h1>Create Member</h1>

        <Loader isLoading={isLoading}>
          <UserForm onSubmit={onSubmit} {...userFormProps} />
        </Loader>
      </BasePage>
    );
  }
}

const mapStateToProps = function(state, ownProps) {
  const createdResponse = state.createdMember.response;

  if (!isEmpty(createdResponse)) {
    browserHistory.push(`/team/manage`);
  }

  return {
    isLoading: false,
    userFormProps: {
      requesting: state.createdMember.isLoading,
      initialValues: {
        role: "3"
      },
      newUser: true
    }
  };
};

const mapDispatchToProps = function(dispatch, ownProps) {
  return {
    onSubmit: values => {
      dispatch(createMember(values, "Member created."));
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CreateUser);
