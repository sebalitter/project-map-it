import React, { Component } from "react";
import { connect } from "react-redux";
import { difference, size, defer, isArray } from "lodash";
import { hideGrowler } from "../modules/common/actions";

class Growler extends Component {
  render() {
    if (!this.props.showing) {
      return null;
    }
    let messages;

    if (isArray(this.props.message)) {
      messages = (
        <ul>{this.props.message.map((str, i) => <li key={i}>{str}</li>)}</ul>
      );
    } else {
      messages = this.props.message;
    }

    return (
      <div className={`growler-message ${this.props.kind}`}>
        {messages}
        <button onClick={this.props.hide} className="growler-close">
          <i className="fa fa-times" />
        </button>
      </div>
    );
  }
}

const mapStateToProps = function(state, ownProps) {
  return {
    message: state.growler.message,
    kind: state.growler.kind,
    showing: state.growler.showing
  };
};

const mapDispatchToProps = function(dispatch) {
  return {
    hide: function() {
      dispatch(hideGrowler());
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Growler);
