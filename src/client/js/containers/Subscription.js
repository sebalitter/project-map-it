import React, { Component } from "react";
import { isEmpty, omit, get, map } from "lodash";
import { connect } from "react-redux";
import moment from "moment";
import SubscriptionForm from "./forms/SubscriptionForm";
import UpgradePlanForm from "./forms/UpgradePlanForm";
import Charges from "../components/Charges";
import BasePage from "../components/BasePage";
import BaseModal from "../components/BaseModal";
import {
  toggleUpgradeModal,
  subscribeToPlan,
  unsubscribeToPlan,
  submitPromo,
  fetchSubscriptionInfo
} from "../modules/subscription/actions";
import { fetchMember } from "../modules/member/actions";

const VIMEO_OPTS = "?autoplay=1&color=ff0179&title=0&byline=0&portrait=0";
const VIMEO_RATIO = 640 / 360;

class Subscription extends Component {
  constructor(props) {
    super(props);
    this.state = {
      iframeWidth: 0,
      iframeHeight: 0
    };
    this.handlePromoSubmit = this.handlePromoSubmit.bind(this);
  }
  componentDidMount() {
    this.props.load(this.props.userId);

    const state = {};
    const parent = this.refs.iframeParent;

    this.setState({
      iframeWidth: parent.offsetWidth,
      iframeHeight: parent.offsetWidth / VIMEO_RATIO
    });

    window.addEventListener("resize", () => {
      this.setState({
        iframeWidth: parent.offsetWidth,
        iframeHeight: parent.offsetWidth / VIMEO_RATIO
      });
    });
  }
  handlePromoSubmit(e) {
    e.preventDefault();
    this.props.submitPromo(this.props.userId, this.refs.promoCodeInput.value);
    this.refs.promoCodeInput.value = "";
  }
  render() {
    const {
      subscriptionProps,
      chargesProps,
      modalProps,
      user,
      upgradeNeeded,
      coupon,
      showPaymentModal,
      paymentMessage
    } = this.props;

    return (
      <BasePage menu="account">
        <h1>Subscription</h1>
        <div className="col-contain">
          <div className="half">
            {/*
              The user needs to upgrade and has not yet put in a promo code.
            */}
            {upgradeNeeded && !coupon ? (
              <form
                className="enter-promo-form"
                onSubmit={this.handlePromoSubmit}
              >
                <p>Have a promo code?</p>
                <input
                  type="text"
                  placeholder="Promo code here"
                  ref="promoCodeInput"
                />
                <button type="submit">Apply</button>
              </form>
            ) : null}

            {map(paymentMessage, (obj, idx) => {
              return (
                <div className={"info " + obj.className} key={idx}>
                  <p
                    dangerouslySetInnerHTML={{
                      __html: obj.message
                    }}
                  />
                </div>
              );
            })}

            <SubscriptionForm
              {...subscriptionProps}
              subscriptionClicked={this.props.togglePaymentModal}
              unsubscribeClicked={this.props.unsubscribeAccount}
            />

            <br />
            <br />
            <p>
              <b>Still have questions?</b> Give us a call directly at
              202-800-0882 or email at{" "}
              <a href="mailto:info@projectmapit.com">info@projectmapit.com</a>.
            </p>
          </div>
          <div className="half" ref="iframeParent">
            {upgradeNeeded ? (
              <div>
                <iframe
                  src={`https://player.vimeo.com/video/217399526${VIMEO_OPTS}`}
                  width={this.state.iframeWidth}
                  height={this.state.iframeHeight}
                  frameBorder="0"
                  allowFullScreen
                />

                <div className="no-contract">
                  <h5>Free Setup</h5>
                  <p>
                    We understand that you're busy. To help get you up to speed,
                    we'll give you a call and import your projects, users, and
                    even embed the map on your website for you.
                  </p>
                  <h5>No Contract</h5>
                  <p>
                    If you aren't completely satisfied, you may cancel at any
                    time.
                  </p>
                </div>
              </div>
            ) : (
              <Charges {...chargesProps} />
            )}
          </div>
        </div>

        <BaseModal
          onExit={this.props.togglePaymentModal}
          className="payment-modal"
          display={showPaymentModal}
        >
          <UpgradePlanForm {...modalProps} onSubmit={this.props.onSubmit} />
        </BaseModal>
      </BasePage>
    );
  }
}

const mapStateToProps = function(state, ownProps) {
  const user = get(state, "fetchedMember.response");
  const info = get(state, "fetchedSubscriptionInfo.response");
  const charges = get(
    state,
    "fetchedSubscriptionInfo.response.charges.data",
    []
  );
  const coupon = get(state, "fetchedSubscriptionInfo.response.discount.coupon");

  const upgradeNeeded = !state.user.organization.active;
  const hasCardOnFile = info && info.default_source;

  const paymentMessage = [];

  // If there is an (un)subscription, reload the page so the initial app state
  // is correct.
  if (
    !isEmpty(state.unsubscribedPlan.response) ||
    !isEmpty(state.subscribedPlan.response)
  ) {
    window.location = window.location.href;
  }

  if (!state.fetchedSubscriptionInfo.isLoading) {
    // Format sentence around the discount.
    if (coupon && !hasCardOnFile) {
      paymentMessage.push({
        message: `You have an active coupon for ${
          coupon.percent_off
        }% off any plan.`,
        className: "discount"
      });
    }

    if (upgradeNeeded) {
      paymentMessage.push({
        message:
          "Your account is currently inactive due to having no valid payment method on file. Reactivate your account by selecting a subscription below and entering a payment method.",
        className: "action-required"
      });
    } else if (get(user, "organization.trialing")) {
      if (hasCardOnFile) {
        paymentMessage.push({
          message:
            "To change your subscription or edit your preferred method of payment, select a subscription below.",
          className: "action-required"
        });
      } else {
        paymentMessage.push({
          message:
            "You currently have no payment method on file - add one by choosing a subscription below to ensure your account remains active. You will NOT be charged until your trial period is over.",
          className: "action-required"
        });
      }
    } else {
      paymentMessage.push({
        message:
          "To change your subscription or edit your preferred method of payment, select a subscription below.",
        className: "action-required"
      });
    }
  }

  return {
    paymentMessage,
    coupon,
    user,
    upgradeNeeded,
    userId: state.user._id,
    showPaymentModal: state.subscriptionState.showPaymentModal,
    chargesProps: {
      charges,
      isLoading: state.fetchedSubscriptionInfo.isLoading
    },
    subscriptionProps: {
      currentCard: "",
      info,
      user: state.user,
      currentPlan: state.user.organization.plan,
      isLoading: state.unsubscribedPlan.isLoading
    },
    modalProps: {
      toPlan: state.subscriptionState.toPlan,
      isLoading: state.subscribedPlan.isLoading,
      responseErrors: state.subscribedPlan.errors,
      currentPlan: state.user.organization.plan,
      info,
      initialValues: {
        name: state.user.name,
        toPlan: state.subscriptionState.toPlan
      }
    }
  };
};

const mapDispatchToProps = function(dispatch, ownProps) {
  return {
    load: id => {
      dispatch(fetchMember(id));
      dispatch(fetchSubscriptionInfo());
    },
    togglePaymentModal: plan => dispatch(toggleUpgradeModal(plan)),
    onSubmit: function(fields) {
      const fieldsToSave = omit(fields, ["toPlan"]);
      dispatch(subscribeToPlan(fieldsToSave, fields.toPlan));
    },
    submitPromo: (id, promo) => {
      dispatch(submitPromo(promo, "Your promo code has been applied!")).then(
        () => {
          dispatch(fetchMember(id));
        }
      );
    },
    unsubscribeAccount: () => {
      if (confirm("Are you sure you want to unsubscribe?")) {
        dispatch(unsubscribeToPlan());
      }
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Subscription);
