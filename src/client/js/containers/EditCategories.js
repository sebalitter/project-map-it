import React, { Component } from "react";
import { isEmpty, isArray, concat, map, find, orderBy } from "lodash";
import { browserHistory } from "react-router";
import { connect } from "react-redux";
import Loader from "../components/Loader";
import BasePage from "../components/BasePage";
import SubCatSelect from "../components/SubCatSelect";
import SubCatPill from "../components/SubCatPill";
import Select from "react-select";
import {
  fetchCategories,
  createCategory,
  updateCategory,
  deleteCategory,
  addLocalCategory,
  removeLocalCategory,
  renameLocalCategory,
  selectTopLevelCategory
} from "../modules/category/actions";

class EditCategories extends Component {
  componentDidMount() {
    this.props.load();
  }
  addCategory(e) {
    e.preventDefault();
    this.props.add();
  }
  removeCategory(uid, _id) {
    if (
      confirm(
        "Are you sure you want to remove this category? All sub categories of this will be removed as well. You can not undo this action."
      )
    ) {
      this.props.remove(uid, _id);
    }
  }
  addCategoryWithParent(e) {
    e.preventDefault();
    const _id = this.props.selectedCategoryId;
    const uid = find(this.props.categories, cat => cat._id === _id).uid;
    this.props.addWithParent(uid, _id);
  }
  onPillSingleClick(_id) {
    this.props.selectParent(_id);
  }
  renderSubPills() {
    const { selectedCategoryId, categories } = this.props;
    const subLevelFilter = cat => cat.parent === selectedCategoryId;
    if (selectedCategoryId) {
      return (
        <div>
          <div className="pills sub">
            {categories.filter(subLevelFilter).map((cat, idx) => {
              return (
                <SubCatPill
                  key={idx + cat.name + cat.uid + (cat._id || "")}
                  name={cat.name}
                  uid={cat.uid}
                  parent={cat.parent}
                  _id={cat._id}
                  isActive={false}
                  onSingleClick={() => {}}
                  onRemove={(uid, _id) => this.props.remove(uid, _id)}
                  onRename={(uid, _id, name) =>
                    this.props.rename(uid, _id, name)
                  }
                  disabled={false}
                />
              );
            })}
            <button
              className="add-category"
              onClick={e => this.addCategoryWithParent(e)}
            >
              <i className="fa fa-plus" />
              Add Sub Category
            </button>
          </div>
          <br />
          <p>
            <small>
              <i>*Double click to rename categories.</i>
            </small>
          </p>
          <br />
          <br />
          <br />
          <br />
        </div>
      );
    }
    return (
      <div className="no-cat-selected">
        <p>Select a category to add sub categories.</p>
      </div>
    );
  }

  render() {
    const {
      isLoading,
      errors,
      info,
      subCatChanged,
      categories,
      selectedCategoryId
    } = this.props;

    const topLevelFilter = cat => !cat.parent;
    const catOptions = map(categories.filter(topLevelFilter), cat => {
      return {
        value: cat._id,
        label: cat.name
      };
    });

    return (
      <BasePage menu="projects">
        <h1>Project Categories</h1>

        <p>
          Categories are used to help organize your projects. For example, you
          may have a category named “Windows” and a sub-category “Victorian”.
          Your customers will then be able to use filters to better views your
          projects.
        </p>

        <br />

        <Loader isLoading={isLoading}>
          <div>
            <h3>Top Level Categories</h3>

            <div className="pills">
              {categories.filter(topLevelFilter).map((cat, idx) => {
                const isActive = cat._id && cat._id === selectedCategoryId;
                const key =
                  idx +
                  cat.name +
                  cat.uid +
                  (cat._id || "") +
                  (isActive ? 1 : 0);
                return (
                  <SubCatPill
                    key={key}
                    name={cat.name}
                    uid={cat.uid}
                    parent={false}
                    _id={cat._id}
                    isActive={isActive}
                    onSingleClick={_id => this.onPillSingleClick(_id)}
                    onRemove={(uid, _id) => this.removeCategory(uid, _id)}
                    onRename={(uid, _id, name) =>
                      this.props.rename(uid, _id, name)
                    }
                    disabled={false}
                  />
                );
              })}
              <button
                className="add-category"
                onClick={e => this.addCategory(e)}
              >
                <i className="fa fa-plus" />
                Add Category
              </button>
            </div>
            <p>
              <small>
                <i>*Double click to rename categories.</i>
              </small>
            </p>

            <div className="sub-cat-selector">
              <div className="label-holder">
                <h3>Select a category</h3>
                <p>
                  Here you can add sub categories for any of the top level
                  categories. This allows for a mucher finder level of
                  filtering.
                </p>
              </div>
              <div className="select-holder">
                <Select
                  options={catOptions}
                  clearable={false}
                  value={selectedCategoryId}
                  onChange={val => this.props.selectParent(val.value)}
                />
              </div>
            </div>

            {this.renderSubPills()}
          </div>
        </Loader>
      </BasePage>
    );
  }
}

const mapStateToProps = function(state, ownProps) {
  const props = {
    categories: orderBy(state.editingCategories.categories, ["name"]),
    selectedCategoryId: state.editingCategories.selectedCategoryId,
    isLoading: false
  };

  return props;
};

const mapDispatchToProps = function(dispatch, ownProps) {
  const orgId = window.pubOrgId;
  const fetchAll = () =>
    dispatch(
      fetchCategories(-1, {
        organization: orgId
      })
    );
  return {
    add: () => {
      dispatch(addLocalCategory("New Category"));
      dispatch(
        createCategory({ name: "New Category" }, "Category created.")
      ).then(fetchAll);
    },
    addWithParent: (uid, _id) => {
      dispatch(addLocalCategory("New Category", uid));
      dispatch(
        createCategory(
          {
            name: "New Category",
            parent: _id
          },
          "Sub category created."
        )
      ).then(fetchAll);
    },
    selectParent: _id => dispatch(selectTopLevelCategory(_id)),
    remove: (uid, _id) => {
      dispatch(removeLocalCategory(_id));
      dispatch(deleteCategory(_id, "Category removed.")).then(fetchAll);
    },
    rename: (uid, _id, name) => {
      dispatch(renameLocalCategory(uid, name));
      dispatch(updateCategory(_id, { name }, `${name} updated.`)).then(
        fetchAll
      );
    },
    load: fetchAll
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(EditCategories);
