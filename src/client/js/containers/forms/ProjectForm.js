import React, { Component } from "react";
import { map, filter, sortBy, each } from "lodash";
import Select from "../../components/Select";
import { reduxForm, Field, formValues } from "redux-form";
import { renderSelect, renderInput, renderDatePicker } from "./renderInputs";
import Button from "../../components/Button";
import PropTypes from "prop-types";

class ProjectForm extends Component {
  states() {
    return {
      AL: "Alabama",
      AK: "Alaska",
      AZ: "Arizona",
      AR: "Arkansas",
      CA: "California",
      CO: "Colorado",
      CT: "Connecticut",
      DE: "Delaware",
      DC: "District Of Columbia",
      FL: "Florida",
      GA: "Georgia",
      GU: "Guam",
      HI: "Hawaii",
      ID: "Idaho",
      IL: "Illinois",
      IN: "Indiana",
      IA: "Iowa",
      KS: "Kansas",
      KY: "Kentucky",
      LA: "Louisiana",
      ME: "Maine",
      MD: "Maryland",
      MA: "Massachusetts",
      MI: "Michigan",
      MN: "Minnesota",
      MS: "Mississippi",
      MO: "Missouri",
      MT: "Montana",
      NE: "Nebraska",
      NV: "Nevada",
      NH: "New Hampshire",
      NJ: "New Jersey",
      NM: "New Mexico",
      NY: "New York",
      NC: "North Carolina",
      ND: "North Dakota",
      OH: "Ohio",
      OK: "Oklahoma",
      OR: "Oregon",
      PW: "Palau",
      PA: "Pennsylvania",
      PR: "Puerto Rico",
      RI: "Rhode Island",
      SC: "South Carolina",
      SD: "South Dakota",
      TN: "Tennessee",
      TX: "Texas",
      UT: "Utah",
      VT: "Vermont",
      VI: "Virgin Islands",
      VA: "Virginia",
      WA: "Washington",
      WV: "West Virginia",
      WI: "Wisconsin",
      WY: "Wyoming"
    };
  }
  errorEl(field) {
    if (field.touched && field.error) {
      return <span className="err-msg">{field.error}</span>;
    }
    return;
  }
  categoryOptions() {
    const { availableCategories } = this.props;
    const cats = sortBy(availableCategories, ["name"]);
    const parents = filter(cats, cat => !cat.parent);
    const options = [];
    each(parents, parent => {
      options.push({
        label: parent.name,
        value: parent._id,
        parent: true
      });
      each(cats, cat => {
        if (cat.parent === parent._id) {
          options.push({
            label: cat.name,
            value: cat._id,
            parent: false,
            parentName: parent.name
          });
        }
      });
    });
    return options;
  }
  customValueRenderer(option) {
    if (option.parent) {
      return <span>{option.label}</span>;
    }
    return (
      <span>
        {option.parentName}: {option.label}
      </span>
    );
  }
  customOptionRenderer(option) {
    if (option.parent) {
      return (
        <span>
          <b>{option.label}</b>
        </span>
      );
    }
    return <span>&nbsp;&nbsp;&nbsp;{option.label}</span>;
  }
  render() {
    const { isNew, requesting, availableCategories, handleSubmit } = this.props;

    const states = map(this.states(), (v, k) => {
      return {
        value: k,
        label: v
      };
    });

    return (
      <div className="user-form">
        <form className="contained" onSubmit={handleSubmit}>
          <div className="control">
            <span className="label-wrap">
              <label htmlFor="project">Project Name*</label>
              <small>
                This is only to help you better identify the project. It will
                not be seen on your map or survey.
              </small>
            </span>
            <div className="field-wrap">
              <Field
                name="name"
                component={renderInput}
                className="basic-field medium"
                placeholder="The Smith's Home"
              />
            </div>
          </div>

          <div className="control">
            <span className="label-wrap">
              <label htmlFor="city">City*</label>
            </span>
            <div className="field-wrap">
              <Field
                name="city"
                component={renderInput}
                className="basic-field medium"
                placeholder="City Name"
              />
            </div>
          </div>

          <div className="control">
            <span className="label-wrap">
              <label htmlFor="state">State*</label>
            </span>
            <div className="field-wrap">
              <Field
                name="state"
                component={renderSelect}
                options={states}
                className="field medium"
                placeholder="Select State"
              />
            </div>
          </div>

          <div className="control">
            <span className="label-wrap">
              <label htmlFor="zip">Zip*</label>
            </span>
            <div className="field-wrap">
              <Field
                name="zip"
                component={renderInput}
                className="basic-field small"
                placeholder="Zip Code"
              />
            </div>
          </div>

          <div className="control">
            <span className="label-wrap">
              <label htmlFor="street">Street*</label>
            </span>
            <div className="field-wrap">
              <Field
                name="street"
                component={renderInput}
                className="basic-field large"
                placeholder="Street"
              />
            </div>
          </div>

          <div className="control">
            <span className="label-wrap">
              <label htmlFor="categories">Categories</label>
              <small>
                Tag your project with relevant categories. These will make it
                easier for your users to filter for what they're looking for on
                your map.
              </small>
            </span>
            <div className="field-wrap">
              <Field
                name="categories"
                component={renderSelect}
                className="field large"
                placeholder="Select Categories"
                options={this.categoryOptions()}
                optionRenderer={this.customOptionRenderer}
                valueRenderer={this.customValueRenderer}
                multi={true}
              />
            </div>
          </div>

          <div className="control">
            <span className="label-wrap">
              <label htmlFor="categories">Customer Email(s)</label>
              <small>
                You may add multiple emails by separating them with a comma.
              </small>
            </span>
            <div className="field-wrap">
              <Field
                name="commaDelimitedEmails"
                component={renderInput}
                className="basic-field large"
                placeholder="john@example.com, jeff@example.com"
              />
            </div>
          </div>

          <div className="action-control">
            <div className="btn-wrap">
              <Button
                type="submit"
                disabled={requesting}
                label={isNew ? "Create Project" : "Update Details"}
              />
            </div>
          </div>
        </form>
      </div>
    );
  }
}

ProjectForm.propTypes = {
  isNew: PropTypes.bool.isRequired,
  availableCategories: PropTypes.array.isRequired,
  requesting: PropTypes.bool.isRequired
};

export default reduxForm({
  form: "projectForm",
  validate: values => {
    const errors = {};
    if (!values.name) {
      errors.name = "A name is required.";
    }
    if (!values.state) {
      errors.state = "A state is required.";
    }
    if (!values.city) {
      errors.city = "A city is required.";
    }
    return errors;
  }
})(ProjectForm);
