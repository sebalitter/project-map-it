import React, { Component } from "react";
import { reduxForm, Field } from "redux-form";
import { renderInput } from "./renderInputs";
import Button from "../../components/Button";
import * as validations from "../../utils/validations";
import PropTypes from "prop-types";

class AccountForm extends Component {
  onAccountRemove(e) {
    e.preventDefault();
    this.props.onAccountRemove();
  }
  render() {
    const { requesting, user, permissions, handleSubmit } = this.props;

    return (
      <div className="account-form">
        <form className="contained" onSubmit={handleSubmit}>
          <div className="control">
            <span className="label-wrap">
              <label htmlFor="name">Name*</label>
            </span>
            <Field
              component={renderInput}
              name="name"
              type="text"
              placeholder="Full Name"
              className="basic-field large"
            />
          </div>

          <div className="control">
            <span className="label-wrap">
              <label htmlFor="email">Email Address*</label>
            </span>
            <Field
              component={renderInput}
              name="email"
              type="email"
              placeholder="johndoe@example.com"
              className="basic-field large"
            />
          </div>
          <div className="control">
            <span className="label-wrap">
              <label htmlFor="email">New Password</label>
            </span>
            <Field
              component={renderInput}
              name="password"
              type="password"
              placeholder="New Password"
              className="basic-field medium"
            />
          </div>

          <div className="control">
            <span className="label-wrap">
              <label htmlFor="email">Confirm New Password</label>
            </span>
            <Field
              component={renderInput}
              name="confirmPassword"
              type="password"
              placeholder="Confirm Password"
              className="basic-field medium"
            />
          </div>

          <div className="action-control">
            <div className="btn-wrap">
              <button
                onClick={e => this.onAccountRemove(e)}
                className="deactivate-account"
              >
                Deactivate Account
              </button>
              <Button type="submit" disabled={requesting} label="Update" />
            </div>
          </div>
        </form>
      </div>
    );
  }
}

AccountForm.propTypes = {
  requesting: PropTypes.bool.isRequired,
  user: PropTypes.object.isRequired,
  onAccountRemove: PropTypes.func.isRequired
};

export default reduxForm({
  form: "acountForm",
  validate: values => {
    const errors = {};

    if (!values.name) {
      errors.name = "Your name is required.";
    }

    if (!values.orgname) {
      errors.orgname = "Organization name is required.";
    }

    if (validations.email(values.email)) {
      errors.email = "Invalid email address";
    }

    if (values.password) {
      if (values.password !== values.confirmPassword) {
        errors.password = "Passwords do not match.";
        errors.confirmPassword = "Passwords do not match.";
      }
    }

    return errors;
  }
})(AccountForm);
