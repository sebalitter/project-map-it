import React, { Component } from "react";
import Loader from "../../components/Loader";
import Button from "../../components/Button";
import { get } from "lodash";
import PropTypes from "prop-types";

export default class SubscriptionForm extends Component {
  submit(e) {
    e.preventDefault();
  }
  btnProps(plan) {
    const args = {
      label: "Select",
      onClick: e => {
        e.preventDefault();
        this.props.subscriptionClicked(plan);
      }
    };

    if (this.props.currentPlan === plan) {
      args.className = "active";
      args.label = "Current";
    }

    return args;
  }
  cardInfo(plan) {
    return null;
  }
  renderUnsubscribe() {
    if (this.props.user.organization.active) {
      return (
        <div className="unsubscribe">
          <button onClick={this.props.unsubscribeClicked}>
            <b>Cancel Subscription.</b> You may cancel at any time, but all
            features will be disabled.
          </button>
        </div>
      );
    }
    return null;
  }
  render() {
    const { isLoading } = this.props;
    const freeBtnProps = this.btnProps("free");
    const basicBtnProps = this.btnProps("basic");
    const proBtnProps = this.btnProps("pro");
    const enterpriseBtnProps = this.btnProps("enterprise");

    return (
      <div className="subscription-wrapper">
        <Loader isLoading={isLoading}>
          <div className="subscriptions">
            <div className="subscription">
              <div className="icon basic">
                <div className="inner" />
              </div>
              <div className="description">
                <h4>Basic ($99/month)</h4>
                <p>A great plan to get you started.</p>
                <p className="details">0 - 250 pins | unlimited users.</p>
              </div>
              <div className="action">
                <Button type="button" {...basicBtnProps} />
                {this.cardInfo("basic")}
              </div>
            </div>

            <div className="subscription">
              <div className="icon pro">
                <div className="inner" />
              </div>
              <div className="description">
                <h4>Pro ($149/month)</h4>
                <p>We scale as your business grows.</p>
                <p className="details">250 - 500 pins | unlimited users</p>
              </div>
              <div className="action">
                <Button type="button" {...proBtnProps} />
                {this.cardInfo("pro")}
              </div>
            </div>

            <div className="subscription">
              <div className="icon enterprise">
                <div className="inner" />
              </div>
              <div className="description">
                <h4>Enterprise ($199/month)</h4>
                <p>Infinitely scalable enterprise level.</p>
                <p className="details">unlimited pins | unlimited users</p>
              </div>
              <div className="action">
                <Button type="button" {...enterpriseBtnProps} />
                {this.cardInfo("enterprise")}
              </div>
            </div>

            {this.renderUnsubscribe()}
          </div>
        </Loader>
      </div>
    );
  }
}

SubscriptionForm.propTypes = {
  subscriptionClicked: PropTypes.func.isRequired,
  unsubscribeClicked: PropTypes.func.isRequired,
  user: PropTypes.object.isRequired,
  info: PropTypes.object.isRequired,
  currentCard: PropTypes.string.isRequired,
  currentPlan: PropTypes.string.isRequired,
  isLoading: PropTypes.bool.isRequired
};
