import React, { Component } from "react";
import PropTypes from "prop-types";
import { reduxForm, Field, formValues } from "redux-form";
import { renderSelect, renderInput, renderDatePicker } from "./renderInputs";
import Button from "../../components/Button";

class ProjectExportForm extends Component {
  render() {
    const { requesting, handleSubmit } = this.props;

    return (
      <div className="account-form">
        <form className="contained" onSubmit={handleSubmit}>
          <div className="control">
            <span className="label-wrap">
              <label htmlFor="from">From*</label>
            </span>
            <Field name="from" component={renderDatePicker} />
          </div>
          <div className="control">
            <span className="label-wrap">
              <label htmlFor="to">To*</label>
            </span>
            <Field name="to" component={renderDatePicker} />
          </div>
          <div className="action-control">
            <div className="btn-wrap">
              <Button type="submit" disabled={requesting} label="Export" />
            </div>
          </div>
        </form>
      </div>
    );
  }
}

ProjectExportForm.propTypes = {
  requesting: PropTypes.bool.isRequired,
  isLoading: PropTypes.bool.isRequired,
  categories: PropTypes.array.isRequired
};

export default reduxForm({
  form: "projectExportForm",
  validate: values => {
    const errors = {};
    return errors;
  }
})(ProjectExportForm);
