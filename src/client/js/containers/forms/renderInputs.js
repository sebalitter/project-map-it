import React from "react";
import moment from "moment";
import { isArray } from "lodash";
import DatePicker from "../../components/DatePicker";
import { DISPLAY_FORMAT } from "../../utils/date";
import { default as ReactSelect } from "react-select";

export const renderInput = field => {
  return (
    <div>
      <input
        placeholder={field.placeholder || ""}
        className={field.className || ""}
        disabled={field.disabled ? true : false}
        type={field.type || "text"}
        {...field.input}
      />
      {field.meta.touched &&
        field.meta.error && <span className="err-msg">{field.meta.error}</span>}
    </div>
  );
};

export const renderSelect = field => {
  const extraOpts = {};

  if (field.optionRenderer) {
    extraOpts.optionRenderer = field.optionRenderer;
  }

  if (field.valueRenderer) {
    extraOpts.valueRenderer = field.valueRenderer;
  }

  return (
    <div>
      <ReactSelect
        clearable={field.clearable ? true : false}
        placeholder={field.placeholder}
        options={field.options}
        disabled={field.disabled ? true : false}
        multi={field.multi ? true : false}
        clearableValue={field.clearableValue ? true : false}
        className={field.className || "inline-react-select stretch"}
        {...extraOpts}
        {...field.input}
        backspaceToRemoveMessage=""
        inputProps={{
          autoComplete: "none",
          autoCorrect: "off",
          spellCheck: "off"
        }}
        onBlur={e => {}}
        onChange={val => {
          if (isArray(val)) {
            field.input.onChange(val.map(obj => obj.value));
          } else {
            field.input.onChange(val.value);
          }
        }}
      />
      {field.meta.touched &&
        field.meta.error && <span className="error">{field.meta.error}</span>}
    </div>
  );
};

export const renderDatePicker = field => {
  const opts = {
    value: field.value,
    className: "basic-field stretch"
  };

  if (field.minDate) {
    opts.minDate = moment(field.minDate, DISPLAY_FORMAT);
  }

  if (field.maxDate) {
    opts.maxDate = moment(field.maxDate, DISPLAY_FORMAT);
  }

  return (
    <div>
      <DatePicker {...opts} />
      {field.meta.touched &&
        field.meta.error && <span className="error">{field.meta.error}</span>}
    </div>
  );
};
