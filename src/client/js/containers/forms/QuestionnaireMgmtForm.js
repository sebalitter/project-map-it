import React, { Component } from "react";
import PropTypes from "prop-types";
import { reduxForm, Field, FieldArray } from "redux-form";
import { renderSelect, renderInput, renderDatePicker } from "./renderInputs";
import Button from "../../components/Button";

const kindOpts = [
  {
    label: "Free Text",
    value: "text"
  },
  {
    label: "Yes/ No",
    value: "boolean"
  },
  {
    label: "Star Rating",
    value: "star"
  }
];

class QuestionnaireMgmtForm extends Component {
  constructor(props) {
    super(props);
    this.renderQuestions = this.renderQuestions.bind(this);
  }
  addQuestion(event, fields) {
    event.preventDefault();
    fields.push({
      immutable: false,
      text: "",
      kind: "text",
      pub: false,

      // This will ensure a newly added question will always be last despite
      // what the largest order number may be.
      order: Math.max.apply(null, fields.getAll().map(f => f.order)) + 1
    });
  }
  removeQuestion(event, idx, questions) {
    event.preventDefault();
    questions.remove(idx);
  }
  renderSubmit() {
    return (
      <div className="action-control">
        <div className="btn-wrap">
          <Button
            type="submit"
            disabled={this.props.requesting}
            label="Update"
          />
        </div>
      </div>
    );
  }
  renderPublicLabel() {
    return (
      <div className="public-label">
        <span>Public:</span>
      </div>
    );
  }
  renderQuestions({ fields }) {
    // This is the field index and used to track order.
    let fidx = 0;

    return [
      fields.map((question, idx) => {
        const _fields = fields.get(idx);
        return (
          <div className="question" key={idx}>
            <Field
              type="hidden"
              name={`${question}.order`}
              component={renderInput}
              input={{
                value: fidx++
              }}
            />
            <div className="kind">
              <div className="control">
                <Field
                  name={`${question}.kind`}
                  component={renderSelect}
                  className="stretch"
                  disabled={_fields.immutable}
                  options={kindOpts}
                />
              </div>
            </div>
            <div className="text">
              <div className="control">
                <Field
                  name={`${question}.text`}
                  component={renderInput}
                  className="basic-field stretch"
                  disabled={_fields.immutable}
                />
              </div>
            </div>
            <div className="pub">
              <Field
                name={`${question}.pub`}
                component={renderInput}
                type="checkbox"
                disabled={_fields.immutable}
              />
            </div>
            <div className="controls">
              {!_fields.immutable ? (
                <button onClick={e => this.removeQuestion(e, idx, fields)}>
                  <i className="fa fa-minus-circle" />
                </button>
              ) : null}
            </div>
          </div>
        );
      }),
      <div className="add-question" key={1}>
        <button onClick={e => this.addQuestion(e, fields)}>
          <span>Add Question</span>
          <i className="fa fa-plus-circle" />
        </button>
      </div>
    ];
  }
  render() {
    const { requesting, handleSubmit } = this.props;

    return (
      <form className="questionnaire-mgmt-form" onSubmit={handleSubmit}>
        {this.renderPublicLabel()}

        <div className="questions">
          <FieldArray name="questions" component={this.renderQuestions} />
        </div>
        {this.renderSubmit()}
      </form>
    );
  }
}

QuestionnaireMgmtForm.propTypes = {
  requesting: PropTypes.bool.isRequired
};

export default reduxForm({
  form: "questionnaireForm",
  validate: values => {
    const errors = {};
    return errors;
  }
})(QuestionnaireMgmtForm);
