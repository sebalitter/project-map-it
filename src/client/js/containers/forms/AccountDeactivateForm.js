import React, { Component } from "react";
import { reduxForm, Field } from "redux-form";
import { renderSelect } from "./renderInputs";
import Button from "../../components/Button";
import PropTypes from "prop-types";

const removalReasons = [
  {
    value: "Too expensive!",
    label: "Too expensive!"
  },
  {
    value: "This doesn't meet my needs.",
    label: "This doesn't meet my needs."
  },
  {
    value: "Going out of business",
    label: "Going out of business"
  },
  {
    value: "Using another mapping platform",
    label: "Using another mapping platform"
  },
  {
    value: "Other",
    label: "Other"
  }
];

class AccountDeactivateForm extends Component {
  render() {
    const { requesting, handleSubmit } = this.props;

    return (
      <div className="account-form">
        <h2>Sorry to see you go!</h2>
        <form className="contained" onSubmit={handleSubmit}>
          <div className="control">
            <span className="label-wrap">
              <label htmlFor="reason">Reason*</label>
            </span>

            <Field
              name="reason"
              component={renderSelect}
              className="inline-react-select stretch"
              options={removalReasons}
              clearable={false}
            />
          </div>
          <div className="action-control">
            <div className="btn-wrap">
              <Button
                type="submit"
                disabled={requesting}
                label="Deactivate My Account"
              />
            </div>
          </div>
        </form>
      </div>
    );
  }
}

AccountDeactivateForm.propTypes = {
  requesting: PropTypes.bool.isRequired
};

export default reduxForm({
  form: "deactivateForm",
  validate: values => {
    const errors = {};

    if (!values.reason) {
      errors.reason = "Reason";
    }

    return errors;
  }
})(AccountDeactivateForm);
