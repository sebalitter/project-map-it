import React, { Component } from "react";
import PropTypes from "prop-types";
import { reduxForm, Field } from "redux-form";
import { renderSelect, renderInput } from "./renderInputs";
import {
  map,
  each,
  isEmpty,
  isString,
  trim,
  has,
  invert,
  toLower,
  toUpper
} from "lodash";
import Button from "../../components/Button";
import { validEmail, containsNumbers } from "../../utils/validations";

class ImportProjectsForm extends Component {
  getValidState(str) {
    if (!isString(str)) {
      return null;
    }
    const hash = {
      alabama: "al",
      alaska: "ak",
      "american samoa": "as",
      arizona: "az",
      arkansas: "ar",
      california: "ca",
      colorado: "co",
      connecticut: "ct",
      delaware: "de",
      "district of columbia": "dc",
      "washington dc": "dc",
      "washington d.c.": "dc",
      "d.c.": "dc",
      "federated states of micronesia": "fm",
      florida: "fl",
      georgia: "ga",
      guam: "gu",
      hawaii: "hi",
      idaho: "id",
      illinois: "il",
      indiana: "in",
      iowa: "ia",
      kansas: "ks",
      kentucky: "ky",
      louisiana: "la",
      maine: "me",
      "marshall islands": "mh",
      maryland: "md",
      massachusetts: "ma",
      michigan: "mi",
      minnesota: "mn",
      mississippi: "ms",
      missouri: "mo",
      montana: "mt",
      nebraska: "ne",
      nevada: "nv",
      "new hampshire": "nh",
      "new jersey": "nj",
      "new mexico": "nm",
      "new york": "ny",
      "north carolina": "nc",
      "north dakota": "nd",
      "northern mariana islands": "mp",
      ohio: "oh",
      oklahoma: "ok",
      oregon: "or",
      palau: "pw",
      pennsylvania: "pa",
      "puerto rico": "pr",
      "rhode island": "ri",
      "south carolina": "sc",
      "south dakota": "sd",
      tennessee: "tn",
      texas: "tx",
      utah: "ut",
      vermont: "vt",
      "virgin islands": "vi",
      virginia: "va",
      washington: "wa",
      "west virginia": "wv",
      wisconsin: "wi",
      wyoming: "wy"
    };
    const invertedHash = invert(hash);
    const cleanStr = toLower(
      trim(str)
        .replace(/[.,*!@:<>{}\\\/;#$%^&*()+=\[\|\]~'"`]/g, "")
        .replace(/[-_\s\t]+/g, " ")
    );
    if (has(hash, cleanStr)) {
      return toUpper(hash[cleanStr]);
    }
    if (has(invertedHash, cleanStr)) {
      return toUpper(cleanStr);
    }
    return null;
  }
  parseEmailString(str) {
    const emails = [];
    if (typeof str === "undefined") {
      return "";
    }
    each(map(str.split(","), trim), email => {
      if (validEmail(email)) {
        emails.push(email);
      }
    });
    return emails.join(",");
  }
  parsePhoneString(str) {
    const phones = [];
    if (typeof str === "undefined") {
      return "";
    }
    each(map(str.split(","), trim), phone => {
      if (containsNumbers(phone)) {
        phones.push(phone);
      }
    });
    return phones.join(",");
  }
  mapColumns(project) {
    const projects = [];
    const {
      name,
      zip,
      city,
      state,
      street,
      phone,
      email,
      categories
    } = project;

    each(this.props.csvData, row => {
      const _row = {};

      if (name) {
        _row.name = row[name];
      }

      if (zip) {
        _row.zip = row[zip];
      }

      if (phone) {
        _row.commaDelimitedPhoneNumbers = this.parsePhoneString(row[phone]);
      }

      if (email) {
        _row.commaDelimitedEmails = this.parseEmailString(row[email]);
      }

      if (categories) {
        _row.categories = row[categories];
      }

      _row.city = row[city];
      _row.street = row[street];
      _row.state = this.getValidState(row[state]);

      if (_row.city && _row.street && _row.state) {
        projects.push(_row);
      }
    });

    this.props.onProcessed(projects);
  }
  render() {
    const { requesting, handleSubmit, csvCols } = this.props;

    return (
      <div className="import-project-form">
        <h3>Configure Your Import</h3>

        <p>
          You must map the column headers in the spreadsheet to their
          corresponding project fields so we know how to handle the file.
        </p>

        <br />

        <form
          className="contained"
          onSubmit={handleSubmit(vals => this.mapColumns(vals))}
        >
          <div className="control">
            <span className="label-wrap">
              <label htmlFor="name">Name</label>
            </span>
            <div className="field-wrap">
              <Field
                name="name"
                component={renderSelect}
                clearable={false}
                placeholder={"CSV Column"}
                options={csvCols}
              />
              <small>
                This is only to help you better identify the project. It will
                not be seen on your map or survey.
              </small>
            </div>
          </div>
          <div className="control">
            <span className="label-wrap">
              <label htmlFor="state">State*</label>
            </span>
            <div className="field-wrap">
              <Field
                name="state"
                component={renderSelect}
                clearable={false}
                placeholder={"CSV Column"}
                options={csvCols}
              />
            </div>
          </div>
          <div className="control">
            <span className="label-wrap">
              <label htmlFor="city">City*</label>
            </span>
            <div className="field-wrap">
              <Field
                name="city"
                className="field stretch"
                component={renderSelect}
                clearable={false}
                placeholder={"CSV Column"}
                options={csvCols}
              />
            </div>
          </div>
          <div className="control">
            <span className="label-wrap">
              <label htmlFor="street">Street*</label>
            </span>
            <div className="field-wrap">
              <Field
                name="street"
                component={renderSelect}
                clearable={false}
                placeholder={"CSV Column"}
                options={csvCols}
              />
            </div>
          </div>
          <div className="control">
            <span className="label-wrap">
              <label htmlFor="zip">Zip</label>
            </span>
            <div className="field-wrap">
              <Field
                name="zip"
                component={renderSelect}
                clearable={false}
                placeholder={"CSV Column"}
                options={csvCols}
              />
            </div>
          </div>
          <div className="control">
            <span className="label-wrap">
              <label htmlFor="email">Email</label>
            </span>
            <div className="field-wrap">
              <Field
                name="email"
                component={renderSelect}
                clearable={false}
                placeholder={"CSV Column"}
                options={csvCols}
              />
            </div>
          </div>
          <div className="control">
            <span className="label-wrap">
              <label htmlFor="state">Phone</label>
            </span>
            <div className="field-wrap">
              <Field
                name="phone"
                component={renderSelect}
                clearable={false}
                placeholder={"CSV Column"}
                options={csvCols}
              />
            </div>
          </div>
          <div className="control">
            <span className="label-wrap">
              <label htmlFor="state">Categories</label>
            </span>
            <div className="field-wrap">
              <Field
                name="categories"
                component={renderSelect}
                clearable={true}
                placeholder={"CSV Column"}
                options={csvCols}
              />
              <small>
                You may select a column to be used to generate top level
                categories. You can use multiple categories by separating them
                with a comma. e.g. "Windows, Hunter Green". For best results,
                try to make sure your spelling and formatting is consistent.
              </small>
            </div>
          </div>

          <div className="action-control">
            <div className="btn-wrap">
              <Button type="submit" disabled={requesting} label="Import!" />
            </div>
          </div>
        </form>
      </div>
    );
  }
}

ImportProjectsForm.propTypes = {
  requesting: PropTypes.bool.isRequired,
  csvCols: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.any.isRequired,
      value: PropTypes.any.isRequired
    })
  )
};

export default reduxForm({
  form: "importForm",
  validate: values => {
    const errors = { project: {} };
    const valid = val => val !== undefined && val !== null;
    const requiredKeys = ["street", "state", "city"];

    each(values.project, (val, key) => {
      if (requiredKeys.indexOf(key) !== -1 && !valid(val)) {
        errors.project[key] = `This field is required.`;
      }
    });

    return errors;
  }
})(ImportProjectsForm);
