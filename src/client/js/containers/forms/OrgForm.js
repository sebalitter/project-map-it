import React, { Component } from "react";
import { reduxForm, Field } from "redux-form";
import { renderInput } from "./renderInputs";
import Button from "../../components/Button";
import * as validations from "../../utils/validations";
import PropTypes from "prop-types";

class OrgForm extends Component {
  render() {
    const { updating, handleSubmit } = this.props;

    return (
      <div className="account-form">
        <form className="contained" onSubmit={handleSubmit}>
          <div className="control">
            <span className="label-wrap">
              <label htmlFor="orgname">Organization Name*</label>
            </span>
            <Field
              component={renderInput}
              name="name"
              type="text"
              disabled={updating}
              placeholder="Name"
              className="basic-field large"
            />
          </div>

          <div className="control">
            <span className="label-wrap">
              <label htmlFor="email">Facebook</label>
            </span>
            <Field
              component={renderInput}
              name="social_facebook"
              disabled={updating}
              type="url"
              placeholder="https://www.facebook.com/your-business"
              className="basic-field medium"
            />
          </div>

          <div className="control">
            <span className="label-wrap">
              <label htmlFor="email">Google</label>
            </span>
            <Field
              name="social_google"
              disabled={updating}
              type="url"
              component={renderInput}
              placeholder="https://www.google.it/maps/place/your-business"
              className="basic-field medium"
            />
          </div>

          <div className="control">
            <span className="label-wrap">
              <label htmlFor="email">Houzz</label>
            </span>
            <Field
              name="social_houzz"
              component={renderInput}
              disabled={updating}
              type="url"
              placeholder="https://www.houzz.com/pro/your-business"
              className="basic-field medium"
            />
          </div>

          <div className="control">
            <span className="label-wrap">
              <label htmlFor="email">Better Business Bureau</label>
            </span>
            <Field
              name="social_bbb"
              component={renderInput}
              disabled={updating}
              type="url"
              placeholder="https://www.bbb.org/your-business"
              className="basic-field medium"
            />
          </div>

          <div className="control">
            <span className="label-wrap">
              <label htmlFor="email">Angies List</label>
            </span>
            <Field
              name="social_angies_list"
              component={renderInput}
              disabled={updating}
              type="url"
              placeholder="https://www.angieslist.com/company/your-company"
              className="basic-field medium"
            />
          </div>

          <div className="control">
            <span className="label-wrap">
              <label htmlFor="email">Home Advisor</label>
            </span>
            <Field
              name="social_home_advisor"
              component={renderInput}
              disabled={updating}
              type="url"
              placeholder="https://www.homeadvisor.com/your-company"
              className="basic-field medium"
            />
          </div>

          <div className="action-control">
            <div className="btn-wrap">
              <Button type="submit" disabled={updating} label="Update" />
            </div>
          </div>
        </form>
      </div>
    );
  }
}

OrgForm.propTypes = {
  updating: PropTypes.bool.isRequired
};

export default reduxForm({
  form: "orgForm",
  validate: values => {
    const errors = {};

    if (!values.name) {
      errors.name = "Your name is required.";
    }

    if (!values.orgname) {
      errors.orgname = "Organization name is required.";
    }

    return errors;
  }
})(OrgForm);
