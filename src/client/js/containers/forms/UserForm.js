import React, { Component } from "react";
import { reduxForm, Field } from "redux-form";
import { renderSelect, renderInput } from "./renderInputs";
import Button from "../../components/Button";
import PropTypes from "prop-types";

const roleOptions = [
  {
    label: "Guest (Map view only)",
    value: 4
  },
  {
    label: "Editor (Project MGMT)",
    value: 3
  },
  {
    label: "Admin (Team & Project MGMT)",
    value: 2
  }
];

class UserForm extends Component {
  render() {
    const { newUser, submitting, requesting, handleSubmit } = this.props;

    return (
      <div className="user-form">
        <form className="contained" onSubmit={handleSubmit}>
          <div className="control">
            <span className="label-wrap">
              <label htmlFor="org">Full Name</label>
            </span>
            <Field
              name="name"
              placeholder="John Doe"
              component={renderInput}
              className="basic-field medium"
            />
          </div>

          <div className="control">
            <span className="label-wrap">
              <label htmlFor="org">Email Address</label>
            </span>
            <Field
              name="email"
              placeholder="your@email.com"
              component={renderInput}
              className="basic-field medium"
              type="email"
            />
          </div>

          {!this.props.isOwner ? (
            <div className="control">
              <span className="label-wrap">
                <label htmlFor="org">Member Role</label>
              </span>
              <div className="field-wrap">
                <Field
                  name="role"
                  component={renderSelect}
                  options={roleOptions}
                />
              </div>
            </div>
          ) : null}

          <div className="action-control">
            <div className="btn-wrap">
              <Button
                type="submit"
                disabled={submitting || requesting}
                label={newUser ? "Add Member" : "Update"}
              />
            </div>
          </div>
        </form>
      </div>
    );
  }
}

UserForm.propTypes = {
  isOwner: PropTypes.bool.isRequired,
  newUser: PropTypes.bool.isRequired,
  requesting: PropTypes.bool.isRequired
};

export default reduxForm({
  form: "userForm",
  fields: ["name", "email", "role"],
  validate: values => {
    const errors = {};

    if (!values.name) {
      errors.name = "Required.";
    }

    if (!values.email) {
      errors.email = "Required.";
    } else if (
      !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)
    ) {
      errors.email = "Invalid email address";
    }

    if (!values.role) {
      errors.role = "Required.";
    }

    return errors;
  }
})(UserForm);
