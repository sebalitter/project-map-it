import React, { Component } from "react";
import { reduxForm, Field } from "redux-form";
import { renderSelect, renderInput } from "./renderInputs";
import * as validations from "../../utils/validations";
import Button from "../../components/Button";
import Loader from "../../components/Loader";
import { get, capitalize } from "lodash";
import PropTypes from "prop-types";

class UpgradePlanForm extends Component {
  modalTitle() {
    const { toPlan } = this.props;

    return capitalize(toPlan);
  }
  modalDescription() {
    const { currentPlan, toPlan, info, isLoading } = this.props;

    if (isLoading) {
      return null;
    }

    const brand = get(info, "sources.data[0].brand");
    const last4 = get(info, "sources.data[0].last4");

    if (currentPlan === toPlan) {
      return `You may update the card on file by entering a new credit card below. The current card on file is the ${brand} ending in ${last4}.`;
    } else {
      return `By entering a credit card below you will change to the ${toPlan} plan.`;
    }
  }
  render() {
    const { toPlan, isLoading, handleSubmit, currentPlan } = this.props;

    return (
      <div className="upgrade-form">
        <h2>{this.modalTitle()}</h2>
        <p>{this.modalDescription()}</p>

        <Loader isLoading={isLoading}>
          <form className="cc-form" onSubmit={handleSubmit}>
            <Field component={renderInput} type="hidden" name="toPlan" />

            <div className="control">
              <Field
                name="name"
                component={renderInput}
                className="basic-field stretch"
                placeholder="Full Name"
              />
            </div>

            <div className="control">
              <Field
                name="number"
                component={renderInput}
                className="basic-field stretch"
                placeholder="Card Number"
              />
            </div>

            <div className="control-ccdate">
              <div className="field">
                <Field
                  name="exp_month"
                  type="number"
                  min={0}
                  max={99}
                  component={renderInput}
                  className="basic-field stretch"
                  placeholder="MM"
                />
              </div>
              <div className="mid-field">
                <span>&nbsp;/&nbsp;</span>
              </div>
              <div className="field">
                <Field
                  name="exp_year"
                  type="number"
                  min={1000}
                  max={9999}
                  component={renderInput}
                  className="basic-field stretch"
                  placeholder="YYYY"
                />
              </div>
            </div>

            <Field
              name="cvc"
              component={renderInput}
              className="basic-field stretch"
              placeholder="CVC"
            />

            <br />
            <br />
            <div className="action-control">
              <div className="btn-wrap">
                <Button type="submit" label="Update Plan" />
              </div>
            </div>
          </form>
        </Loader>
      </div>
    );
  }
}

UpgradePlanForm.propTypes = {
  isLoading: PropTypes.bool.isRequired
};

export default reduxForm({
  form: "upgradeForm",
  validate: values => {
    const errors = {};
    const card = new validations.Card(values.number);
    const required = validations.required(values.name);
    const curYear = new Date().getFullYear();

    if (required) {
      errors.name = required;
    }

    if (values.exp_month < 1 || values.exp_month > 12) {
      errors.exp_month = "Invalid month.";
    }

    if (values.exp_year < curYear || values.exp_year > curYear + 40) {
      errors.exp_year = "Invalid year.";
    }

    if (!card.isValid()) {
      errors.number = "Invalid number.";
    }

    return errors;
  }
})(UpgradePlanForm);
