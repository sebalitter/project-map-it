import React, { Component } from "react";
import { map, each, trim, isEmpty, filter, cloneDeep } from "lodash";
import { reduxForm, Field } from "redux-form";
import { renderInput } from "./renderInputs";
import Button from "../../components/Button";
import * as validations from "../../utils/validations";
import PropTypes from "prop-types";

class SendSurveyForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      questions: []
    };
  }
  componentWillMount() {
    this.setState({
      questions: cloneDeep(this.props.completedSurvey)
    });
  }
  renderAnswer(kind, value) {
    switch (kind) {
      case "star":
        const amount = parseInt(value, 10);
        const five = new Array(5);
        return (
          <div className="stars">
            {map(five, (num, idx) => {
              if (amount >= idx + 1) {
                return <i key={idx} className="fa fa-star" />;
              }
              return <i key={idx} className="fa fa-star-o" />;
            })}
          </div>
        );
      case "text":
        return <p>{value}</p>;
      case "boolean":
        if (parseInt(value, 10)) {
          return <p>Yes.</p>;
        }
        return <p>No.</p>;
      default:
        throw new Error("Unknown type of question. Fail.");
    }
  }
  renderReadOnly() {
    const qs = this.props.completedSurvey;
    return (
      <div>
        <div>
          <span>
            <strong>Make Public:</strong>
          </span>
        </div>
        <br />
        {map(this.state.questions, (row, idx) => {
          return (
            <div
              className={"question-wrap " + (row.pub ? "pub" : "")}
              key={idx}
            >
              <div className="pub">
                <input
                  type="checkbox"
                  checked={row.pub}
                  onChange={e => {
                    this.setState(
                      {
                        questions: this.state.questions.map((question, i) => {
                          if (i === idx) {
                            return Object.assign({}, question, {
                              pub: !row.pub
                            });
                          }
                          return question;
                        })
                      },
                      () => {
                        this.props.onPrivacyChange(this.state.questions);
                      }
                    );
                  }}
                />
              </div>
              <div className="question">
                <h4>{row.question}</h4>
                {this.renderAnswer(row.kind, row.answer)}
              </div>
            </div>
          );
        })}
      </div>
    );
  }
  render() {
    const { requesting, handleSubmit, completedSurvey } = this.props;

    if (!isEmpty(completedSurvey)) {
      return this.renderReadOnly();
    }

    return (
      <div className="user-form">
        <form className="contained" onSubmit={handleSubmit}>
          <div className="control">
            <span className="label-wrap">
              <label htmlFor="orgname">Email(s)*</label>
            </span>
            <Field
              name="email"
              component={renderInput}
              className="basic-field large"
              placeholder="customer-1@example.com, customer-2@example.com"
            />
          </div>

          <div className="action-control">
            <Field component={renderInput} name="id" type="hidden" />
            <div className="btn-wrap">
              <Button type="submit" disabled={requesting} label="Send Survey" />
            </div>
          </div>
        </form>
      </div>
    );
  }
}

SendSurveyForm.propTypes = {
  requesting: PropTypes.bool.isRequired,
  onPrivacyChange: PropTypes.func,
  completedSurvey: PropTypes.array
};

export default reduxForm({
  form: "surveyForm",
  validate: values => {
    const errors = {};

    if (values.email) {
      each(values.email.split(","), email => {
        const valid = validations.validEmail(trim(email));
        if (!valid) {
          errors.email = "Invalid email(s).";
        }
      });
    } else {
      errors.email = "At least one email is required.";
    }

    return errors;
  }
})(SendSurveyForm);
