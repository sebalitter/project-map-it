import React, { Component } from "react";
import BasePage from "../components/BasePage";
import { connect } from "react-redux";
import { concat, isEmpty } from "lodash";
import QuestionnaireMgmtForm from "./forms/QuestionnaireMgmtForm";
import Loader from "../components/Loader";
import {
  removeQuestion,
  addQuestion,
  updateQuestions,
  fetchQuestions
} from "../modules/questionnaire/actions";

class QuestionnaireMgmt extends Component {
  componentDidMount() {
    this.props.load();
  }
  render() {
    if (!this.props.questions.questions.length) {
      return null;
    }
    return (
      <BasePage menu="survey">
        <h1>Customer Survey</h1>

        <p>
          These questions will be sent to the customer when adding a new
          project. If marked "public", they will be shown in the map as well.
        </p>

        <Loader isLoading={this.props.isLoading}>
          <QuestionnaireMgmtForm
            initialValues={this.props.questions}
            onSubmit={vals => this.props.update(vals)}
            requesting={this.props.requesting}
          />
        </Loader>
      </BasePage>
    );
  }
}

const mapStateToProps = function(state) {
  const requesting = state.updatedQuestions.isLoading;

  return {
    questions: {
      questions: state.fetchedQuestions.response.results || []
    },
    isLoading: state.fetchedQuestions.isLoading,
    requesting
  };
};

const mapDispatchToProps = function(dispatch) {
  return {
    load: () => dispatch(fetchQuestions(-1)),
    update: vals => dispatch(updateQuestions(vals, "Questions updated."))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(QuestionnaireMgmt);
