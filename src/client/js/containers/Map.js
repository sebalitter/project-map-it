/*global google */
import React, { Component } from "react";
import ReactDOM from "react-dom";
import {
  debounce,
  throttle,
  concat,
  defer,
  filter,
  each,
  map,
  isEmpty,
  sortBy,
  chunk,
  get,
  find,
  uniqueId,
  assign,
  compact
} from "lodash";
import { connect } from "react-redux";
import moment from "moment";
import Slider from "react-slick";
import Tooltip from "rc-tooltip";
import { paddedBounds, getBoundsZoomLevel } from "../utils/gmap";
import BasePage from "../components/BasePage";
import Button from "../components/Button";
import GoogleMap from "../components/GoogleMap";
import BaseModal from "../components/BaseModal";
import MapFilter from "../components/MapFilter";
import Loader from "../components/Loader";
import Authorize from "./Authorize";

import { fetchOrganization } from "../modules/organization/actions";

import { fetchCategories } from "../modules/category/actions";

import {
  showMapFilters,
  hideMapFilters,
  findUserLocation,
  launchProjectModal,
  dismissProjectModal,
  registerProjectFilter,
  unregisterProjectFilter,
  selectMapCategory,
  unselectMapCategory,
  showProjectsWithPhotosOnly,
  showProjectsWithOrWithoutPhotos
} from "../modules/map/actions";

import {
  fetchMapDefaults,
  fetchProjects,
  fetchGeoProjects,
  fetchProject
} from "../modules/project/actions";

class Map extends Component {
  constructor(props) {
    super(props);
    this._mapChangeHash = uniqueId();
  }
  componentDidMount() {
    const idFromUser = get(this, "props.user.organization.slug");
    const slugFromUrl = get(this, "props.params.slug");

    this.__onWindowClickCallback = e => {
      this.props.onPageClick(this.refs.filter, e);
    };

    this.__onWindowResize = debounce(() => {
      this.forceUpdate();
    }, 100);

    window.addEventListener("mousedown", this.__onWindowClickCallback, false);
    window.addEventListener("resize", this.__onWindowResize, false);

    this.props.load(idFromUser || slugFromUrl);
    this.loadPins();
  }
  componentWillUnmount() {
    window.removeEventListener("mousedown", this.__onWindowClickCallback);
    window.removeEventListener("resize", this.__onWindowResize);
  }
  handlePinClick(type, val) {
    switch (type) {
      case "single":
        this.props.launchProjectModal(val);
        break;
      default:
        // Cluster.
        const gmap = this.refs.map.__gmap;
        const bounds = new google.maps.LatLngBounds();
        each(val, pt => {
          bounds.extend(new google.maps.LatLng(pt.l[1], pt.l[0]));
        });

        // If the bounds of all the pins are greater than what's currently in
        // view, zoom in 1. Else, just zoom in on bounds specifically.
        const el = document.getElementsByClassName("gmap-canvas")[0];

        const newZoom =
          getBoundsZoomLevel(
            {
              width: el.offsetWidth,
              height: el.offsetHeight
            },
            bounds
          ) - 1; // This might be a little off... So minus one to be safe.

        if (newZoom <= gmap.getZoom()) {
          gmap.setCenter(bounds.getCenter());
          gmap.setZoom(gmap.getZoom() + 1);
        } else {
          gmap.fitBounds(bounds);
        }

        break;
    }
  }
  toggleFilters() {
    if (this.props.mapFilters.showing) {
      return this.props.hideFilters();
    }
    this.props.showFilters();
  }
  mapChangeHash() {
    return this._mapChangeHash;
  }
  handleFilterChange(type, val, checked) {
    this.props.filterChanged(type, val, checked);
    defer(() => this.loadPins());
  }
  handlePlaceSelect(suggest) {
    const gmap = get(this, "refs.map.__gmap");
    const bounds = get(suggest, "gmaps.geometry.bounds");
    const point = get(suggest, "location");

    if (bounds) {
      gmap.fitBounds(bounds);
      gmap.setZoom(15);
    } else if (point) {
      gmap.setCenter(point);
      gmap.setZoom(16);
    }
  }
  loadPins() {
    const { activeFilters } = this.props.mapFilters;
    const { organization, route } = this.props;
    const isPublic = this.isPublic();
    const org = isPublic ? window.pubOrgId : organization._id;

    if (!org) {
      return;
    }

    const cats = compact(
      map(activeFilters, f => {
        if (f.type === "category" && get(f, "value")) {
          return f.value;
        }
        return false;
      })
    );

    const photos = isEmpty(
      find(activeFilters, {
        type: "photo"
      })
    )
      ? 0
      : 1;

    const surveys = isEmpty(
      find(activeFilters, {
        type: "survey"
      })
    )
      ? 0
      : 1;

    const gmap = get(this, "refs.map.__gmap");

    if (!gmap) {
      return;
    }

    const proj = gmap.getProjection();
    const zoom = gmap.getZoom();
    const bounds = paddedBounds(gmap, 200);

    if (!proj) {
      return;
    }

    const sw = bounds.getSouthWest();
    const ne = bounds.getNorthEast();

    this.props.loadPins({
      org,
      cats,
      zoom,
      surveys,
      photos,
      bllng: sw.lng(),
      bllat: sw.lat(),
      urlng: ne.lng(),
      urlat: ne.lat()
    });
  }
  renderCategories() {
    const cats = sortBy(this.props.loadedProject.categories, ["name"]);
    if (cats) {
      return (
        <ul className="categories">
          {map(cats, cat => {
            return (
              <li key={"_cat" + cat._id}>
                <span>{cat.name}</span>
              </li>
            );
          })}
        </ul>
      );
    }
    return [];
  }
  modalDimensions() {
    const ratio = 4.5 / 3;
    const width = window.innerWidth;
    const height = window.innerHeight;
    let modalHeight;

    if (width - 20 > height) {
      modalHeight = height * 0.8;
    } else {
      modalHeight = height * 0.4;
    }

    let modalWidth = modalHeight * ratio;
    if (modalWidth >= width) {
      modalWidth = width - 60;
    }

    return {
      width: `${modalWidth}px`,
      height: `${modalHeight}px`
    };
  }
  renderAnswer(kind, value) {
    switch (kind) {
      case "star":
        const amount = parseInt(value, 10);
        const five = new Array(5);
        return (
          <div className="stars">
            {map(five, (num, idx) => {
              if (amount >= idx + 1) {
                return <i key={idx} className="fa fa-star" />;
              }
              return <i key={idx} className="fa fa-star-o" />;
            })}
          </div>
        );
      case "text":
        return <p>{value || "None."}</p>;
      case "boolean":
        if (parseInt(value, 10)) {
          return <p>Yes.</p>;
        }
        return <p>No.</p>;
      default:
        throw new Error("Unknown type of question. Fail.");
    }
  }
  renderReview(key) {
    const project = this.props.loadedProject;
    const pubQuestions = filter(project.survey, q => q.pub);

    if (!isEmpty(project) && pubQuestions.length) {
      const rows = chunk(pubQuestions, Math.ceil(pubQuestions.length / 2));
      return (
        <div key={key} className="review slide">
          <h4>Review</h4>
          {map(rows, (questions, idx) => {
            return (
              <div key={idx} className="col">
                {map(questions, (row, idx) => {
                  return (
                    <div className="question" key={idx}>
                      <h5>{row.question}</h5>
                      {this.renderAnswer(row.kind, row.answer)}
                    </div>
                  );
                })}
              </div>
            );
          })}
        </div>
      );
    }
    return null;
  }
  renderModalSlider() {
    const content = [];
    const photos = get(this, "props.loadedProject.photos", []);
    const survey = get(this, "props.loadedProject.survey", []);
    let c = 0;

    // Add in review if exists.
    if (survey.length) {
      content.push(this.renderReview(c++));
    }

    // Add in photos if exists.
    if (photos.length) {
      each(photos, photo => {
        const styles = assign(
          {
            backgroundImage: `url(${photo.path.large})`
          },
          this.modalDimensions()
        );

        let className = "slide img";

        if (photo.meta.height < photo.meta.width) {
          className += " landscape";
        }

        content.push(
          <div className={className} key={photo._id} style={styles} />
        );
      });
    } else {
      content.push(
        <div key={c++} className="slide" style={this.modalDimensions()}>
          <p className="no-images">
            This project does not have photos. Please check back at a later
            time!
          </p>
        </div>
      );
    }

    return (
      <Slider infinite={false} slidesToShow={1} slidesToScroll={1}>
        {content}
      </Slider>
    );
  }
  renderDetails() {
    const { loadedProject } = this.props;

    const photos = get(this, "props.loadedProject.photos", []);
    const survey = get(this, "props.loadedProject.survey", []);

    let msg = "";

    if (photos.length && survey.length) {
      msg = "Click or drag to see photos.";
    } else if (!survey.length && photos.length) {
      msg = "Click or drag to see more photos.";
    } else if (survey.length && !photos.length) {
      msg = "There are no photos available for this project yet.";
    } else if (!survey.length && !photos.length) {
      msg = "There is no survey or photos associated with this project yet.";
    }

    return (
      <div className="detail-bar">
        <span className="message">{msg}</span>
        <span className="address">
          {get(loadedProject, "street", "")},&nbsp;
          {get(loadedProject, "city", "")}
        </span>
      </div>
    );
  }
  renderUpgrade() {
    if (this.props.organization.active) {
      return null;
    }

    const props = {};
    const loc = window.location;
    const url = `${loc.protocol}//${loc.host}/account/subscription`;
    props.href = url;
    const isPublic = this.isPublic();

    if (isPublic) {
      props.target = "_blank";
    }

    props.label = `
      <span class='welcome-btn'><b>Welcome to Project Map It!</b>Please set up your plan before engaging with the map.</span>
      <ol>
        <li><p><strong>Ready.</strong>&nbsp;Choose a plan that's appropriate for your business.</p></li>
        <li><p><strong>Set.</strong>&nbsp;Configure and organize your projects.</p></li>
        <li><p><strong>Go.</strong>&nbsp;Start sending customer surveys and building your portfolio!</p></li>
        <li><p><strong>Profit.</strong>&nbsp;Rinse and repeat.</p></li>
      </ol>
    `;

    return (
      <div className="upgrade-overlay">
        <Button type="external" {...props} />
      </div>
    );
  }
  isPublic() {
    return this.props.route.path.match(/^public\//) ? true : false;
  }
  render() {
    const {
      projects,
      total,
      shareLink,
      hasPhotos,
      hasReviews,
      bbox,
      categories,
      isLoading,
      geolocating,
      mapOptions,
      mapChangeHash,
      organization,
      loadedProject,
      mapFilters,
      route
    } = this.props;

    const isPublic = this.isPublic();
    const publicSurvey = filter(
      get(this, "props.loadedProject.survey", []),
      obj => obj.pub
    );

    const modalDimensions = this.modalDimensions();

    let projectPins = projects;
    let projectModalClass = "project-modal";

    if (get(loadedProject, "photos.length")) {
      projectModalClass += " has-photos";
    } else {
      projectModalClass += " has-no-photos";
    }

    if (publicSurvey.length) {
      projectModalClass += " has-survey";
    } else {
      projectModalClass += " has-no-survey";
    }

    if (!organization.active) {
      projectPins = [];
    }

    return (
      <BasePage noSidebar={isPublic}>
        <Loader center={true} isLoading={isLoading}>
          <div>
            {this.renderUpgrade()}
            <MapFilter
              ref="filter"
              total={total}
              shareLink={shareLink}
              hasPhotos={hasPhotos}
              hasReviews={hasReviews}
              categories={categories}
              geolocating={geolocating}
              projects={projects}
              organizationName={get(organization, "name", "")}
              onFilterClick={() => this.toggleFilters()}
              onFilterChange={(type, val, checked) =>
                this.handleFilterChange(type, val, checked)
              }
              onLocateMeClick={this.props.findUserLocation}
              activeFilters={mapFilters.activeFilters}
              onPlaceSelect={suggest => this.handlePlaceSelect(suggest)}
              onProjectClick={id => this.handlePinClick("single", id)}
              filtersOpened={mapFilters.showing}
            />
            <GoogleMap
              ref="map"
              pins={projects}
              mapOptions={mapOptions}
              bbox={bbox}
              changeHash={mapChangeHash}
              onPinClick={(type, val) => this.handlePinClick(type, val)}
              onMapChange={() => this.loadPins()}
            />
          </div>
        </Loader>
        <BaseModal
          width={modalDimensions.width}
          height={modalDimensions.height}
          className={projectModalClass}
          display={this.props.showProjectModal}
          onExit={this.props.dismissProjectModal}
        >
          <Loader isLoading={this.props.modalIsLoading}>
            <div>
              {this.renderModalSlider()}
              {this.renderDetails()}
              {this.renderCategories()}
            </div>
          </Loader>
        </BaseModal>
      </BasePage>
    );
  }
}

const mapStateToProps = function(state) {
  const errors = concat(
    state.fetchedGeoProjects.errors,
    state.fetchedCategories.errors
  );

  const isLoading =
    state.fetchedCategories.isLoading ||
    state.fetchedMapDefaults.isLoading ||
    state.fetchedOrganization.isLoading;

  const modalIsLoading = state.fetchedProject.isLoading;

  if (errors.length && window.console) {
    console.warn("There are errors that are not displaying:", errors);
  }

  const total = get(state, "fetchedGeoProjects.response.results.total", 0);
  const hasReviews = get(
    state,
    "fetchedGeoProjects.response.results.hasReviews",
    0
  );
  const hasPhotos = get(
    state,
    "fetchedGeoProjects.response.results.hasPhotos",
    0
  );
  const bbox = get(state, "fetchedMapDefaults.response.bbox", null);
  const orgSlug = get(state, "fetchedOrganization.response.slug", "");
  const shareLink = `${window.location.origin}/public/${orgSlug}`;

  return {
    shareLink,
    total,
    hasReviews,
    bbox,
    hasPhotos,
    isLoading,
    mapChangeHash: state.mapChangeHash,
    modalIsLoading,
    user: state.user,
    organization: state.fetchedOrganization.response,
    showProjectModal: state.projectModal.launched,
    loadedProject: state.fetchedProject.response,
    projects: state.fetchedGeoProjects.response.results || {},
    categories: state.fetchedCategories.response.results || [],
    geolocating: state.userLocation.isLoading,
    mapOptions: state.mapOptions,
    mapFilters: state.mapFilters
  };
};

const mapDispatchToProps = function(dispatch, ownProps) {
  const organizationId = window.pubOrgId; // Always available.

  const loadPins = debounce(function(args) {
    dispatch(
      fetchGeoProjects(
        args.org,
        args.zoom,
        args.bllng,
        args.bllat,
        args.urlng,
        args.urlat,
        args.cats,
        args.photos,
        args.surveys
      )
    );
  }, 200);

  return {
    loadPins,
    load: orgSlug => {
      dispatch(fetchOrganization(orgSlug));
      dispatch(
        fetchCategories(-1, {
          organization: organizationId
        })
      );
      dispatch(fetchMapDefaults(orgSlug));
    },

    // When triggered when someone clicks outside the filter dropdown.
    onPageClick: (ref, event) => {
      const el = ReactDOM.findDOMNode(ref);

      if (!el.contains(event.target)) {
        dispatch(hideMapFilters());
      }
    },

    showFilters: () => dispatch(showMapFilters()),
    hideFilters: () => dispatch(hideMapFilters()),
    findUserLocation: () => dispatch(findUserLocation()),
    dismissProjectModal: () => dispatch(dismissProjectModal()),
    launchProjectModal: id => {
      dispatch(launchProjectModal(id));
      dispatch(fetchProject(id));
    },
    filterChanged: (type, value, checked) => {
      if (!checked) {
        dispatch(registerProjectFilter(type, value));
      } else {
        dispatch(unregisterProjectFilter(type, value));
      }
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Map);
