import React, { Component } from "react";
import PropTypes from "prop-types";
import { Link } from "react-router";
import { connect } from "react-redux";

class Menu extends Component {
  renderLink(item) {
    const props = {
      dangerouslySetInnerHTML: {
        __html: item.title
      }
    };

    if (item.disabled) {
      return <span className="disabled-lnk" {...props} />;
    }

    if (item.external) {
      return <a href={item.path} {...props} />;
    }

    return <Link activeClassName="active" to={item.path} {...props} />;
  }

  renderLi(item, idx) {
    if (item.hidden) {
      return null;
    }

    return (
      <li key={idx} className={item.className}>
        {this.renderLink(item)}
      </li>
    );
  }

  render() {
    return (
      <ul className="menu">
        {this.props.menu.map((item, idx) => this.renderLi(item, idx))}
      </ul>
    );
  }
}

Menu.propTypes = {
  type: PropTypes.string.isRequired,
  menu: PropTypes.arrayOf(
    PropTypes.shape({
      path: PropTypes.string.isRequired,
      title: PropTypes.string.isRequired,
      disabled: PropTypes.bool.isRequired,
      hidden: PropTypes.bool.isRequired,
      external: PropTypes.bool.isRequired
    })
  ).isRequired
};

const mapStateToProps = (state, ownProps) => {
  return {
    menu: state.menu[ownProps.type] || []
  };
};

export default connect(mapStateToProps)(Menu);
