import React, { Component } from 'react'
import { connect } from 'react-redux';
import { Link } from 'react-router';
import Tooltip from 'rc-tooltip';
import moment from 'moment';
import { map, assign, find, isEmpty, concat } from 'lodash';
import BasePage from '../components/BasePage';
import Table from '../components/Table';
import Button from '../components/Button';
import Loader from '../components/Loader';
import {
  fetchMembers,
  deleteMember,
  inviteMember
} from '../modules/member/actions';
import Authorize from './Authorize';

class Team extends Component {
  componentDidMount() {
    this.props.load();
  }
  removeMember(id) {
    this.props.removeMember(id);
  }
  inviteMember(id) {
    this.props.inviteMember(id);
  }
  formatTableData(results) {
    const { roles, permissions } = this.props;
    const cols = [
      {
        key: 'name',
        label: 'Name'
      },
      {
        key: 'created',
        label: 'Member Since'
      },
      {
        key: 'role',
        label: 'Role'
      },
      {
        key: '_action',
        label: 'Action'
      }
    ];
    const resendBtn = row => {
      if (row.unconfirmed) {
        return (
          <Tooltip animation='zoom' placement="bottom" overlay={<span>Resend Invitation</span>}>
            <button onClick={e => this.inviteMember(row._id)}>
              <i className='fa fa-paper-plane alittlesmaller' />
            </button>
          </Tooltip>
        );
      }
      return null;
    };
    const deleteBtn = row => {
      // Users can't remove themselves and can't remove owners.
      if (row._id === this.props.user._id || row.role === 1) {
        return null;
      }
      return (
        <Authorize roles={permissions.CAN_DELETE_MEMBERS}>
          <Tooltip animation='zoom' placement="bottom" overlay={<span>Delete Member</span>}>
            <button onClick={e => this.removeMember(row._id)}>
              <i className='fa fa-times-circle-o selectable' />
            </button>
          </Tooltip>
        </Authorize>
      );
    };
    const editBtn = row => {
      // Non-owners can't edit owners.
      if (this.props.user.role !== 1 && row.role === 1) {
        return null;
      }

      return (
        <Tooltip animation='zoom' placement="bottom" overlay={<span>Edit Member</span>}>
          <Link title='Edit User' to={`/team/edit/${row._id}`}>
            <i className='fa fa-cog'></i>
          </Link>
        </Tooltip>
      );
    };
    const rows = map(results, (row) => {
      return assign({}, row, {
        created: moment(row.created).format('MMMM Do, YYYY'),
        role: find(roles, r => r.id === row.role).name,
        _action: (
          <div className='control-icons'>
            {editBtn(row)}
            {deleteBtn(row)}
            {resendBtn(row)}
          </div>
        )
      });
    });
    return { rows, cols };
  }
  render() {
    const { confirmed, unconfirmed, isLoading } = this.props;
    const confirmedTableData = this.formatTableData(confirmed);
    const unconfirmedTableData = this.formatTableData(unconfirmed);

    return (
      <BasePage menu='team'>
        <h1>
          <span>Your Team</span>
          <Button type='link' to='/team/create' label='New Member' />
        </h1>
        <Loader isLoading={isLoading}>
          <Table rows={confirmedTableData.rows} cols={confirmedTableData.cols} />
        </Loader>
        <div className='component-space' />
        <h3>Requested Members</h3>
        <Loader isLoading={isLoading}>
          <Table rows={unconfirmedTableData.rows} cols={unconfirmedTableData.cols} />
        </Loader>
      </BasePage>
    );
  }
}

const mapStateToProps = function(state, ownProps) {
  const members = state.fetchedMembers.response.results || [];
  const isLoading = state.invitedMember.isLoading ||
                    state.fetchedMembers.isLoading ||
                    state.deletedMember.isLoading;

  return {
    user: state.user,
    roles: state.roles,
    permissions: state.permissions,
    confirmed: members.filter(u => !u.unconfirmed),
    unconfirmed: members.filter(u => u.unconfirmed),
    isLoading
  };
};

const mapDispatchToProps = function(dispatch, ownProps) {
  return {
    load: () => dispatch(fetchMembers()),
    removeMember: (id) => {
      if (confirm('Are you sure you want to remove this member from your team? You can not undo this action.')) {
        dispatch(deleteMember(id, 'Member deleted.')).then(() => dispatch(fetchMembers()));
      }
    },
    inviteMember: (id) => {
      if (confirm('Are you sure you want to re-send an invitation to this user?')) {
        dispatch(inviteMember(id, 'Invitation re-sent!')).then(() => dispatch(fetchMembers()));
      }
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Team);
