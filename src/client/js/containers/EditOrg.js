import React, { Component } from "react";
import { isEmpty, isArray } from "lodash";
import { connect } from "react-redux";
import OrgForm from "./forms/OrgForm";
import Loader from "../components/Loader";
import BasePage from "../components/BasePage";
import {
  fetchOrganization,
  updateOrganization
} from "../modules/organization/actions";

class EditOrg extends Component {
  componentDidMount() {
    this.props.load();
  }
  render() {
    const { isLoading, isUpdating, onSubmit, initialValues } = this.props;
    return (
      <BasePage menu="account">
        <h1>Organization Details</h1>

        <Loader isLoading={isLoading}>
          <OrgForm
            initialValues={initialValues}
            updating={isUpdating}
            onSubmit={onSubmit}
          />
        </Loader>
      </BasePage>
    );
  }
}

const mapStateToProps = function(state, ownProps) {
  const isLoading = state.fetchedOrganization.isLoading;
  const isUpdating = state.updatedOrganization.isLoading;
  const loadedOrganization = state.fetchedOrganization.response;
  const updatedMember = state.updatedOrganization.response;

  return {
    isLoading,
    isUpdating,
    initialValues: loadedOrganization
  };
};

const mapDispatchToProps = function(dispatch, ownProps) {
  return {
    load: () => dispatch(fetchOrganization("me")),
    onSubmit: values => {
      dispatch(updateOrganization(values, "Organization updated."));
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(EditOrg);
