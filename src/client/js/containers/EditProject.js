import React, { Component } from "react";
import Sortable from "react-anything-sortable";
import { isEmpty, isArray, concat, map, assign, get, debounce } from "lodash";
import { each } from "async";
import { browserHistory } from "react-router";
import { connect } from "react-redux";
import Loader from "../components/Loader";
import BasePage from "../components/BasePage";
import PreviewPhoto from "../components/PreviewPhoto";
import ProjectForm from "./forms/ProjectForm";
import StaticGoogleMap from "../components/StaticGoogleMap";
import Dropzone from "react-dropzone";
import {
  fetchProject,
  fetchProjectLocation,
  updateProject,
  updateProjectPhotoOrder,
  uploadProjectPhoto,
  deleteProjectPhoto
} from "../modules/project/actions";
import { fetchCategories } from "../modules/category/actions";
import io from "socket.io-client";

class EditProject extends Component {
  componentDidMount() {
    this.props.load();

    const socket = io();

    // On image load.
    socket.on(
      this.props.routeParams.projectId,
      debounce(() => {
        this.props.forceProjectReload();
      }, 1500)
    );
  }
  manualUpload(e) {
    e.preventDefault();
    this.refs.dropzone.open();
  }
  renderStaticMap() {
    if (!isEmpty(this.props.projectLocation)) {
      return (
        <StaticGoogleMap
          latitude={this.props.projectLocation[1]}
          longitude={this.props.projectLocation[0]}
        />
      );
    }
    return null;
  }
  renderExistingPhotos() {
    const photos = get(this, "props.formProps.initialValues.photos", []);
    if (!photos.length) {
      return null;
    }
    return (
      <div className="existing-images">
        <Sortable onSort={this.props.onSort}>
          {map(this.props.formProps.initialValues.photos, (image, idx) => {
            return (
              <PreviewPhoto
                className="preview-image"
                sortData={image._id}
                key={image._id}
                id={image._id}
                onPhotoDelete={id => this.props.onPhotoDelete(id)}
                path={get(image, "path.small")}
              />
            );
          })}
        </Sortable>
      </div>
    );
  }
  renderPhotos() {
    return (
      <div className="control-images">
        <Dropzone
          accept="image/jpeg, image/png, image/jpg"
          className="drop-area"
          onDrop={this.props.onPhotoUpload}
        >
          <p className="text-center">
            Drag in images for your project here. You may drop in up to 5 photos
            at a time. Images must be under 15mb in size.
          </p>
        </Dropzone>
        {this.renderExistingPhotos()}
      </div>
    );
  }
  render() {
    const {
      isDetailsLoading,
      isPhotosLoading,
      onSubmit,
      formProps
    } = this.props;

    return (
      <BasePage menu="projects">
        <h1>{formProps.isNew ? "Create Project" : "Edit Project"}</h1>

        {this.renderStaticMap()}

        <h3>Project Images</h3>

        <Loader isLoading={isPhotosLoading}>
          <div>{this.renderPhotos()}</div>
        </Loader>

        <br />
        <br />
        <h3>Project Details</h3>
        <Loader isLoading={isDetailsLoading}>
          <div>
            <ProjectForm onSubmit={onSubmit} {...formProps} />
          </div>
        </Loader>
      </BasePage>
    );
  }
}

const mapStateToProps = function(state, ownProps) {
  const isPhotosLoading = state.uploadedProjectPhoto.isLoading;

  const isDetailsLoading =
    state.fetchedProject.isLoading || state.fetchedCategories.isLoading;

  const initialValues = assign({}, state.fetchedProject.response, {
    categories: map(state.fetchedProject.response.categories, o => o._id)
  });

  const projectLocation = get(
    state,
    "fetchedProjectLocation.response.location",
    []
  );

  const props = {
    projectLocation,
    isDetailsLoading,
    isPhotosLoading,
    formProps: {
      availableCategories: state.fetchedCategories.response.results || [],
      initialValues,
      requesting: state.updatedProject.isLoading,
      isNew: false
    }
  };

  return props;
};

const mapDispatchToProps = function(dispatch, ownProps) {
  const projectId = ownProps.params.projectId;

  const organizationId = window.pubOrgId;

  return {
    load: () => {
      dispatch(
        fetchCategories(-1, {
          organization: organizationId
        })
      );
      dispatch(fetchProject(projectId));
      dispatch(fetchProjectLocation(projectId));
    },
    onSort: order => dispatch(updateProjectPhotoOrder(order)),
    onPhotoUpload: files => {
      const noun = files.length > 1 ? "Photos" : "Photo";
      dispatch(
        uploadProjectPhoto(
          projectId,
          files,
          noun + " images uploaded for processing. This may take a few moments."
        )
      ).then(() => {
        dispatch(fetchProject(projectId));
      });
    },
    forceProjectReload: () => {
      dispatch(fetchProject(projectId));
    },
    onPhotoDelete: id => {
      dispatch(deleteProjectPhoto(id, "Photo removed.")).then(() => {
        dispatch(fetchProject(projectId));
      });
    },
    onSubmit: vals => {
      dispatch(
        updateProject(ownProps.params.projectId, vals, "Project updated.")
      );
      setTimeout(() => {
        dispatch(fetchProjectLocation(projectId));
      }, 3000);
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(EditProject);
