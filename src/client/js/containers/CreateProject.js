import React, { Component } from "react";
import { isEmpty, isArray, concat } from "lodash";
import { browserHistory } from "react-router";
import { connect } from "react-redux";
import Loader from "../components/Loader";
import BasePage from "../components/BasePage";
import ProjectForm from "./forms/ProjectForm";
import { createProject } from "../modules/project/actions";
import { fetchCategories } from "../modules/category/actions";

class CreateProject extends Component {
  componentDidMount() {
    this.props.load();
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.createdProject && nextProps.createdProject._id) {
      this.props.redirectToProject(nextProps.createdProject._id);
    }
  }
  render() {
    const { isLoading, onSubmit, formProps, user, plans } = this.props;

    return (
      <BasePage menu="projects">
        <h1>Create Project</h1>

        <Loader isLoading={isLoading}>
          <ProjectForm
            onSubmit={vals => onSubmit(user, plans, vals)}
            {...formProps}
          />
        </Loader>
      </BasePage>
    );
  }
}

const mapStateToProps = function(state, ownProps) {
  const createdResponse = state.createdProject.response;
  const props = {
    user: state.user,
    plans: state.plans,
    isLoading: false,
    createdProject: createdResponse,
    formProps: {
      availableCategories: state.fetchedCategories.response.results || [],
      requesting: state.createdProject.isLoading,
      isNew: true
    }
  };

  return props;
};

const mapDispatchToProps = function(dispatch, ownProps) {
  const orgId = window.pubOrgId;
  return {
    load: () =>
      dispatch(
        fetchCategories(-1, {
          organization: orgId
        })
      ),
    onSubmit: (user, plans, vals) => {
      dispatch(createProject(vals, "Project created."));
    },
    redirectToProject: id => browserHistory.push(`/projects/edit/${id}`)
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CreateProject);
