import React, { Component } from "react";
import { isEmpty, concat, assign } from "lodash";
import { connect } from "react-redux";
import { browserHistory } from "react-router";
import AccountDeactivateForm from "./forms/AccountDeactivateForm";
import BasePage from "../components/BasePage";
import BaseModal from "../components/BaseModal";
import Loader from "../components/Loader";
import AccountForm from "./forms/AccountForm";
import { showGrowler } from "../modules/common/actions";
import { fetchMember, updateMember } from "../modules/member/actions";
import {
  deactivateAccount,
  toggleAccountDeactivateModal
} from "../modules/account/actions";

class Account extends Component {
  componentDidMount() {
    this.props.load(this.props.user._id);
  }
  render() {
    const {
      user,
      isLoading,
      requesting,
      initialValues,
      permissions,
      showAcctDeactivateModal,
      onSubmit
    } = this.props;

    return (
      <BasePage menu="account">
        <h1>Edit Member</h1>

        <Loader isLoading={isLoading}>
          <AccountForm
            permissions={permissions}
            user={user}
            initialValues={initialValues}
            onAccountRemove={this.props.toggleDeactivateModal}
            onSubmit={vals => onSubmit(user._id, vals)}
            requesting={requesting}
          />
        </Loader>

        <BaseModal
          onExit={this.props.toggleDeactivateModal}
          display={showAcctDeactivateModal}
        >
          <AccountDeactivateForm
            onSubmit={this.props.onAccountDeactivate}
            requesting={this.props.acctDeactivateRequesting}
          />
        </BaseModal>
      </BasePage>
    );
  }
}

const prepValuesForServer = (obj = {}) => {
  const newObj = assign({}, obj);

  if (newObj.organization) {
    newObj.orgname = obj.organization.name;
    newObj.social_google = obj.organization.social_google;
    newObj.social_facebook = obj.organization.social_facebook;
    newObj.social_yelp = obj.organization.social_yelp;
    newObj.social_houzz = obj.organization.social_houzz;
    newObj.social_bbb = obj.organization.social_bbb;
    newObj.social_angies_list = obj.organization.social_angies_list;
    newObj.social_home_advisor = obj.organization.social_home_advisor;

    delete newObj.organization;
  }

  if (isEmpty(newObj.password)) {
    delete newObj.password;
    delete newObj.confirmPassword;
  }

  return newObj;
};

const mapStateToProps = function(state, ownProps) {
  const isLoading = state.fetchedMember.isLoading;
  const requesting = state.updatedMember.isLoading;

  if (!isEmpty(state.deactivatedAccount.response)) {
    window.location = "/logout";
  }

  return {
    showAcctDeactivateModal: state.accountDeactivateState.modalShowing,
    acctDeactivateRequesting: state.deactivatedAccount.isLoading,
    permissions: state.permissions,
    user: state.user,
    initialValues: prepValuesForServer(state.fetchedMember.response),
    isLoading,
    requesting
  };
};

const mapDispatchToProps = function(dispatch, ownProps) {
  return {
    load: id => dispatch(fetchMember(id)),
    onSubmit: (id, vals) => {
      return dispatch(
        updateMember(
          id,
          prepValuesForServer(vals),
          "Your account has been updated successfully."
        )
      );
    },
    onAccountDeactivate: vals => dispatch(deactivateAccount(vals.reason)),
    toggleDeactivateModal: () => dispatch(toggleAccountDeactivateModal())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Account);
