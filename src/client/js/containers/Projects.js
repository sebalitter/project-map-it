import React, { Component } from "react";
import {
  forEach,
  get,
  concat,
  isEmpty,
  map,
  assign,
  debounce,
  filter
} from "lodash";
import { Link } from "react-router";
import ReactPaginate from "react-paginate";
import { connect } from "react-redux";
import Tooltip from "rc-tooltip";
import Papa from "papaparse";
import moment from "moment";
import BasePage from "../components/BasePage";
import BaseModal from "../components/BaseModal";
import Authorize from "./Authorize";
import Button from "../components/Button";
import Loader from "../components/Loader";
import Table from "../components/Table";
import SendSurveyForm from "./forms/SendSurveyForm";
import ProjectExportForm from "./forms/ProjectExportForm";
import ImportProjectsForm from "./forms/ImportProjectsForm";
import { DISPLAY_FORMAT } from "../utils/date.js";
import XLSX from "xlsx";

import { fetchQuestions } from "../modules/questionnaire/actions";

import {
  fetchProjects,
  removeAllProjects,
  deleteProject,
  sendAllSurveys,
  showSurveyModal,
  hideSurveyModal,
  fetchProjectsForDownload,
  updateProject,
  createProjectsBulk,
  sendSurvey,
  toggleProjectExportModal,
  toggleProjectImportModal,
  toggleProjectReviewExportModal
} from "../modules/project/actions";

import { fetchCategories } from "../modules/category/actions";

class Projects extends Component {
  componentDidMount() {
    this.props.load();
  }
  renderRemoveAllButton() {
    if (!this.props.projects.length) {
      return null;
    }
    const handleClick = e => {
      this.props.handleRemoveAllProjects();
      e.preventDefault();
    };
    return (
      <button className="remove-all-projects" onClick={handleClick}>
        Remove all projects
      </button>
    );
  }
  renderSendAllSurveysButton() {
    if (!this.props.projects.length) {
      return null;
    }
    const handleClick = e => {
      this.props.handleSendAllSurveysProjects();
      e.preventDefault();
    };
    return (
      <button className="send-all-surveys" onClick={handleClick}>
        Send all unsent surveys
      </button>
    );
  }
  formatTableData(results) {
    const { roles, permissions } = this.props;
    const cols = [
      {
        key: "created",
        label: "Created"
      },
      {
        key: "name",
        label: "Name"
      },
      {
        key: "state",
        label: "State"
      },
      {
        key: "city",
        label: "City"
      },
      {
        key: "photos",
        label: "# of Photos"
      },
      {
        key: "_action",
        label: "Action"
      }
    ];
    const deleteBtn = rowId => {
      if (rowId !== this.props.user._id) {
        return (
          <Authorize roles={permissions.CAN_DELETE_PROJECTS}>
            <Tooltip
              animation="zoom"
              placement="bottom"
              overlay={<span>Delete Project</span>}
            >
              <button onClick={e => this.props.deleteProject(rowId)}>
                <i className="fa fa-times-circle-o selectable" />
              </button>
            </Tooltip>
          </Authorize>
        );
      }
      return null;
    };
    const surveyBtn = row => {
      if (!row.surveySent && !row.surveyCompleted) {
        if (!this.props.hasSurveyQuestions) {
          return (
            <Tooltip
              animation="zoom"
              placement="bottom"
              overlay={
                <span>
                  <i>No Questions Available</i>
                </span>
              }
            >
              <button onClick={function() {}}>
                <i className="fa fa-star-o selectable" />
              </button>
            </Tooltip>
          );
        }
        return (
          <Tooltip
            animation="zoom"
            placement="bottom"
            overlay={<span>Send Survey</span>}
          >
            <button
              onClick={() =>
                this.props.showSurveyModal(
                  row._id,
                  row.surveyEmails,
                  row.survey
                )
              }
            >
              <i className="fa fa-star-o selectable" />
            </button>
          </Tooltip>
        );
      } else if (row.surveySent && !row.surveyCompleted) {
        return (
          <Tooltip
            animation="zoom"
            placement="bottom"
            overlay={<span>Re-Send Survey</span>}
          >
            <button
              onClick={() =>
                this.props.showSurveyModal(
                  row._id,
                  row.surveyEmails,
                  row.survey
                )
              }
            >
              <i className="fa fa-star-half-o selectable" />
            </button>
          </Tooltip>
        );
      } else {
        return (
          <Tooltip
            animation="zoom"
            placement="bottom"
            overlay={<span>View Survey</span>}
          >
            <button
              onClick={() =>
                this.props.showSurveyModal(
                  row._id,
                  row.surveyEmails,
                  row.survey
                )
              }
            >
              <i className="fa fa-star selectable" />
            </button>
          </Tooltip>
        );
      }
      return null;
    };
    const directSurveyBtn = row => {
      if (row.surveyCompleted) {
        return null;
      }
      return (
        <Tooltip
          animation="zoom"
          placement="bottom"
          overlay={<span>Conduct Survey</span>}
        >
          <button onClick={() => window.open(`/survey/${row._id}`)}>
            <i className="fa fa-arrow-circle-o-right selectable" />
          </button>
        </Tooltip>
      );
    };
    const geoWarning = row => {
      const createdDiff = moment().diff(row.created, "minute");

      if (createdDiff > 5 && isEmpty(get(row, "location", []))) {
        return (
          <Tooltip
            animation="zoom"
            placement="bottom"
            overlay={<span>Invalid address.</span>}
          >
            <i className="fa fa-exclamation-triangle color-warning" />
          </Tooltip>
        );
      }
      return null;
    };
    const rows = map(results, row => {
      return assign({}, row, {
        created: moment(row.created).format("MMMM Do, YYYY"),
        photos: row.photos && row.photos.length ? row.photos.length : 0,
        _action: (
          <div className="control-icons">
            <Tooltip
              animation="zoom"
              placement="bottom"
              overlay={<span>Edit Project</span>}
            >
              <Link to={`/projects/edit/${row._id}`}>
                <i className="fa fa-cog" />
              </Link>
            </Tooltip>
            {deleteBtn(row._id)}
            {surveyBtn(row)}
            {directSurveyBtn(row)}
            {geoWarning(row)}
          </div>
        )
      });
    });
    return { rows, cols };
  }
  render() {
    const {
      page,
      totalCount,
      totalPages,
      permissions,
      projects,
      isLoading,
      importInfo,
      projectImportRequesting,
      categories
    } = this.props;

    const tableData = this.formatTableData(projects);
    const surveyModalFormInitialValues = {
      id: this.props.surveyModal.id,
      email: map(this.props.surveyModal.emails, "email").join(", ")
    };

    return (
      <BasePage menu="projects">
        <h1>
          <span>Projects {totalCount}</span>
          <Button type="link" to="/projects/create" label="New Project" />
        </h1>

        <div className="table-search">
          <input
            type="text"
            onChange={this.props.onSearchInputChange}
            placeholder="Search name, state, street or zip."
          />
          <i className="fa fa-search" />
        </div>

        <Loader isLoading={isLoading}>
          <div>
            <Table rows={tableData.rows} cols={tableData.cols} />
            <ReactPaginate
              containerClassName="pagination"
              forceSelected={page - 1}
              pageCount={totalPages}
              pageRangeDisplayed={5}
              marginPagesDisplayed={2}
              previousLabel={<i className="fa fa-angle-double-left" />}
              nextLabel={<i className="fa fa-angle-double-right" />}
              onPageChange={this.props.onPageClick}
            />
            {this.renderRemoveAllButton()}
            {this.renderSendAllSurveysButton()}
          </div>
        </Loader>

        <Authorize roles={permissions.CAN_EXPORT_IMPORT_PROJECTS}>
          <div>
            <div className="component-space" />
            <h4>Import/ Export Projects</h4>
            <p>
              Import your existing projects into Project Map It by importing
              either a .csv or an Excel file. Make sure each column in your
              spreadsheet has a title (e.g. 'Address' or 'Project Name').
            </p>
            <br />
            <Button
              type="button"
              label="Export"
              onClick={this.props.toggleProjectExportModal}
            />
            <span>&nbsp;</span>
            <Button
              type="file"
              label="Import"
              onChange={e => this.props.onImportFileUpload(e)}
            />
          </div>
        </Authorize>

        {/*
        <Authorize roles={permissions.CAN_EXPORT_IMPORT_PROJECTS}>
          <div>
            <div className='component-space' />
            <h4>Export Customer Reviews</h4>
            <p>Export the customer reviews associated with your projects.</p>
            <br />
            <Button
              type='button'
              label='Export'
              onClick={this.props.toggleProjectReviewExportModal}
            />
          </div>
        </Authorize>
        */}

        <BaseModal
          onExit={this.props.hideSurveyModal}
          className="send-survey-email-modal"
          display={this.props.surveyModal.showing}
        >
          <SendSurveyForm
            initialValues={surveyModalFormInitialValues}
            onSubmit={vals => this.props.sendSurvey(vals.id, vals.email)}
            completedSurvey={this.props.surveyModal.survey}
            onPrivacyChange={survey =>
              this.props.updateQuestions(this.props.surveyModal.id, survey)
            }
            requesting={this.props.surveyModalRequesting}
          />
        </BaseModal>

        <BaseModal
          display={this.props.showProjectExportModal}
          onExit={this.props.toggleProjectExportModal}
        >
          <div>
            <h2>Export Projects</h2>
            <ProjectExportForm
              isLoading={false}
              requesting={false}
              initialValues={{
                from: moment()
                  .subtract(5, "day")
                  .format(DISPLAY_FORMAT),
                to: moment().format(DISPLAY_FORMAT)
              }}
              onSubmit={vals => {
                this.props.exportProjects(
                  moment(vals.from, DISPLAY_FORMAT),
                  moment(vals.to, DISPLAY_FORMAT)
                );
              }}
              categories={categories}
            />
          </div>
        </BaseModal>

        <BaseModal
          display={this.props.showProjectImportModal}
          onExit={this.props.toggleProjectImportModal}
          isLoading={projectImportRequesting}
        >
          <ImportProjectsForm
            requesting={projectImportRequesting}
            onProcessed={this.props.importProjects}
            csvCols={importInfo.fields}
            csvData={importInfo.data}
          />
        </BaseModal>

        {/* NOT IMPLEMENTED */}
        <BaseModal
          display={this.props.showProjectReviewExportModal}
          onExit={this.props.toggleProjectReviewExportModal}
        >
          <h2>Export Projects Reviews</h2>
          <ProjectExportForm
            isLoading={false}
            requesting={false}
            initialValues={{
              from: moment()
                .subtract(5, "day")
                .format(DISPLAY_FORMAT),
              to: moment().format(DISPLAY_FORMAT)
            }}
            onSubmit={vals =>
              this.props.exportProjectReviews(vals.from, vals.to)
            }
            categories={categories}
          />
        </BaseModal>
        {/* NOT IMPLEMENTED */}
      </BasePage>
    );
  }
}

/**
 * Returns all projects with addresses that could not be geolocated.
 *
 * @TODO Should implement on backend.
 * @return array
 */
function projectsWithInvalidAddresses(projects) {
  return filter(projects || [], project => {
    const minutes = moment().diff(project.created);
    return minutes > 5 && isEmpty(get(project, "location", []));
  });
}

const mapStateToProps = function(state, ownProps) {
  const projects = state.fetchedProjects.response.results || [];

  const page = state.fetchedProjects.response.page
    ? parseInt(state.fetchedProjects.response.page, 10)
    : 1;

  const totalPages = state.fetchedProjects.response.pages || 1;

  let totalCount = get(state, "fetchedProjects.response.totalCount", "");

  if (totalCount) {
    totalCount = `(${totalCount})`;
  }

  const hasSurveyQuestions = get(
    state,
    "fetchedQuestions.response.results.length",
    0
  )
    ? true
    : false;

  const isLoading =
    state.fetchedMembers.isLoading ||
    state.deletedMember.isLoading ||
    state.deletedAllProjects.isLoading ||
    state.sendAllSurveys.isLoading ||
    state.fetchedCategories.isLoading;

  const categories = state.fetchedCategories.response.results || [];

  const {
    showProjectExportModal,
    showProjectImportModal,
    showProjectReviewExportModal,
    importInfo
  } = state.projectModalState;

  return {
    hasSurveyQuestions,
    page,
    totalCount,
    totalPages,
    categories,
    showProjectExportModal,
    showProjectImportModal,
    showProjectReviewExportModal,
    importInfo,
    projectImportRequesting: state.createdBulkProjects.isLoading,
    surveyModal: state.surveyModal,
    surveyModalRequesting: state.emailedSurvey.isLoading,
    user: state.user,
    roles: state.roles,
    permissions: state.permissions,
    projects,
    isLoading
  };
};

const mapDispatchToProps = function(dispatch, ownProps) {
  let persistentFilters = {};
  const persistentSort = {
    created: -1
  };
  let persistentPage = 1;
  const orgId = window.pubOrgId;
  return {
    load: () => {
      dispatch(fetchProjects(1, null, persistentSort));
      dispatch(
        fetchCategories(-1, {
          organization: orgId
        })
      );
      dispatch(fetchQuestions());
    },
    onSearchInputChange: e => {
      const val = e.target.value;

      if (isEmpty(val)) {
        persistentFilters = {};
      } else {
        persistentFilters = {
          $text: {
            $search: val
          }
        };
      }
      dispatch(fetchProjects(1, persistentFilters, persistentSort));
    },
    handleRemoveAllProjects: () => {
      if (
        confirm(
          "Are you sure you want to remove all of your projects? This action can not be undone."
        )
      ) {
        dispatch(removeAllProjects("All projects removed.")).then(() =>
          dispatch(fetchProjects(1, null, persistentSort))
        );
      }
    },
    handleSendAllSurveysProjects: () => {
      if (
        confirm(
          "Are you sure you want to re-send surveys to all customers who have received surveys in the past but have not yet completed them?"
        )
      ) {
        dispatch(sendAllSurveys("All surveys sent.")).then(() =>
          dispatch(fetchProjects(1, null, persistentSort))
        );
      }
    },
    updateQuestions: (projectId, survey) => {
      dispatch(updateProject(projectId, { survey }, "Survey updated."));
    },
    hideSurveyModal: () => {
      // Reload projects on close to reflect any survey changes.
      dispatch(
        fetchProjects(persistentPage, persistentFilters, persistentSort)
      );

      dispatch(hideSurveyModal());
    },
    showSurveyModal: (id, emails, survey) =>
      dispatch(showSurveyModal(id, emails, survey)),
    sendSurvey: (id, emails) =>
      dispatch(sendSurvey(id, emails)).then(() =>
        dispatch(
          fetchProjects(persistentPage, persistentFilters, persistentSort)
        )
      ),
    toggleProjectExportModal: () => dispatch(toggleProjectExportModal()),
    toggleProjectImportModal: () => dispatch(toggleProjectImportModal()),
    toggleProjectReviewExportModal: () =>
      dispatch(toggleProjectReviewExportModal()),
    onPageClick: data => {
      persistentPage = data.selected + 1;
      dispatch(
        fetchProjects(persistentPage, persistentFilters, persistentSort)
      );
    },
    importProjects: projects =>
      dispatch(createProjectsBulk(projects, "Projects imported.")).then(() =>
        dispatch(fetchProjects(1, null, persistentSort))
      ),
    exportProjects: (from, to) => dispatch(fetchProjectsForDownload(from, to)),
    exportProjectReviews: vals =>
      console.log("importing project reviews", vals),
    deleteProject: id => {
      if (
        confirm(
          "Are you sure you want to remove this project? You can not undo this action."
        )
      ) {
        dispatch(deleteProject(id, "Project deleted.")).then(() =>
          dispatch(fetchProjects(1, null, persistentSort))
        );
      }
    },
    onImportFileUpload: e => {
      const { target } = e;
      const item = target.files.item(0);

      if (!item) {
        return;
      }
      // I'd like to use the mimetype to determine file type but IE doen't
      // support mimetype. So just using the file name extension.
      const { name } = item;
      const ext = name.split(".").pop();

      switch (ext.toLowerCase()) {
        case "csv":
          Papa.parse(item, {
            header: true,
            complete: results => {
              dispatch(
                toggleProjectImportModal({
                  errors: map(results.errors, "message"),
                  fields: map(results.meta.fields, name => {
                    return {
                      label: name,
                      value: name
                    };
                  }),
                  data: results.data
                })
              );
            }
          });
          break;
        case "xlsb":
        case "xls":
        case "xlsx":
          const reader = new FileReader();
          reader.onload = e => {
            try {
              const data = e.target.result;
              const wb = XLSX.read(data, { type: "binary" });
              const wsname = wb.SheetNames[0];
              const ws = wb.Sheets[wsname];
              const json = XLSX.utils.sheet_to_json(ws, {
                header: 1
              });
              const header = json.shift();

              const rowData = json.map(row => {
                const rowObj = {};
                forEach(row, (val, idx) => {
                  rowObj[header[idx]] = val;
                });
                return rowObj;
              });

              dispatch(
                toggleProjectImportModal({
                  errors: [],
                  fields: map(header, name => {
                    return {
                      label: name,
                      value: name
                    };
                  }),
                  data: rowData
                })
              );
            } catch (err) {
              console.warn("Error while trying to parse excel file.");
              console.warn(err);
              alert(
                "We had an issue reading your file. Please check the formatting and try again."
              );
            }
          };
          reader.readAsBinaryString(item);
          break;
        default:
          console.warn("File format:", ext);
          alert("You must import a CSV or Excel file.");
      }

      // We must set the value to null to handle multiple file uploads in
      // a row. Weirdness.
      // http://stackoverflow.com/a/11280934/4263296
      target.value = null;
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Projects);
