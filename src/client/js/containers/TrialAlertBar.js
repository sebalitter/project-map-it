import React, { Component } from "react";
import { Link } from "react-router";
import { connect } from "react-redux";
import PropTypes from "prop-types";

class TrialAlertBar extends Component {
  render() {
    if (!this.props.user || !this.props.user.organization) {
      return null;
    }

    const org = this.props.user.organization;

    if (!org.active) {
      return (
        <Link to={"account/subscription"}>
          <div className="trial-alert-bar">
            <span>
              Your free trial period has ended. Click{" "}
              <b className="underline">here</b> to select a subscription or to
              add a payment method.
            </span>
          </div>
        </Link>
      );
    }

    if (!org.trialing) {
      return null;
    }

    return (
      <Link to={"account/subscription"}>
        <div className="trial-alert-bar">
          <span>Your trial period will end in {org.timeLeftOnTrial}.</span>
        </div>
      </Link>
    );
  }
}

TrialAlertBar.propTypes = {
  user: PropTypes.object
};

const mapStateToProps = (state, ownProps) => {
  return {
    user: state.user
  };
};

export default connect(mapStateToProps)(TrialAlertBar);
