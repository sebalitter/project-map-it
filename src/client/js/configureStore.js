import { createStore, applyMiddleware, combineReducers } from "redux";
import thunkMiddleware from "redux-thunk";
import { reducers } from "./modules/core";
import ReactGA from "react-ga";
import { get } from "lodash";

ReactGA.initialize("UA-81857493-1");

const track = store => next => action => {
  if (action.type === "@@router/LOCATION_CHANGE") {
    ReactGA.set({
      userId: get(window.initialState, "user._id"),
      page: action.payload.pathname
    });
    ReactGA.pageview(action.payload.pathname);
  }
  return next(action);
};

export default function configureStore(initialState) {
  return createStore(
    combineReducers(reducers),
    initialState,
    applyMiddleware(thunkMiddleware, track)
  );
}
