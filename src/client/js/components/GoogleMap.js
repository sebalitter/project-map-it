/*global google */
import React, { Component } from "react";
import { ReactDOM } from "react-dom";
import PropTypes from "prop-types";
import {
  values,
  keys,
  each,
  difference,
  isEmpty,
  debounce,
  defer,
  isNil
} from "lodash";
import MapClusterPoint from "../utils/MapClusterPoint";

export default class GoogleMap extends Component {
  constructor(props) {
    super(props);
    this._pinCache = {};
    this.state = {
      zoom: null,
      map: null,
      center: {
        lat: 30,
        lng: -30
      }
    };
    this.placePins = debounce(this.placePins, 220);
  }
  latLng(lat, lng) {
    return new google.maps.LatLng({ lat, lng });
  }
  mapReady(map) {
    this.__gmap = map;
    this.placePins();
    this.props.onMapChange();
  }
  componentDidMount() {
    const map = new google.maps.Map(this.refs.mapCanvas, this.props.mapOptions);

    map.addListener("center_changed", () => {
      const pos = map.getCenter();
      this.setState({
        center: {
          lat: pos.lat(),
          lng: pos.lng()
        }
      });
      this.onUpdate();
    });

    map.addListener("zoom_changed", () => {
      this.setState({
        zoom: map.getZoom()
      });
      this.onUpdate();
    });

    if (this.props.bbox) {
      map.fitBounds(this.props.bbox);
    }

    google.maps.event.addListenerOnce(map, "idle", () => this.mapReady(map));
  }
  componentWillUnmount() {
    if (this.__gmap) {
      google.maps.event.clearListeners(this.__gmap);
      this.__gmap = null;
    }
  }
  componentWillReceiveProps(nextProps) {
    const newOpts = nextProps.mapOptions;
    const oldOpts = this.props.mapOptions;
    const map = this.__gmap;

    if (!map) {
      return;
    }

    if (nextProps.changeHash !== this.props.changeHash) {
      if (!isNil(newOpts.zoom)) {
        map.setZoom(newOpts.zoom);
      }

      if (!isNil(newOpts.center)) {
        map.panTo(this.latLng(newOpts.center.lat, newOpts.center.lng));
      }
    }

    defer(() => this.placePins());
  }
  onUpdate() {
    if (this.__gmap) {
      this.props.onMapChange();
    }
  }
  mapIcon(pin) {
    return new google.maps.MarkerImage(
      pin.s || pin.p
        ? "/static/images/map/map-marker-shadow-bright.png"
        : "/static/images/map/map-marker-shadow.png",
      null,
      null,
      null,
      new google.maps.Size(20, 28)
    );
  }
  placePins() {
    const newPins = [];
    const map = this.__gmap;
    if (!map) {
      return;
    }

    each(this.props.pins.singles, pin => {
      const pos = pin.l;
      const pinId = pin._id;
      newPins.push(pinId);

      if (!this._pinCache[pinId]) {
        this._pinCache[pinId] = new google.maps.Marker({
          position: this.latLng(pos[1], pos[0]),
          map: map,
          zIndex: 100,
          icon: this.mapIcon(pin)
        });

        this._pinCache[pinId].addListener("click", () => {
          this.props.onPinClick("single", pinId);
        });
      }
    });

    each(this.props.pins.clusters, pin => {
      const pinCount = pin.t;
      const pos = pin.c;
      const pinId = `c__${pos.toString() + pinCount}`;

      newPins.push(pinId);

      if (!this._pinCache[pinId]) {
        this._pinCache[pinId] = new MapClusterPoint(
          map,
          [pos[1], pos[0]],
          pin.t
        );

        this._pinCache[pinId].addListener("click", () => {
          this.props.onPinClick("cluster", pin.pts);
        });
      }
    });

    const removed = difference(keys(this._pinCache), newPins);

    each(removed, id => {
      if (this._pinCache[id]) {
        google.maps.event.clearListeners(this._pinCache[id]);
        this._pinCache[id].setMap(null);
        this._pinCache[id] = null;
        delete this._pinCache[id];
      }
    });
  }
  render() {
    return (
      <div className="gmap-wrapper">
        <div className="gmap-canvas" ref="mapCanvas" />
      </div>
    );
  }
}

GoogleMap.propTypes = {
  changeHash: PropTypes.string.isRequired,
  mapOptions: PropTypes.object.isRequired,
  onPinClick: PropTypes.func.isRequired,
  onMapChange: PropTypes.func.isRequired,
  bbox: PropTypes.object,
  pins: PropTypes.object.isRequired
};
