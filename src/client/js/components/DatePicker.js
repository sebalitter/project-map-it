import React, { Component } from "react";
import moment from "moment";
import ReactDatePicker from "react-datepicker";
import { map, assign, omit, isString } from "lodash";
import { DISPLAY_FORMAT } from "../utils/date";

export default class DatePicker extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: moment(),
      onFocus: () => {},
      onChange: this.onChange.bind(this)
    };
  }
  componentWillMount() {
    const attrs = omit(assign({}, this.props), ["value"]);

    if (moment.isMoment(this.props.value)) {
      attrs.selected = this.props.value;
    } else if (this.props.value) {
      attrs.selected = moment(this.props.value, DISPLAY_FORMAT);
    } else {
      attrs.selected = moment();
    }

    this.setState(attrs);
  }
  onChange(date) {
    this.setState({
      selected: date
    });
  }
  render() {
    return <ReactDatePicker {...this.state} />;
  }
}
