import React, { Component } from "react";
import { sortable } from "react-anything-sortable";

class PreviewPhoto extends Component {
  onDelete(e) {
    e.preventDefault();
    this.props.onPhotoDelete(this.props.id);
  }
  render() {
    const imgStyles = {
      backgroundImage: `url(${this.props.path})`
    };
    return (
      <div draggable={false} {...this.props}>
        <div className="inner">
          <button onClick={e => this.onDelete(e)}>
            <i className="fa fa-times" />
          </button>
          <div className="img" draggable={false} style={imgStyles} />
        </div>
      </div>
    );
  }
}

// PreviewPhoto.propTypes = {
//   onPhotoDelete: PropTypes.func.isRequired,
//   id: PropTypes.any.isRequired,
//   path: PropTypes.string.isRequired,
//
//   // Props passed by sortable.
//   sortData: PropTypes.any,
//   sortData: PropTypes.any,
//
// };

export default sortable(PreviewPhoto);
