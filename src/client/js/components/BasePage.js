import React, { Component } from "react";
import PropTypes from "prop-types";
import Menu from "../containers/Menu";
import Growler from "../containers/Growler";
import TrialAlertBar from "../containers/TrialAlertBar";

export default class BasePage extends Component {
  renderSkyMenu() {
    if (this.props.menu) {
      return (
        <div className="sub-nav">
          <Menu type={this.props.menu} />
        </div>
      );
    }
    return null;
  }
  renderSidebar() {
    if (this.props.noSidebar) {
      return null;
    }
    return (
      <div className="app-sidebar">
        <h1 className="branding">Project Map It</h1>
        <Menu type="sidebar" />
      </div>
    );
  }
  render() {
    let appClass = "app-wrapper";

    if (this.props.noSidebar) {
      appClass += " no-sidebar";
    }

    return (
      <div className={appClass}>
        {this.renderSidebar()}
        <div className="app-bd">
          <Growler />
          <TrialAlertBar />
          {this.renderSkyMenu()}
          {this.props.children}
        </div>
      </div>
    );
  }
}

BasePage.propTypes = {
  menu: PropTypes.string,
  noSidebar: PropTypes.bool
};
