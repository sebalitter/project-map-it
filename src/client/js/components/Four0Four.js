import React, { Component } from "react";
import BasePage from "../components/BasePage";

export default class Four0Four extends Component {
  render() {
    return (
      <BasePage>
        <h1>404 | I think you're lost!</h1>
        <p>The page you are looking for could not be found.</p>
      </BasePage>
    );
  }
}
