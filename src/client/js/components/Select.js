// Deprecated: Can likely be removed.
import React, { Component } from "react";
import { default as ReactSelect } from "react-select";
import { map } from "lodash";

export default class Select extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: props.value
    };
    this.onChange = this.onChange.bind(this);
  }
  onChange(val) {
    this.setState({
      value: this.props.multi ? map(val, v => v.value) : val
    });
  }
  render() {
    return (
      <ReactSelect
        {...this.props}
        {...this.state}
        backspaceToRemoveMessage=""
        inputProps={{
          autoComplete: "none",
          autoCorrect: "off",
          spellCheck: "off"
        }}
        onBlur={e => {}}
        onChange={this.onChange}
      />
    );
  }
}
