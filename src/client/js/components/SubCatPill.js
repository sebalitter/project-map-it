import React, { Component } from "react";
import ReactDOM from "react-dom";
import isDblTouchTap from "../utils/isDoubleTouchTap";
import PropTypes from "prop-types";

export default class SubCatPill extends Component {
  constructor(props) {
    super(props);

    this.onPageClick = this.onPageClick.bind(this);
    ////////////////////////////////////////////////////////////////////////////
    // I am aware this is bad, but i think it's appropriate. Fuck ya'll.
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    this.state = {
      isEditing: false
    };
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
  }
  componentDidMount() {
    window.addEventListener("mousedown", this.onPageClick, false);
  }
  componentWillUnmount() {
    window.removeEventListener("mousedown", this.onPageClick);
  }
  onPageClick(e) {
    if (this.state.isEditing && this.refs._input) {
      const el = ReactDOM.findDOMNode(this.refs._input);
      if (!el.contains(e.target)) {
        this.keepRenamedText();
      }
    }
  }
  onDoubleClick(event) {
    event.preventDefault();
    if (this.props._id) {
      this.setState({
        isEditing: true
      });
    }
  }
  onSingleClick() {
    event.preventDefault();
    this.props.onSingleClick(this.props._id);
  }
  onRemove(event) {
    event.preventDefault();
    if (this.props._id) {
      this.props.onRemove(this.props.uid, this.props._id);
    }
  }
  onNameKeyPress(e) {
    if (this.state.isEditing && e.charCode === 13) {
      return this.keepRenamedText();
    }
    if (this.state.isEditing && e.charCode === 27) {
      return this.trashRenamedText();
    }
  }
  keepRenamedText() {
    const newName = this.refs._input.value || "Untitled";

    setTimeout(() => {
      this.setState(
        {
          isEditing: false
        },
        () => this.props.onRename(this.props.uid, this.props._id, newName)
      );
    }, 1);
  }
  trashRenamedText() {
    this.setState({
      isEditing: false
    });
  }
  renderNameInput() {
    if (this.state.isEditing) {
      setTimeout(() => {
        const el = ReactDOM.findDOMNode(this.refs._input);
        if (el) {
          el.focus();
        }
      }, 50);

      return (
        <input
          ref="_input"
          type="text"
          defaultValue={this.props.name}
          onKeyPress={e => this.onNameKeyPress(e)}
        />
      );
    }
    return <span>{this.props.name}</span>;
  }
  render() {
    const { name, _id } = this.props;
    let className = _id ? "catpill saved" : "catpill unsaved";
    if (this.props.isActive) {
      className += " active";
    }
    return (
      <button
        className={className}
        onClick={e => this.onSingleClick()}
        onTouchTap={e => {
          if (isDblTouchTap(e)) {
            this.onDoubleClick(e);
          }
        }}
        onDoubleClick={e => this.onDoubleClick(e)}
      >
        {this.renderNameInput()}
        <i className="fa fa-times" onClick={e => this.onRemove(e)} />
      </button>
    );
  }
}

SubCatPill.propTypes = {
  name: PropTypes.string,
  uid: PropTypes.string.isRequired,
  _id: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
  parent: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
  isActive: PropTypes.bool.isRequired,
  disabled: PropTypes.bool.isRequired,
  onSingleClick: PropTypes.func.isRequired,
  onRemove: PropTypes.func.isRequired,
  onRename: PropTypes.func.isRequired
};
