import React, { Component, createElement } from "react";
import { omit } from "lodash";
import PropTypes from "prop-types";

/**
 * This can be used in a few ways.
 * @extends Component
 * @example
 * return <Loader Component={TheComponent} isLoading={true} {...otherProps} />
 *  - or -
 * return <Loadder isLoading={true}>Loaded children...</Loader>
 */
function Loader(props) {
  const loaderClass = props.center ? "loader center" : "loader";

  // Need to pass a explicit key to both or else IE doesn't update props well.
  if (props.isLoading) {
    return <div key="loader" className={loaderClass} />;
  }

  if (props.Component) {
    return createElement(
      props.Component,
      Object.assign(omit(props, ["isLoading"]), {
        key: "loaded-content"
      })
    );
  } else {
    return props.children;
  }
}

Loader.propTypes = {
  Component: PropTypes.func,
  isLoading: PropTypes.bool.isRequired
};

export default Loader;
