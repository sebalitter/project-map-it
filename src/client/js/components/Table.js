import React, { Component } from "react";
import { ReactDOM } from "react-dom";
import PropTypes from "prop-types";

export default class Table extends Component {
  header() {
    return (
      <tr>
        {this.props.cols.map(function(colData, idx) {
          return <th key={colData.key + idx}>{colData.label}</th>;
        })}
      </tr>
    );
  }
  rows() {
    const { cols, rows } = this.props;
    let colCount = 0;

    return rows.map(function(item, rowCount) {
      const cells = cols.map(function(colData, idx) {
        const key = colData.key;
        const val = item[key];

        if (val === null || val === undefined) {
          return (
            <td key={key + colCount++}>
              <span />
            </td>
          );
        }
        return <td key={key + colCount++}>{val}</td>;
      });

      return <tr key={`row_${rowCount}`}>{cells}</tr>;
    });
  }
  renderRows() {
    const rows = this.rows();
    if (rows.length) {
      return rows;
    }
    return (
      <tr>
        <td colSpan={rows.length < 1 ? 1 : rows.length}>
          {this.props.noResultsText || "No Results"}
        </td>
      </tr>
    );
  }
  render() {
    return (
      <table className="generic-tbl">
        <thead>{this.header()}</thead>
        <tbody>{this.renderRows()}</tbody>
      </table>
    );
  }
}

Table.propTypes = {
  noResultsText: PropTypes.string,

  rows: PropTypes.array.isRequired,

  cols: PropTypes.arrayOf(
    PropTypes.shape({
      key: PropTypes.string.isRequired,
      label: PropTypes.string.isRequired
    })
  ).isRequired
};
