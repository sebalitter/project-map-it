import React, { Component } from "react";
import { GOOGLE_API_KEY } from "../modules/common/constants";
import PropTypes from "prop-types";

export default class StaticGoogleMap extends Component {
  constructor(props) {
    super(props);
  }
  url(zoom) {
    let base = `https://maps.googleapis.com/maps/api/staticmap?`;
    base += `key=${GOOGLE_API_KEY}`;
    base += `&center=${this.props.latitude},${this.props.longitude}`;
    base += `&size=1000x90&zoom=${zoom}&scale=2`;
    base += `&markers=size:tiny%7Ccolor:black|${this.props.latitude},${
      this.props.longitude
    }`;
    return base;
  }
  render() {
    return (
      <div className="static-map-wrapper">
        <img className="out" src={this.url(6)} />
        <img className="in" src={this.url(14)} />
      </div>
    );
  }
}

StaticGoogleMap.propTypes = {
  latitude: PropTypes.number.isRequired,
  longitude: PropTypes.number.isRequired
};
