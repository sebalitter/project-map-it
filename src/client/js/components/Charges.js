import React, { Component } from "react";
import PropTypes from "prop-types";
import moment from "moment";
import { formatMoney } from "accounting";
import { Link } from "react-router";
import { map } from "lodash";
import Loader from "./Loader";
import Table from "./Table";

/**
 * Simple abstraction for either a <button> or <Link>.
 */
export default class Charges extends Component {
  cols() {
    return [
      {
        key: "status",
        label: "Past Charges"
      },
      {
        key: "created",
        label: " "
      },
      {
        key: "amount",
        label: " "
      }
    ];
  }
  rows() {
    return map(this.props.charges, charge => {
      return {
        status: charge.paid ? "Paid" : "Unpaid",
        created: moment.unix(charge.created).format("MMMM Do, YYYY"),
        amount: formatMoney(charge.amount / 100)
      };
    });
  }
  render() {
    const { isLoading } = this.props;
    return (
      <Loader isLoading={isLoading}>
        <Table
          rows={this.rows()}
          cols={this.cols()}
          noResultsText="You have no past charges."
        />
      </Loader>
    );
  }
}

Charges.propTypes = {
  isLoading: PropTypes.bool.isRequired,

  // This is based off the stripe Charge object.
  // see: https://stripe.com/docs/api#charge_object
  charges: PropTypes.arrayOf(
    PropTypes.shape({
      paid: PropTypes.bool.isRequired,
      amount: PropTypes.number.isRequired,
      refunded: PropTypes.bool.isRequired,
      created: PropTypes.number.isRequired
    })
  )
};
