import React, { Component } from "react";
import Tooltip from "rc-tooltip";
import Geosuggest from "react-geosuggest";
import PropTypes from "prop-types";
import {
  get,
  filter,
  each,
  flatten,
  sortBy,
  map,
  chunk,
  find,
  isEmpty
} from "lodash";

import CopyToClipboard from "react-copy-to-clipboard";

export default class MapFilter extends Component {
  constructor(props) {
    super(props);

    this._shareTooltipText = "Click to copy to clipboard.";
    this.__shareTooltipText = this._shareTooltipText;
  }
  isChecked(type, value = null) {
    return !isEmpty(find(this.props.activeFilters, { type, value }));
  }
  filterCategoryList() {
    const filterList = [];
    const cats = sortBy(this.props.categories, ["name"]);
    const onlyParents = cat => !cat.parent;
    const populateParents = motherCat => {
      filterList.push({
        parent: motherCat,
        children: filter(cats, _cat => motherCat._id === _cat.parent)
      });
    };
    each(filter(cats, onlyParents), populateParents);
    return filterList;
  }
  renderCategoryCheckbox(cat) {
    const onChange = e => {
      const currentlyChecked = this.isChecked("category", cat._id);
      this.props.onFilterChange("category", cat._id, currentlyChecked);

      if (!cat.parent) {
        each(this.props.categories, _c => {
          if (_c.parent === cat._id) {
            this.props.onFilterChange("category", _c._id, currentlyChecked);
          }
        });
      }
    };
    return (
      <span>
        <input
          onChange={onChange}
          checked={this.isChecked("category", cat._id)}
          type="checkbox"
          name={cat._id}
          id={cat._id}
        />
        <label htmlFor={cat._id}>{cat.name}</label>
      </span>
    );
  }
  renderFilterOptions(list) {
    return map(list, (topLevel, idx) => {
      const { parent, children } = topLevel;
      return (
        <div key={parent._id} className="top-level-category-list">
          <h5>{this.renderCategoryCheckbox(parent)}</h5>

          <ul className="child-categories">
            {map(children, child => {
              return (
                <li key={child._id}>{this.renderCategoryCheckbox(child)}</li>
              );
            })}
          </ul>
        </div>
      );
    });
  }
  renderFilters() {
    if (!this.props.filtersOpened) {
      return null;
    }
    const allFilters = this.filterCategoryList();
    const lists = chunk(allFilters, Math.ceil(allFilters.length / 2));

    return (
      <div className="filters-area" onClick={e => e.stopPropagation()}>
        <div className="col">{this.renderFilterOptions(lists[0])}</div>
        <div className="col">{this.renderFilterOptions(lists[1])}</div>
      </div>
    );
  }
  locateMeClick(e) {
    e.preventDefault();
    this.props.onLocateMeClick();
  }
  renderLocateButton() {
    if (!navigator.geolocation) {
      return null;
    }
    if (this.props.geolocating) {
      return <div className="loader geolocating" />;
    }
    return (
      <button
        onClick={e => this.locateMeClick(e)}
        className="location-me"
        data-intro="Hello step one!"
        type="button"
      >
        <i className="fa fa-location-arrow" />
      </button>
    );
  }
  _onCopyToClipboard() {
    this._shareTooltipText = "Copied!";
    this.forceUpdate();
    setTimeout(() => {
      this._shareTooltipText = this.__shareTooltipText;
      this.forceUpdate();
    }, 2500);
  }
  renderShare() {
    return (
      <div className="share-link">
        <CopyToClipboard
          text={this.props.shareLink}
          onCopy={() => this._onCopyToClipboard()}
        >
          <Tooltip
            animation="zoom"
            placement="left"
            overlay={<span>{this._shareTooltipText}</span>}
          >
            <span>
              <i className="fa fa-share-alt-square" />&nbsp;Share Map
            </span>
          </Tooltip>
        </CopyToClipboard>
      </div>
    );
  }
  renderTotal() {
    const total = this.props.total;
    const noun = total === 1 ? "project" : "projects";
    const org = this.props.organizationName;

    return (
      <span className="total">
        Displaying <b>{total}</b> {noun} for <b>{org}</b>.
      </span>
    );
  }
  getProjects() {
    const clusters = get(this, "props.projects.clusters", []);
    const singles = get(this, "props.projects.singles", []);

    if (clusters.length) {
      return flatten(map(clusters, cluster => cluster.pts));
    }
    return singles;
  }
  renderProjectListItem(project) {
    let msg = "";
    const _onClick = this.props.onProjectClick;

    if (project.p) {
      msg = '<i title="Has Photos" class="fa fa-camera-retro"></i>';
    } else {
      msg = '<i title="No photos yet." class="fa fa-camera-retro greyed"></i>';
    }

    if (project.s) {
      msg += '<i title="Has Review" class="fa fa-list-alt"></i>';
    } else {
      msg += '<i title="No review yet." class="fa fa-list-alt greyed"></i>';
    }

    function onClick() {
      _onClick(project._id);
    }

    return (
      <tr key={`list-item_${project._id}`} onClick={onClick}>
        <td className="street">
          <span>{project.a.street}</span>
        </td>
        <td className="state">
          <span>{project.a.state}</span>
        </td>
        <td className="icons">
          <span dangerouslySetInnerHTML={{ __html: msg }} />
        </td>
      </tr>
    );
  }
  renderProjectTable() {
    const projects = this.getProjects();
    if (!projects.length) {
      return (
        <p className="no-projects">
          There are currently no projects to display.
        </p>
      );
    }
    return (
      <table>
        <thead>
          <tr>
            <th className="street">Street</th>
            <th>State</th>
            <th>-</th>
          </tr>
        </thead>
        <tbody>{projects.map(this.renderProjectListItem.bind(this))}</tbody>
      </table>
    );
  }
  render() {
    const filterClass = this.props.filtersOpened
      ? "map-filter filters-on"
      : "map-filter";

    const withPhotos = this.props.hasPhotos;
    const withReviews = this.props.hasReviews;

    return (
      <div className="map-filter-wrap">
        <div className="list-view">{this.renderProjectTable()}</div>
        <div className="misc-filters">
          {this.renderTotal()}
          {this.renderShare()}
          <span>
            <input
              type="checkbox"
              checked={this.isChecked("photo")}
              onChange={() =>
                this.props.onFilterChange(
                  "photo",
                  null,
                  this.isChecked("photo")
                )
              }
              name="_photosonly"
              id="_photosonly"
            />&nbsp;
            <label htmlFor="_photosonly">
              Projects w/ Photos ({withPhotos})
            </label>
          </span>
          <span>
            <input
              type="checkbox"
              checked={this.isChecked("survey")}
              onChange={() =>
                this.props.onFilterChange(
                  "survey",
                  null,
                  this.isChecked("survey")
                )
              }
              name="_surveysonly"
              id="_surveysonly"
            />&nbsp;
            <label htmlFor="_surveysonly">
              Projects w/ Reviews ({withReviews})
            </label>
          </span>
        </div>
        <div className={filterClass}>
          <div className="top">
            <div className="search-area">
              <Geosuggest
                onSuggestSelect={this.props.onPlaceSelect}
                placeholder="Search for a location."
              />
              {this.renderLocateButton()}
            </div>
            <div className="filter-btn-area">
              <button type="button" onClick={this.props.onFilterClick}>
                <i className="fa fa-filter" />
              </button>
            </div>
          </div>
          {this.renderFilters()}
        </div>
      </div>
    );
  }
}

MapFilter.propTypes = {
  organizationName: PropTypes.string.isRequired,
  shareLink: PropTypes.string.isRequired,
  total: PropTypes.number.isRequired,
  hasPhotos: PropTypes.number.isRequired,
  hasReviews: PropTypes.number.isRequired,
  projects: PropTypes.object.isRequired,
  categories: PropTypes.array.isRequired,
  geolocating: PropTypes.bool.isRequired,
  activeFilters: PropTypes.array.isRequired,
  filtersOpened: PropTypes.bool.isRequired,
  onPlaceSelect: PropTypes.func.isRequired,
  onFilterChange: PropTypes.func.isRequired,
  onFilterClick: PropTypes.func.isRequired,
  onProjectClick: PropTypes.func.isRequired,
  onLocateMeClick: PropTypes.func.isRequired
};
