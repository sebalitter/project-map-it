import React, { Component } from "react";
import PropTypes from "prop-types";
import { Link } from "react-router";
import { omit, uniq } from "lodash";

/**
 * Simple abstraction for either a <button> or <Link>.
 */
export default class Button extends Component {
  render() {
    const btnProps = omit(this.props, ["type", "label", "onChange"]);

    btnProps.className = btnProps.className ? btnProps.className : "";
    btnProps.className = btnProps.className.split(" ");
    btnProps.className.push("btn");
    btnProps.className = uniq(btnProps.className);
    btnProps.className = btnProps.className.join(" ");

    const label = {
      __html: this.props.label
    };

    if (this.props.type === "link") {
      return <Link {...btnProps} dangerouslySetInnerHTML={label} />;
    } else if (this.props.type === "file") {
      return (
        <label {...btnProps}>
          <span dangerouslySetInnerHTML={label} />
          <input type="file" onChange={e => this.props.onChange(e)} />
        </label>
      );
    } else if (this.props.type === "external") {
      return <a {...btnProps} dangerouslySetInnerHTML={label} />;
    } else {
      btnProps.type = this.props.type;
      return <button {...btnProps} dangerouslySetInnerHTML={label} />;
    }
  }
}

Button.propTypes = {
  type: PropTypes.oneOf(["file", "link", "button", "submit", "external"])
    .isRequired,
  label: PropTypes.string.isRequired
};
