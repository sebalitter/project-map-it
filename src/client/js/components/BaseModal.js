import React, { Component } from "react";
import PropTypes from "prop-types";
import { defer, debounce } from "lodash";
import Loader from "./Loader";

export default class BaseModal extends Component {
  constructor(props) {
    super(props);
    this.forcedUpdate = false;
  }
  componentDidMount() {
    window.onresize = debounce(() => this.forceUpdate(), 100);
    this.forceUpdate();
  }
  componentWillUnmount() {
    window.onresize = null;
  }
  componentDidUpdate() {
    if (this.forcedUpdate) {
      this.forcedUpdate = false;
    } else {
      this.forceUpdate();
      this.forcedUpdate = true;
    }
  }
  exit(e) {
    e.preventDefault();
    this.props.onExit();
  }
  keepOpen() {
    return this.props.notExitable || this.props.isLoading;
  }
  closeBtn() {
    if (!this.keepOpen()) {
      return (
        <button className="close" onClick={e => this.exit(e)}>
          <i className="fa fa-times-circle" />
        </button>
      );
    }
    return null;
  }
  render() {
    let wrapClass = "modal-outer";

    if (this.props.className) {
      wrapClass += " " + this.props.className;
    }

    const onClick = e => {
      e.preventDefault();
      e.stopPropagation();
      e.nativeEvent.stopImmediatePropagation();
      if (!this.keepOpen()) {
        this.exit(e);
      }
    };
    const noPropagation = e => {
      e.stopPropagation();
      e.nativeEvent.stopImmediatePropagation();
    };
    const styles = {};

    if (this.props.height && this.props.width) {
      styles.width = this.props.width;
      styles.minHeight = this.props.height;
    }

    if (this.props.display) {
      return (
        <div className={wrapClass} onClick={onClick}>
          <div
            ref="innerBox"
            style={styles}
            className={"modal-inner"}
            onClick={noPropagation}
          >
            <div className="modal-box" onClick={noPropagation}>
              {this.closeBtn()}
              <Loader isLoading={this.props.isLoading || false}>
                {this.props.children}
              </Loader>
            </div>
          </div>
        </div>
      );
    }
    return null;
  }
}

BaseModal.propTypes = {
  display: PropTypes.bool.isRequired,
  fromTop: PropTypes.bool,
  notExitable: PropTypes.bool,
  width: PropTypes.string,
  height: PropTypes.string,
  isLoading: PropTypes.bool,
  onExit: PropTypes.func.isRequired
};
