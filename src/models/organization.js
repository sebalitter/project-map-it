const config = require("config");
const mongoose = require("mongoose");
const security = require("../helpers/security");
const logger = require("../helpers/logger");
const moment = require("moment");
const stripe = require("stripe")(config.stripeAPISecreKey);
const { get } = require("lodash");

const Schema = mongoose.Schema;

const BASIC_PLAN = "basic";
const PRO_PLAN = "pro";
const ENTERPRISE_PLAN = "enterprise";

const schema = new Schema({
  name: {
    type: String,
    required: "Organization name is required."
  },
  slug: String,
  stripeId: String,
  promo: String,
  usedPromos: Array,
  social_google: String,
  social_facebook: String,
  social_yelp: String,
  social_houzz: String,
  social_bbb: String,
  social_angies_list: String,
  social_home_advisor: String,
  gafbucks: {
    type: Boolean,
    default: false
  },
  plan: {
    type: String,
    enum: [BASIC_PLAN, PRO_PLAN, ENTERPRISE_PLAN],
    default: BASIC_PLAN,
    required: "A plan name is required."
  },
  active: {
    type: Boolean,
    default: true
  },
  trialing: {
    type: Boolean,
    default: true
  },
  timeLeftOnTrial: String,
  created: {
    type: Date,
    default: Date.now
  }
});

schema.index({
  slug: 1
});

schema.index({
  stripeId: 1
});

schema.set("toJSON", {
  virtuals: true
});

schema.virtual("pinsAllowed").get(function() {
  if (this.trialing) {
    return config.plans[ENTERPRISE_PLAN];
  }
  return config.plans[this.plan];
});

schema.pre("save", async function(next) {
  if (this.isNew) {
    try {
      const slug = await this.generateSlug();
      this.set("slug", slug);
    } catch (err) {
      logger.error("There was an error when saving new organization.");
      logger.error(err);
      return next(err);
    }
  }
  next();
});

/**
 * Generate a unique slug for the organization.
 *
 * This is for unsaved records.
 *
 * @return {promise}
 */
schema.methods.generateSlug = async function() {
  return new Promise((resolve, reject) => {
    const model = this.model("Organization");
    const self = this;
    let attempts = 0;

    function _attempt(candidate) {
      model
        .findOne(
          {
            slug: candidate
          },
          "_id"
        )
        .lean()
        .exec(async (err, doc) => {
          if (err) {
            return reject(err);
          }

          if (doc) {
            return _attempt(`${candidate}-${++attempts}`);
          }

          resolve(candidate);
        });
    }

    _attempt(security.slugify(this.name));
  });
};

/**
 * Create the first immutable question for the account.
 *
 * @async
 * @return {object} Question model
 */
schema.methods.createPresetQuestions = async function createPresetQuestions() {
  const Question = this.model("Question");
  let order = -1;

  await Question.create([
    {
      organization: this._id,
      immutable: true,
      kind: "star",
      text: "How was your overall experience?",
      pub: true,
      order: order++
    },
    {
      organization: this._id,
      immutable: false,
      kind: "star",
      text: "How would you rate the overall craftsmanship of the job?",
      pub: true,
      order: order++
    },
    {
      organization: this._id,
      immutable: false,
      kind: "star",
      text: "How would you rate the overall communication?",
      pub: true,
      order: order++
    },
    {
      organization: this._id,
      immutable: false,
      kind: "boolean",
      text: "Was the job site kept clean and safe?",
      pub: true,
      order: order++
    },
    {
      organization: this._id,
      immutable: false,
      kind: "boolean",
      text: `Would you recommend ${this.name} to a friend?`,
      pub: true,
      order: order++
    },
    {
      organization: this._id,
      immutable: false,
      kind: "boolean",
      text: "I felt I received good value for my money.",
      pub: true,
      order: order++
    },
    {
      organization: this._id,
      immutable: false,
      kind: "text",
      text: `Describe your experience working with ${this.name}.`,
      pub: true,
      order: order++
    }
  ]);
};

/**
 * Register organization with stripe. This is only used when a user is
 * registering for the first time.
 *
 * @async
 * @param {object} user
 * @return {void}
 */
schema.methods.initStripeWithUser = async function initStripeWithUser(user) {
  const matched = user.name.match(/^(\S+)\s(.*)/);
  const nameSplit = matched ? matched.slice(1) : [user.name, ""];

  const customer = await stripe.customers.create({
    email: user.email,
    description: `Customer: ${user.name} -- Organization: ${this.name}`,
    metadata: {
      full_name: user.name,
      first_name: nameSplit[0],
      last_name: nameSplit[1],
      promo: user.promo || null,
      cell: user.cell || null,
      gafbucks: user.gafbucks || null
    }
  });

  const subscription = await stripe.subscriptions.create({
    customer: customer.id,
    plan: BASIC_PLAN,
    trial_end: moment()
      .add(1, "months")
      .format("X")
  });

  this.trialing = true;
  this.stripeId = customer.id;
  this.plan = BASIC_PLAN;
  await this.save();
  await this.verifyOrganization();
  return customer;
};

/**
 * Update a organization's existing subscription.
 *
 * @param {string} plan
 * @return {promise}
 */
schema.methods.updateSubscription = async function updateSubscription(
  plan,
  card
) {
  if (Object.keys(config.plans).indexOf(plan) === -1) {
    throw new Error("Plan not valid: " + plan);
  }

  const now = moment().unix();
  const customer = await stripe.customers.retrieve(this.stripeId);
  const subscription = get(customer, "subscriptions.data[0]");

  // If the subscription we're updating is already canceled, then create a new
  // one. Otherwise, just update their existing one and stripe will handle the
  // rest. Since we're updating, we never need to set it to be a trial period.
  if (
    !subscription ||
    subscription.status === "canceled" ||
    subscription.status === "unpaid"
  ) {
    logger.info(`${this.name}'s subscription is invalid. Creating a new one.`);
    const newSubscription = await stripe.subscriptions.create({
      customer: this.stripeId,
      plan: plan,
      source: Object.assign({ object: "card" }, card)
    });
    await this.verifyOrganization();
  } else {
    logger.info(
      `${this.name} has a ${subscription.status} subscription. Updating.`
    );

    // Make sure the trialing period persists if they're still trialing.
    let trialEnd = "now";

    if (subscription.trial_end > now) {
      trialEnd = subscription.trial_end;
    }

    await stripe.subscriptions.update(subscription.id, {
      plan,
      trial_end: trialEnd,
      source: Object.assign({ object: "card" }, card)
    });
    await this.verifyOrganization();
  }
};

/**
 * Apply a coupon to the organization's customer.
 *
 * @param {string} name ID/Name of coupon to apply
 * @return {promise}
 */
schema.methods.applyCoupon = async function applyCoupon(name) {
  if ((this.usedPromos || []).indexOf(name) !== -1) {
    throw new Error("You have already used this promo.");
  }

  await this.model("Organization").findByIdAndUpdate(this._id, {
    $push: {
      usedPromos: name
    }
  });

  await stripe.customers.update(this.stripeId, {
    coupon: name
  });
};

/**
 * Get information about a user.
 *
 * @param {promise}
 */
schema.methods.accountDetails = async function accountDetails() {
  const customer = await stripe.customers.retrieve(this.stripeId);
  const subscription = get(customer, "subscriptions.data[0]");

  customer.charges = await stripe.charges.list({
    customer: this.stripeId,
    limit: 24
  });

  customer.subscription = subscription;

  return customer;
};

/**
 * Cancel the subscription for the user and then update their organization
 * appropriately.
 *
 * @return {promise}
 */
schema.methods.cancelSubscription = async function cancelSubscription() {
  const customer = await stripe.customers.retrieve(this.stripeId);
  const subscription = get(customer, "subscriptions.data[0]");

  if (!subscription) {
    throw new Error(
      `Could not find subscription for organization: ${this.name}`
    );
  }

  await stripe.subscriptions.del(subscription.id);
  await this.verifyOrganization();
};

/**
 * Update the active property on an organization based on the subscription
 * status. This is responsible for keeping the organization in sync with the
 * corresponding stripe account.
 *
 * @return {promise}
 */
schema.methods.verifyOrganization = async function verifyOrganization() {
  if (!this.stripeId) {
    throw new Error(`${this.name} does not yet have stripe id associated.`);
  }

  const customer = await stripe.customers.retrieve(this.stripeId);
  const subscription = get(customer, "subscriptions.data[0]");

  this.set("timeLeftOnTrial", getTimeLeftOnTrial(subscription));

  // If the customer doesn't have a subscription at all, it must mean they
  // canceled or it was removed using the admin.
  if (!subscription) {
    logger.info(
      `Setting ${this.name} to unactive because canceled subscription.`
    );
    this.set("trialing", false);
    this.set("active", false);
    this.setProjectActiveState();
    await this.save();
  } else {
    switch (subscription.status) {
      case "active":
      case "trialing":
        logger.info(
          `Setting ${this.name} to be active on the ${
            subscription.plan.name
          } plan.`
        );
        this.set("active", true);
        this.set("plan", subscription.plan.id);
        this.set("trialing", subscription.status === "trialing");
        await this.save();
        break;
      default:
        logger.info(
          `Setting ${this.name} to not be active since the status is ${
            subscription.status
          }.`
        );
        this.set("active", false);
        this.set("plan", subscription.plan.id);
        this.set("trialing", false);
        await this.save();
    }
  }

  // Update all projects in organization.
  this.setProjectActiveState();
};

/**
 * Update the active state of the projects within an organization given the
 * current plan.
 *
 * If the plan is trialing, always allow for the enterprise plan.
 *
 * @return {promise}
 */
schema.methods.setProjectActiveState = async function projectActiveState() {
  const Project = this.model("Project");
  const projects = await Project.find(
    {
      organization: this._id
    },
    "active"
  );

  if (!projects) {
    return;
  }

  const allowedAmount = this.trialing
    ? config.plans[ENTERPRISE_PLAN]
    : config.plans[this.plan];

  for (let i = 0; i < projects.length; i++) {
    await Project.findOneAndUpdate(
      {
        _id: projects[i]._id
      },
      {
        $set: {
          active: i < allowedAmount
        }
      }
    );
  }
};

/**
 * Update all organization. This should be run every once in a while.
 *
 * @return {promise}
 */
schema.statics.verifyAllOrganizations = async function verifyAllOrganizations() {
  const all = await this.model("Organization").find({});

  for (let i = 0; i < all.length; i++) {
    try {
      await all[i].verifyOrganization();
    } catch (err) {
      logger.error(
        "There was an error when verifying organization in loop:",
        err.message
      );
    }
  }
};

/**
 * Get the remaining time left on a subscription trial.
 *
 * @param {object} subscription
 * @return {string}
 */
function getTimeLeftOnTrial(subscription) {
  let timeLeftOnTrial = "expired";

  if (
    subscription &&
    subscription.status === "trialing" &&
    subscription.trial_end
  ) {
    const now = moment().unix();
    const end = moment.unix(subscription.trial_end);
    const _days = end.diff(moment(), "days");
    const days = _days < 1 ? 0 : _days;

    if (days && subscription.trial_end > now) {
      const noun = days > 1 ? "days" : "day";
      timeLeftOnTrial = `${days} ${noun}`;
    } else if (subscription.trial_end > now) {
      const hours = end.diff(moment(), "hours");
      const noun = hours > 1 ? "hours" : "hour";
      timeLeftOnTrial = `${hours} ${noun}`;
    }
  }

  return timeLeftOnTrial;
}

module.exports = mongoose.model("Organization", schema);
