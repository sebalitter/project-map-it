const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
  organization: {
    type: Schema.Types.ObjectId,
    required: 'Organization required',
    ref: 'Organization'
  },
  parent: {
    type: Schema.Types.ObjectId,
    ref: 'ProjectCategory'
  },
  name: {
    type: String,
    required: 'Name required',
    maxLength: 60
  },
  created: {
    type: Date,
    default: Date.now
  }
});

schema.virtuals.fullname = function() {
  return (this.parent && this.parent.name)
    ? `${this.parent.name} -> ${this.name}`
    : this.name;
};

module.exports = mongoose.model('ProjectCategory', schema);
