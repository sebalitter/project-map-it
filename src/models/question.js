const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const { indexOf, pick } = require("lodash");

const schema = new Schema({
  organization: {
    type: Schema.Types.ObjectId,
    required: "Organization required",
    ref: "Organization"
  },
  kind: {
    type: String,
    required: "Kind required",
    validate: {
      validator: function(v) {
        return indexOf(["text", "boolean", "star"], v) >= 0;
      },
      message: 'Invalid kind {VALUE}: valids are "text", "boolean", "star"'
    }
  },
  text: {
    type: String,
    required: "Text required"
  },
  pub: {
    type: Boolean
  },
  immutable: {
    type: Boolean,
    default: false
  },
  order: {
    type: Number
  },
  created: {
    type: Date,
    default: Date.now
  }
});

/**
 * Not the best, but better. Let's just remove the old questions and add the
 * new. There isn't a need to do a comparison.
 */
schema.statics.apiUpdate = async function updateQuestions(
  orgId,
  questions,
  complete
) {
  const Question = this.model("Question");
  const prev = await Question.find({ organization: orgId });
  const newDocs = [];

  // These are the sanitized props. All others will be stripped to prevent any
  // weird issues - like the _id or __v being saved.
  const props = ["kind", "text", "pub", "order", "immutable"];
  questions = questions.map(q => pick(q, props));

  // Validate all new records first.
  for (let i = 0; i < questions.length; i++) {
    questions[i].organization = orgId;
    newDocs.push(new Model(questions[i]));
    await newDocs[newDocs.length - 1].validate();
  }

  // Then save.
  for (let i = 0; i < newDocs.length; i++) {
    await newDocs[i].save();
  }

  // Remove all the previous questions.
  await Question.remove({ _id: { $in: prev.map(q => q._id) } });

  return newDocs;
};

const Model = mongoose.model("Question", schema);

module.exports = Model;
