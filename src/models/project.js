const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const config = require("config");
const _ = require("lodash");
const emailHelper = require("../helpers/email");
const logger = require("../helpers/logger");
const supercluster = require("supercluster");

const defaultRoles = config.roles;

const emailSubSchema = new Schema({
  email: {
    type: String
  }
});

emailSubSchema.path("email").validate(email => {
  var emailRegex = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  return emailRegex.test(email);
}, "Please make sure the customer email is formatted correctly.");

const phoneSubSchema = new Schema({
  phone: String
});

const surveySubSchema = new Schema({
  immutable: Boolean,
  question: String,
  kind: String,
  answer: String,
  pub: Boolean
});

const schema = new Schema(
  {
    organization: {
      type: Schema.Types.ObjectId,
      required: "Organization required",
      ref: "Organization"
    },
    categories: [
      {
        type: Schema.Types.ObjectId,
        ref: "ProjectCategory"
      }
    ],
    name: {
      type: String,
      default: "Untitled"
    },
    state: {
      type: String,
      required: true
    },
    city: {
      type: String,
      required: true
    },
    zip: {
      type: String
    },
    street: {
      type: String,
      required: true
    },
    location: {
      type: [Number]
    },
    photoCount: {
      type: Number,
      default: 0
    },
    active: {
      type: Boolean,
      default: true
    },
    surveyPhoneNumbers: [phoneSubSchema],
    surveyEmails: [emailSubSchema],
    surveySent: {
      type: Date
    },
    surveyCompleted: {
      type: Date
    },
    survey: [surveySubSchema],
    created: {
      type: Date,
      default: Date.now
    }
  },
  {
    emitIndexErrors: process.env.NODE_ENV !== "production"
  }
);

// Used on a map pin query with no filters.
schema.index({
  location: "2d",
  organization: 1,
  active: 1
});

// Used to search.
schema.index({
  street: "text",
  name: "text",
  state: "text",
  zip: "text",
  city: "text"
});

schema
  .virtual("commaDelimitedEmails")
  .set(function(emails) {
    if (_.trim(emails).length > 0) {
      this.surveyEmails = _.map(emails.split(","), function(v) {
        return {
          email: _.trim(v)
        };
      });
    } else {
      this.surveyEmails = [];
    }
  })
  .get(function() {
    return (this.surveyEmails || []).map(e => e.email).join(", ");
  });

schema
  .virtual("commaDelimitedPhoneNumbers")
  .set(function(phones) {
    if (_.trim(phones).length > 0) {
      this.surveyPhoneNumbers = _.map(phones.split(","), function(v) {
        return {
          phone: _.trim(v)
            .replace(/[^\d]/g, "")
            .replace(/(\d{3})(\d{3})(\d{4})/, "$1-$2-$3")
        };
      });
    } else {
      this.surveyPhoneNumbers = [];
    }
  })
  .get(function() {
    return (this.surveyPhoneNumbers || []).map(e => e.phone).join(", ");
  });

schema.set("toJSON", {
  virtuals: true
});

// Check for obvious dups.
schema.pre("save", function(next) {
  if (!this.isNew) {
    return process.nextTick(() => next());
  }

  if (!this.organization || !this.state || !this.city || !this.street) {
    return next();
  }

  this.model("Project")
    .where({
      organization: this.organization,
      state: this.state,
      street: this.street,
      city: this.city
    })
    .count((err, count) => {
      if (count) {
        this.invalidate("street", "This looks like a duplicate entry");
        return next(new Error("This looks like a duplicate entry"));
      }
      next(err);
    });
});

// Check to make sure the owner's plan is enough.
schema.pre("save", async function(next) {
  const orgId = _.get(this, "organization._id", this.organization);

  if (!orgId) {
    return next();
  }

  const org = await this.model("Organization").findById(orgId);

  if (!org) {
    return next(new Error("Could not find associated organization."));
  }

  // If it is already saved, no further checks needed.
  if (!this.isNew) {
    return next();
  }

  // Make sure user is capable of adding more projects given their
  // organization's current plan.
  const count = await this.model("Project").count({
    organization: org._id
  });

  const notAllowed = new Error(
    "Please upgrade your plan to add more projects."
  );
  notAllowed.code = 1201988;

  if (count >= org.pinsAllowed) {
    return next(notAllowed);
  } else if (!org.active) {
    return next(notAllowed);
  }

  next();
});

// Inject photos into projects.
schema.pre("init", function(fn, doc) {
  this.model("File")
    .find(
      {
        reference: doc._id
      },
      null,
      {
        sort: {
          order: 1
        }
      }
    )
    .exec((err, files) => {
      doc.photos = files ? files : [];
      fn();
    });
});

schema.pre("remove", async function(next) {
  try {
    const relatedFiles = await this.model("File").find({ reference: this._id });

    for (let i = 0, len = relatedFiles.length; i < len; i++) {
      await relatedFiles[i].remove();
    }
  } catch (err) {
    logger.error(
      "Error occurred when removing related files to project: " + err.message
    );
  }

  next();
});

schema.post("save", function(doc) {
  let search = `${doc.street},${doc.city},${doc.state}`;

  if (doc.zip) {
    search += `,${doc.zip}`;
  }

  // Required here to avoid a circular dependency because geo requires Project.
  require("../helpers/geo")
    .queue.create("geolocate", {
      title: `Geolocating project ${doc._id.toString()} with address ${search}`,
      search: search,
      docId: doc._id.toString()
    })
    .attempts(1) // Keeping this to one to prevent a ton of tries.
    .backoff({
      delay: 60 * 1000,
      type: "fixed"
    })
    .save();
});

/**
 * Email the owner of the organization alerting them about a completely survey.
 *
 * @async
 * @param {Object} Project model
 * @return {void}
 */
schema.methods.sendCompletionNotifications = async function() {
  const owners = await this.getOwners();
  const org = await this.model("Organization").findById(this.organization);

  const emailSettings = {
    to: owners.map(o => o.email).join(","),
    subject: "Project Map It |  You have a new customer review!"
  };

  const viewArgs = {
    project: this,
    url: `${config.url}/projects/manage`
  };

  emailHelper.send("survey-complete-owner", emailSettings, viewArgs);

  // If the first immutable survey question has a 4 or 5 rating, send the
  // customer a follow up email.
  let rating;

  for (let i = 0; i < this.survey.length; i++) {
    if (this.survey[i].immutable) {
      rating = this.survey[i].answer;
      break;
    }
  }

  const hasSocial =
    org.social_facebook ||
    org.social_google ||
    org.social_bbb ||
    org.social_yelp ||
    org.social_houzz;

  const isSatisfactory = rating === "4" || rating === "5";

  if (hasSocial && isSatisfactory) {
    this.surveyEmails.forEach(record => {
      emailHelper.send(
        "survey-complete-customer",
        {
          to: record.email,
          subject: `Thank you from ${org.name}!`
        },
        {
          org,
          project: this
        }
      );
    });
  } else {
    logger.info(
      "Not sending customer email because either the rating or the org didn't have any social links:",
      this,
      org
    );
  }
};

/**
 * Return all of the owners for a particular project.
 *
 * @return Array<User>
 */
schema.methods.getOwners = async function() {
  const orgId = _.get(this, "organization._id", this.organization);

  return await this.model("User").find({
    organization: orgId,
    role: defaultRoles.OWNER
  });
};

/**
 * Remove all projects within an organization.
 *
 * @return {object} organization
 */
schema.statics.removeAllProjects = async function(organization) {
  const all = await this.model("Project").find({
    organization: organization
  });
  for (let i = 0, len = all.length; i < len; i++) {
    await all[i].remove();
  }
};

/**
 * Bulk import projects.
 *
 * @return {array} projects
 */
schema.statics.importBulk = async function importBulk(projects, orgId) {
  const len = projects.length;
  const Project = this.model("Project");
  const ProjectCategory = this.model("ProjectCategory");
  const Organization = this.model("Organization");

  logger.info(`Getting read to import ${len} projects from a spreadsheet.`);

  const organization = await Organization.findById(orgId, "plan trialing");

  if (!organization) {
    throw new Error("Could not find organization.");
  }

  if (len > 20000) {
    throw new Error(
      "You cannot import over 20,000 projects at a time. Please " +
        "break the file up in to multiple sheets."
    );
  }

  const existing = await Project.count({
    organization: organization._id
  });

  const newTotal = existing + projects.length;

  if (newTotal > organization.pinsAllowed) {
    throw new Error(
      "You must upgrade your plan before you can import this many pins"
    );
  }

  const result = {
    projects: [],
    errors: []
  };

  for (let i = 0; i < len; i++) {
    try {
      const tags = await Project.associateTags(
        organization._id,
        processTagString(projects[i].categories)
      );

      delete projects[i].categories;

      const project = new Project(
        Object.assign({}, projects[i], {
          categories: tags,
          organization: organization._id
        })
      );

      const doc = await project.save();
      result.projects.push(doc);
    } catch (err) {
      logger.error("Error captured while saving project", err);
      result.errors.push(err);
    }
  }

  return result;
};

/**
 * Send a survey for a given project to an array of emails.
 *
 * @return {object} project
 */
schema.statics.sendSurvey = function(
  orgId,
  projId,
  commaDelimitedEmails,
  callback
) {
  const condition = {
    organization: orgId,
    _id: projId
  };

  this.model("Project")
    .findOne(condition)
    .populate("organization")
    .exec((err, project) => {
      if (err) {
        return callback(err);
      }

      if (!project) {
        return callback(new Error("Record not found"));
      }

      project.set("commaDelimitedEmails", commaDelimitedEmails);
      project.set("surveySent", new Date());

      project.save(function(err, doc) {
        if (err) {
          return callback(err);
        }
        doc.organization = project.organization;
        sendSurveyMail(doc, project.organization);
        callback(null, true);
      });
    });
};

/**
 * Transform a collection of tags/parent objects into a list of ObjectIds which
 * can get assigned to a project.
 *
 * @param {object} ProjectCategory
 * @param {ObjectId} Array<string>
 * @param {array} Array<string>
 * @return {array} Array<ObjectId>
 */
schema.statics.associateTags = async function associateTags(orgId, tags) {
  const tagIds = [];
  const ProjectCategory = this.model("ProjectCategory");

  for (let i = 0, len = tags.length; i < len; i++) {
    const { parent, tag } = tags[i];

    // Check to see if tags are already in the system.
    const existingParent = await tagExists(ProjectCategory, orgId, parent);
    const existingTag = await tagExists(
      ProjectCategory,
      orgId,
      tag,
      existingParent ? existingParent : null
    );

    // Check if there is a parent tag present. If there is, either grab it or
    // create it.
    let parentDoc = null;

    if (existingParent) {
      logger.info("Parent already exists, assigning: " + parent);
      parentDoc = existingParent;
    } else if (parent) {
      logger.info("Parent tag doesn't exist. Creating: " + parent);
      try {
        parentDoc = await ProjectCategory.create({
          organization: orgId,
          name: parent
        });
      } catch (err) {
        logger.error(
          `Error during import tag (creating parent tag) (${parent}): ${
            err.message
          }`
        );
      }
    }

    // If there is a tag that already exists with the same parent then don't
    // create a new one.
    if (
      existingTag &&
      existingTag.parent &&
      existingTag.parent.toString() === parentDoc._id.toString()
    ) {
      logger.info("Found existing tag during import for, reusing: " + tag);
      tagIds.push(existingTag._id);
    } else {
      if (!existingTag) {
        logger.info(`${tag} didn't exist.`);
      }
      if (existingTag) {
        logger.info(
          `${tag} existed, but the parent was different.`,
          existingTag.parent.toString(),
          parentDoc._id.toString()
        );
      }
      try {
        logger.info("Creating new tag during project import: " + tag);

        const insert = {
          organization: orgId,
          name: tag
        };

        if (parentDoc) {
          insert.parent = parentDoc._id;
        }

        const newDoc = await ProjectCategory.create(insert);

        if (newDoc) {
          tagIds.push(newDoc._id);
        }
      } catch (err) {
        logger.error(`Error during import tag (${tag}): ${err.message}`);
      }
    }
  }

  return tagIds;
};

/**
 * This method is used for the map queries only.
 */
schema.statics.spatialFind = async function(
  organization,
  zoom,
  bllng,
  bllat,
  urlng,
  urlat,
  cats,
  photos,
  surveys
) {
  const Project = this.model("Project");

  const query = {
    location: {
      $geoWithin: {
        $box: [[bllng, bllat], [urlng, urlat]]
      }
    },
    organization: mongoose.Types.ObjectId(organization),
    active: true
  };

  if (_.isArray(cats)) {
    query.categories = {
      $in: cats.map(mongoose.Types.ObjectId)
    };
  }

  if (surveys && photos) {
    query.$or = [
      {
        survey: {
          $elemMatch: {
            pub: true
          }
        }
      },
      {
        photoCount: {
          $gt: 0
        }
      }
    ];
  } else {
    if (surveys) {
      query.survey = {
        $elemMatch: {
          pub: true
        }
      };
    } else if (photos) {
      query.photoCount = {
        $gte: 1
      };
    }
  }

  console.time("spatial query");

  const fResults = await Project.collection.find(query, {
    _id: 1,
    city: 1,
    state: 1,
    zip: 1,
    street: 1,
    photoCount: 1,
    survey: 1,
    location: 1
  });

  // Map results to a geojson feature with super minimal object names for
  // lighten the load on the response.
  const projects = await fResults.map(mapToSmallObject).toArray();

  console.timeEnd("spatial query");

  console.time("supercluster");

  const completeClusters = [];
  const singles = [];

  const index = supercluster({
    radius: 180,
    maxZoom: 13
  });

  index.load(projects);

  const clusters = index.getClusters([bllng, bllat, urlng, urlat], zoom);

  clusters.forEach(function(cluster) {
    if (cluster.properties.cluster) {
      const totalInCluster = cluster.properties.point_count;
      const children = index.getLeaves(cluster.properties.cluster_id, 100);
      const coords = [];
      const pts = [];
      const len = children.length;

      for (let i = 0; i < len; i++) {
        pts.push(children[i].properties);
        coords.push(children[i].geometry.coordinates);
      }

      completeClusters.push({
        t: totalInCluster,
        pts,
        c: centroid(coords)
      });
    } else {
      singles.push(cluster.properties);
    }
  });

  console.timeEnd("supercluster");

  console.time("spatial count queries");

  const total = await Project.count({
    organization: mongoose.Types.ObjectId(organization),
    active: true,
    location: {
      $exists: true,
      $not: {
        $size: 0
      }
    }
  });

  const hasReviews = await Project.count({
    organization: mongoose.Types.ObjectId(organization),
    active: true,
    location: {
      $exists: true,
      $not: {
        $size: 0
      }
    },
    survey: {
      $elemMatch: {
        pub: true
      }
    }
  });

  const hasPhotos = await Project.count({
    organization: mongoose.Types.ObjectId(organization),
    active: true,
    location: {
      $exists: true,
      $not: {
        $size: 0
      }
    },
    photoCount: {
      $gte: 1
    }
  });

  console.timeEnd("spatial count queries");

  return {
    total,
    hasReviews,
    hasPhotos,
    clusters: completeClusters,
    singles
  };
};

function centroid(arr) {
  const len = arr.length;
  let minX;
  let maxX;
  let minY;
  let maxY;
  let i = 0;

  for (; i < len; i++) {
    minX = arr[i][0] < minX || minX == null ? arr[i][0] : minX;
    maxX = arr[i][0] > maxX || maxX == null ? arr[i][0] : maxX;
    minY = arr[i][1] < minY || minY == null ? arr[i][1] : minY;
    maxY = arr[i][1] > maxY || maxY == null ? arr[i][1] : maxY;
  }

  return [(minX + maxX) / 2, (minY + maxY) / 2];
}

function mapToSmallObject(project) {
  return {
    type: "Feature",
    properties: {
      _id: project._id.toString(),
      a: {
        city: project.city,
        state: project.state,
        zip: project.zip,
        street: project.street
      },
      s: project.survey && project.survey.length,
      p: project.photoCount || 0,
      l: project.location
    },
    geometry: {
      type: "Point",
      coordinates: project.location
    }
  };
}

function sendSurveyMail(project, org) {
  project.surveyEmails.forEach(function(e, key) {
    const emailSettings = {
      to: e.email,
      subject: `${org.name} | Customer Survey`
    };
    const viewArgs = {
      email: e.email,
      baseUrl: config.url,
      url: `${config.url}/survey/${project.id}`,
      project: project
    };

    emailHelper.send("survey", emailSettings, viewArgs);
  });
}

/**
 * Trim and properly format tag name.
 *
 * @param {string} name
 * @return {string} cleaned name
 */
function cleanTag(name) {
  return _.toLower(_.trim(name));
}

/**
 * This function will tag a comma delimited string and return well formatted
 * and clean tags.
 *
 * @param {string} tagStr "window, category"
 * @return {array} []{parent, tag}
 */
function processTagString(tagStr) {
  const tags = [];

  if (_.isString(tagStr) && !_.isEmpty(tagStr)) {
    const parts = _.compact(tagStr.split(",").map(cleanTag));

    for (let i = 0, len = parts.length; i < len; i++) {
      if (parts[i].match(/:/)) {
        const childParent = parts[i].split(":");
        tags.push({
          parent: cleanTag(childParent[0]),
          tag: cleanTag(childParent[1])
        });
      } else {
        tags.push({
          tag: parts[i]
        });
      }
    }
  }

  return tags;
}

/**
 * Check to see if tag exists.
 *
 * @param {ObjectId} The organization's id
 * @param {string} name
 * @return {boolean|object} exists
 */
async function tagExists(ProjectCategoryModel, orgId, name, parentId) {
  if (!name) {
    return false;
  }

  const query = {
    organization: orgId,
    name: new RegExp(_.escapeRegExp(name), "i")
  };

  if (parentId) {
    query.parent = parentId;
  }

  const doc = await ProjectCategoryModel.findOne(query, { _id: 1, parent: 1 });

  return doc ? doc : false;
}

module.exports = mongoose.model("Project", schema);
