const _            = require('lodash');
const mongoose     = require('mongoose');
const Schema       = mongoose.Schema;

const schema = new Schema({
  email: {
    type: String,
    required: 'Email required',
    validate: {
      validator: function(v) {
        return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(v);
      },
      message: 'Invalid email'
    }
  },
  name: {
    type: String,
    required: 'Name required'
  },
  orgId: {
    type: Schema.Types.ObjectId
  },
  orgName: {
    type: String,
    required: 'Organization Name required'
  },
  temp: {
    type: Boolean,
    default: false
  },
  reason: {
    type: String,
    required: 'Reason required'
  },
  removed: {
    type: Date,
    default: Date.now
  }
});

const Model = mongoose.model('AccountRemove', schema);

module.exports = Model;
