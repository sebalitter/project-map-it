const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const crypto = require('crypto');

const schema = new Schema({
  user: {
    required: true,
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  token: {
    type: String,
    default: function() {
      return crypto.randomBytes(64);
    }
  },
  created: {
    type: Date,
    default: Date.now
  }
});

const Model = mongoose.model('RememberToken', schema);

module.exports = Model;
