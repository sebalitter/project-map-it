const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const _ = require("lodash");
const logger = require("../helpers/logger");
const imageup = require("../helpers/imageup");

const schema = new Schema({
  organization: {
    type: Schema.Types.ObjectId,
    ref: "Organization",
    required: "Organization required"
  },
  order: {
    type: Number
  },
  reference: {
    type: Schema.Types.ObjectId,
    required: "Reference required"
  },
  path: {
    small: String,
    large: String
  },
  meta: Schema.Types.Mixed,
  created: {
    type: Date,
    default: Date.now
  }
});

schema.pre("save", function(next) {
  if (this.isNew) {
    this.model("Project").findOneAndUpdate(
      {
        _id: this.reference
      },
      {
        $inc: {
          photoCount: 1
        }
      },
      next
    );
  } else {
    next();
  }
});

schema.pre("remove", async function(next) {
  const imgRegExp = /.*\/+/;
  const small = getLastSegment(_.get(this, "path.small", ""));
  const large = getLastSegment(_.get(this, "path.large", ""));

  imageup.remove([small, large]);

  try {
    await this.model("Project").findOneAndUpdate(
      {
        _id: this.reference
      },
      {
        $inc: {
          photoCount: -1
        }
      }
    );
    next();
  } catch (err) {
    next(err);
  }
});

schema.statics.order = async function(ids) {
  let i = 0;
  for (var l = ids.length; i < l; i++) {
    const doc = await this.findById(ids[i]);
    doc.set("order", i);
    await doc.save();
  }
};

// Get the filename from a url.
function getLastSegment(url) {
  const segs = (url || "").split("/");
  return segs[segs.length - 1];
}

const Model = mongoose.model("File", schema);

module.exports = Model;
