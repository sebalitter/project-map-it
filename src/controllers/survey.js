const config = require("config");
const express = require("express");
const router = express.Router();
const { Project, Question, Organization } = require("../models");
const { isUndefined, isNull } = require("lodash");
const emailHelper = require("../helpers/email");
const errorFormat = require("../helpers/errorFormat");
const logger = require("../helpers/logger");

// Display a survey.
router.get("/:id", async function(req, res) {
  try {
    const { org, project, questions } = await getProjectModels(req.params.id);

    res.status(200).render("survey", {
      submitted: false,
      orgName: org.name,
      project: project,
      questions: questions,
      error: [],
      info: []
    });
  } catch (err) {
    res.status(400).render("survey", {
      submitted: false,
      orgName: "",
      project: {},
      questions: [],
      error: errorFormat(err).errors,
      info: []
    });
  }
});

// Submit a survey.
router.post("/:id", async function(req, res) {
  try {
    const { org, project, questions } = await getProjectModels(req.params.id);

    project.set("surveyCompleted", new Date());

    if (req.body.email) {
      project.set("commaDelimitedEmails", req.body.email);
    }

    project.set(
      "survey",
      questions.map(q => ({
        immutable: q.immutable,
        pub: q.pub,
        question: q.text,
        kind: q.kind,
        answer: req.body[q._id]
      }))
    );

    await project.save();

    project.sendCompletionNotifications();

    return res.status(200).render("survey", {
      submitted: true,
      project: project,
      questions: [],
      orgName: org.name,
      error: [],
      info: ["Survey sent, thanks!"]
    });
  } catch (err) {
    res.status(400).render("survey", {
      submitted: true,
      orgName: "",
      project: {},
      questions: [],
      error: errorFormat(err).errors,
      info: []
    });
  }
});

/**
 * Get the project, org, and question models for a project.
 *
 * @async
 * @param {string} projectId
 * @return {Object} org, project, questions
 */
async function getProjectModels(projectId) {
  const project = await Project.findById(projectId);

  if (!project) {
    throw new Error("No project found.");
  }

  const org = await Organization.findById(project.organization);

  if (!org) {
    throw new Error("No organization found.");
  }

  if (
    !isUndefined(project.surveyCompleted) &&
    !isNull(project.surveyCompleted)
  ) {
    throw new Error("Survey already filled.");
  }

  const questions = await Question.find({
    organization: project.organization
  }).sort({ order: 1 });

  return { org, project, questions };
}

module.exports = router;
