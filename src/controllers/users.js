const _ = require("lodash");
const express = require("express");
const router = express.Router();
const User = require("../models/user");
const Project = require("../models/project");
const protect = require("../middlewares/api-protect");
const emailHelper = require("../helpers/email");
const errorFormat = require("../helpers/errorFormat");
const Organization = require("../models/organization");
const async = require("async");
const config = require("config");

const { roles } = config;

const options = {
  populate: "organization"
};

const crud = require("../helpers/crud")("User", options);

router.get("/", protect.forceUserOrganization, crud.get);
router.get("/:id", protect.forceUserOrganization, crud.getOne);

/**
 * This will invite a new user using fields provided.
 */
router.post("/", protect.forceUserOrganization, async function(req, res) {
  try {
    let doc = new User(
      Object.assign({}, req.body, {
        organization: _.get(
          req,
          "user.organization._id",
          req.user.organization
        ),
        password: new Date().toString() + _.random(100, 999)
      })
    );

    doc = await doc.save();
    await doc.sendInvitation();
    res.status(200).json(doc.toJSON());
  } catch (err) {
    return res.status(400).json(errorFormat(err));
  }
});

router.post("/deactivate", protect.forceUserOrganization, async function(
  req,
  res
) {
  if (!req.body.reason) {
    return res
      .status(400)
      .json(errorFormat("A reason must be sent to deactivate an account"));
  }

  try {
    const user = await User.findById(req.user);

    if (!user) {
      throw new Error("User not found.");
    }

    await user.deactivate(req.body.reason);
    return res.status(200).json({ ok: 1 });
  } catch (err) {
    return res.status(400).json(errorFormat(err));
  }
});

router.delete("/:id", protect.forceUserOrganization, function(req, res) {
  if (!req.user.isSuperAdmin() && !req.user.isOwner()) {
    return res
      .status(400)
      .json(errorFormat("Only owners can remove a team member."));
  }

  const query = crud._getQuery(req);
  _.merge(query, {
    _id: req.params.id
  });
  User.findOne(query, function(err, user) {
    if (err) {
      return res.status(400).json(errorFormat(err));
    }
    if (!user) {
      return res.status(400).json(errorFormat("Record not found"));
    }
    if (user.isOwner()) {
      return res
        .status(400)
        .json(errorFormat("Owners can't be removed - cancel account instead"));
    }
    user.remove(function(err) {
      if (err) {
        return res.status(400).json(errorFormat(err));
      }
      res.status(200).json({
        message: "Record removed"
      });
    });
  });
});

router.put(
  "/:id",
  protect.forceUserOrganization,

  // Middleware for updating necessary fields on the organization.
  async function(req, res, next) {
    if (
      req.user.role === roles.OWNER &&
      req.user.organization &&
      req.body.orgname
    ) {
      try {
        const org = await Organization.findById(req.user.organization);

        if (!org) {
          return res.status(400).json({
            errors: ["Organization not found"]
          });
        }

        // Remove the fields from the body so they don't persist to the next
        // user middleware.
        org.set("name", req.body.orgname);
        org.set("social_google", req.body.social_google);
        org.set("social_facebook", req.body.social_facebook);
        org.set("social_yelp", req.body.social_yelp);
        org.set("social_houzz", req.body.social_houzz);
        org.set("social_bbb", req.body.social_bbb);

        delete req.body.social_bbb;
        delete req.body.social_google;
        delete req.body.social_yelp;
        delete req.body.social_houzz;
        delete req.body.social_bbb;

        await org.save();

        // Update fields for the organization.
      } catch (err) {
        return res.status(400).json(errorFormat(err));
      }
    }
    next();
  },
  crud.put
);

router.post("/invite/:id", protect.forceUserOrganization, function(
  req,
  res,
  next
) {
  const queryOpts = { runValidators: true, context: "query", new: true };
  const query = crud._getQuery(req);
  req.body.unconfirmed = true;
  _.merge(query, { _id: req.params.id });

  User.findOneAndUpdate(query, req.body, queryOpts, function(err, doc) {
    if (err) {
      return res.status(400).json(errorFormat(err));
    }
    if (!doc) {
      return res.status(400).json(errorFormat("Record not found"));
    }
    doc.sendInvitation();
    res.json(doc.toJSON());
  });
});

module.exports = router;
