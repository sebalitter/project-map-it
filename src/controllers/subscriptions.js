const express      = require('express');
const router       = express.Router();
const User         = require('../models/user');
const Organization = require('../models/organization');
const crud         = require('../helpers/crud')('User', {});
const errorFormat  = require('../helpers/errorFormat');

router.post('/subscribe', async function(req, res) {
  try {
    const { card, plan } = req.body;
    const org = await Organization.findById(req.user.organization);
    await org.updateSubscription(plan, card);

    res.status(200).json({
      message: `You subscribed ${plan} successfully.`
    });
  } catch (err) {
    return res.status(400).json(errorFormat(err));
  }
});

router.post('/unsubscribe', async function(req, res) {
  try {
    const org = await Organization.findById(req.user.organization);
    await org.cancelSubscription();

    res.status(200).json({
      message: `Your subscription has been canceled.`
    });
  } catch (err) {
    return res.status(400).json(errorFormat(err));
  }
});

router.get('/info', async function(req, res) {
  try {
    const organization = await Organization.findById(req.user.organization);
    const info = await organization.accountDetails();
    res.status(200).json(info);
  } catch (err) {
    res.status(400).json(errorFormat(err));
  }
});

router.post('/promo', async function(req, res) {
  try {
    const org = await Organization.findById(req.user.organization);
    const info = await org.applyCoupon(req.body.promo);

    res.status(200).json({
      message: `Your promo code has been applied.`
    });
  } catch (err) {
    res.status(400).json(errorFormat(err));
  }
});

module.exports = router;
