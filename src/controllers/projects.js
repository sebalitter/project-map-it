const express = require("express");
const router = express.Router();
const moment = require("moment");
const Project = require("../models/project");
const File = require("../models/file");
const Organization = require("../models/organization");
const protect = require("../middlewares/api-protect");
const { each, flatten, omit } = require("lodash");
const json2csv = require("json2csv");
const geo = require("../helpers/geo");
const config = require("config");
const errorFormat = require("../helpers/errorFormat");
const logger = require("../helpers/logger");
const mongoose = require("mongoose");
const async = require("async");

const options = {
  populate: "categories"
};

const crud = require("../helpers/crud")("Project", options);

router.get(
  "/spatial/:org/:zoom/:bllng/:bllat/:urlng/:urlat/:cats/:photos/:surveys",
  async function(req, res) {
    const cats = req.params.cats === "none" ? null : req.params.cats.split(",");
    const bllng = parseFloat(req.params.bllng, 10);
    const bllat = parseFloat(req.params.bllat, 10);
    const urlng = parseFloat(req.params.urlng, 10);
    const urlat = parseFloat(req.params.urlat, 10);
    const zoom = parseInt(req.params.zoom, 10);
    const photos = parseInt(req.params.photos, 10);
    const surveys = parseInt(req.params.surveys, 10);

    try {
      const results = await Project.spatialFind(
        req.params.org,
        zoom,
        bllng,
        bllat,
        urlng,
        urlat,
        cats,
        photos ? true : false,
        surveys ? true : false
      );
      res.json({ results });
    } catch (err) {
      return res.status(400).json({
        errors: [err.message]
      });
    }
  }
);

router.get("/csv", protect.forceUserOrganization, function(req, res) {
  const query = crud._getQuery(req);
  const select = options.select || {};
  const sort = req.query.sort
    ? JSON.parse(req.query.sort)
    : options.sort || { name: "asc" };

  const modelQuery = Project.find(query);

  if (select) {
    modelQuery.select(select);
  }

  if (options.populate) {
    modelQuery.populate(options.populate);
  }

  modelQuery.sort(sort).exec(function(err, results) {
    if (err) {
      return res.status(400).json(errorFormat(err));
    }
    if (!results) {
      return res.status(404).json({});
    }
    const fields = [
      "name",
      "state",
      "city",
      "zip",
      "street",
      "lat",
      "lng",
      "review",
      "categories",
      "created",
      "emails"
    ];
    const docs = [];
    each(results, function(result) {
      const doc = result.toJSON();
      doc.categories = doc.categories.map(obj => obj.name).join(" | ");
      doc.created = moment(doc.created).format("dddd, MMMM Do YYYY, h:mm a");
      const emails = doc.surveyEmails.map(item => item.email);
      doc.emails = emails.join(",");
      docs.push(doc);
    });
    json2csv({ data: docs, fields: fields }, function(err, csv) {
      if (err) {
        return res.status(400).json(errorFormat(err));
      }
      return res
        .status(200)
        .type("text")
        .send(csv);
    });
  });
});

router.get("/map-defaults/:slug", function(req, res) {
  const formatPoint = pt => {
    return [pt[1], pt[0]];
  };
  Organization.findOne({ slug: req.params.slug }, (err, org) => {
    if (err) {
      return res.status(404).json(errorFormat(err));
    }
    if (!org) {
      return res.status(404).json(errorFormat("Organization not found."));
    }
    const query = Project.where({
      organization: org._id,
      location: {
        $exists: true,
        $not: {
          $size: 0
        }
      }
    });
    const defaultBox = {
      north: 68,
      south: 22.4918787,
      west: -137.969815,
      east: -42.7842681
    };
    query
      .select("location")
      .lean()
      .exec(function(err, locs) {
        res.json({
          bbox: locs.length
            ? geo.getBBoxExtent(locs.map(o => o.location.reverse()))
            : defaultBox
        });
      });
  });
});

// Remove all projects.
router.delete("/all", async function(req, res) {
  if (!req.user || !req.user.organization) {
    return res.status(400).json(errorFormat("Insufficient permissions."));
  }

  try {
    Project.removeAllProjects(req.user.organization);

    res.status(200).json({
      success: 1
    });
  } catch (err) {
    res.status(400).json(errorFormat(err));
  }
});

// Send a survey for a project.
router.put("/survey/:id", protect.forceUserOrganization, function(req, res) {
  Project.sendSurvey(
    req.user.organization,
    mongoose.Types.ObjectId(req.params.id),
    req.body.commaDelimitedEmails,
    (err, sent) => {
      if (err) {
        return res.status(400).json(errorFormat(err));
      }
      return res.status(200).json({
        message: "Survey emails sent"
      });
    }
  );
});

// Send all surveys for an organization for all projects.
router.post("/survey", protect.forceUserOrganization, async function(req, res) {
  try {
    const query = {
      organization: req.user.organization,
      surveyCompleted: { $exists: false },
      "surveyEmails.0": { $exists: true }
    };
    const count = await Project.count(query).exec();
    if (count === 0) {
      return res.status(200).json({ message: `No surveys available to send` });
    }
    const projects = await Project.find(query).exec();

    // This is async - so server will no block the response and send in the background
    projects.forEach(project => {
      Project.sendSurvey(
        req.user.organization,
        project.id,
        project.surveyEmails.map(item => item.email).join(","),
        (err, sent) => {
          if (!err) {
            // Maybe here we could count actual surveys sent
          }
        }
      );
    });
    return res.status(200).json({ message: `${count} survey emails sent` });
  } catch (error) {
    return res.status(400).json({ message: error.message, some: "here" });
  }
});

router.get("/location/:id", protect.forceUserOrganization, function(req, res) {
  Project.findOne(
    {
      organization: req.user.organization,
      _id: mongoose.Types.ObjectId(req.params.id)
    },
    "location",
    function(err, project) {
      if (err) {
        return res.status(400).json(errorFormat(err));
      }
      if (!project) {
        return res.status(404).json(errorFormat("Project does not exist."));
      }
      return res.json(project);
    }
  );
});

router.post("/import", protect.forceUserOrganization, async function(req, res) {
  try {
    const { projects, errors } = await Project.importBulk(
      req.body,
      req.user.organization
    );

    return res.status(200).json({
      results: projects || [],
      errors: flatten(errors)
    });
  } catch (err) {
    return res.status(400).json(errorFormat(err));
  }
});

// Update a project.
router.put("/:id", protect.forceUserOrganization, async (req, res) => {
  try {
    const project = await Project.findOne({
      organization: req.user.organization,
      _id: req.params.id
    });

    if (!project) {
      throw new Error("Could not find project.");
    }

    each(omit(req.body, ["__v", "_id"]), (v, k) => {
      project.set(k, v);
    });

    await project.save();
    res.status(200).json(project.toJSON());
  } catch (err) {
    return res.status(400).json(errorFormat(err));
  }
});

router.delete("/:id", protect.forceUserOrganization, crud.delete);
router.get("/", protect.forceUserOrganization, crud.get);
router.get("/:id", crud.getOne);
router.post("/", protect.forceUserOrganization, crud.post);

module.exports = router;
