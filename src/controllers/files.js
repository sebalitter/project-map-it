const express = require("express");
const router = express.Router();
const fs = require("fs");
const os = require("os");
const path = require("path");
const config = require("config");
const _ = require("lodash");
const Busboy = require("busboy");
const { spawn } = require("child_process");
const File = require("../models/file");
const errorFormat = require("../helpers/errorFormat");
const logger = require("../helpers/logger");
const io = require("../helpers/socket");
const imageup = require("../helpers/imageup");
const { rand } = require("../helpers/security");

const crud = require("../helpers/crud")("File", {
  sort: {
    order: -1
  }
});

const MAX_MB = 15;
const MAX_FILES = 5;
const MAX_SIZE = MAX_MB * 1024 * 1024;
const SUPPORTED_FILES = [
  "image/jpeg",
  "image/jpg",
  "image/pjpeg",
  "image/pjpeg",
  "image/png",
  "image/gif"
];

// Upload by reference
router.post("/upload/:idReference", function(req, res) {
  if (!req.user.organization || !req.params.idReference) {
    logger.warn("Organization or idref not provided when uploading image.");
    return res.status(400).json(errorFormat(new Error("Invalid request.")));
  }

  try {
    const busboy = new Busboy({
      headers: req.headers,
      limits: {
        fileSize: MAX_SIZE,
        files: MAX_FILES
      }
    });

    processFiles(
      req.params.idReference,
      req.user.organization.toString(),
      busboy,
      function() {
        res.status(200).json({
          ok: 1
        });
      },
      function(err) {
        logger.warn("Image upload had an error: " + err.message);
        req.unpipe(busboy);
        return res.status(400).json(errorFormat(err));
      }
    );

    req.pipe(busboy);
  } catch (err) {
    logger.error("Busboy error thrown!", err);
    return res.status(400).json(errorFormat(err));
  }
});

router.delete("/:id", crud.delete);
router.get("/", crud.get);
router.get("/:id", crud.getOne);

router.put("/order", async function(req, res) {
  await File.order(req.body);
  return res.status(200).json({
    success: 1
  });
});

// Stream the files to disk.
function processFiles(projectID, orgID, busboy, onSuccess, onError) {
  const sourceFiles = [];

  busboy.on("file", onFile);

  busboy.on("finish", finish);

  busboy.on("filesLimit", () => {
    busboy.removeAllListeners("finish");
    onError(new Error(`You may only upload up to ${MAX_FILES} at a time.`));
  });

  // Clean up any temp files incase of error.
  function cleanup() {
    logger.info("Cleaning up temp files from disk.");

    for (let i = 0, len = sourceFiles.length; i < len; i++) {
      if (typeof sourceFiles[i] === "string") {
        fs.unlink(sourceFiles[i], err => {
          if (err) {
            logger.error("Error removing file from disk:" + err.message);
          }
        });
      }
    }
  }

  function checkValidity() {
    for (let i = 0, len = sourceFiles.length; i < len; i++) {
      if (sourceFiles[i] instanceof Error) {
        return sourceFiles[i];
      }
    }
  }

  function finish(err) {
    if (checkValidity() instanceof Error) {
      onError(checkValidity());
      cleanup();
      return;
    }

    // Now that we know that there aren't any errors with the files, we can
    // respond back to the user and begin uploading.
    onSuccess();

    logger.info("All photos look good from user. Now going to write to store.");

    // Now upload all photos to imageup.
    const promises = [];

    for (let i = 0, len = sourceFiles.length; i < len; i++) {
      promises.push(
        imageup.upload(sourceFiles[i], [
          {
            name: "thumb",
            width: 180,
            height: 180,
            fill: true
          },
          {
            name: "full",
            width: 1200,
            height: 1200,
            fill: false
          }
        ])
      );

      promises[promises.length - 1].catch(function(err) {
        logger.warn(
          "There was an error when uplaoding an image to ImageUp: " +
            err.message
        );
      });

      // When each image finishes, save a File for it which associates it to the
      // project.
      promises[promises.length - 1].then(async function(files) {
        try {
          await File.create({
            organization: orgID,
            reference: projectID,
            path: {
              small: files[0].url,
              large: files[1].url
            },
            meta: {
              width: files[1].width,
              height: files[1].height
            }
          });
          io().emit(projectID);
        } catch (err) {
          logger.error(
            "There was an error when saving a file for a uploaded image: " +
              err.message
          );
        }
      });
    }

    // When all images finish, clean up the tmp files.
    Promise.all(promises).then(() => {
      logger.info("All uploads complete. Cleaning up from disk.");
      cleanup();
    });
  }

  function onFile(fieldname, file, filename, encoding, mimetype) {
    const sourceFile = path.join(
      os.tmpdir(),
      "img-" + rand() + "--" + path.basename(fieldname)
    );

    if (SUPPORTED_FILES.indexOf(mimetype) === -1) {
      logger.warn("User attempted to upload a bad file type: " + mimetype);
      sourceFiles.push(
        new Error(
          "Sorry, but the image format you tried uploading is not supported. Please make sure you're using either a JPEG or PNG file format."
        )
      );
    }

    file.pipe(fs.createWriteStream(sourceFile));

    file.on("error", err => {
      logger.warn(
        "There was an unknown/strange error with an uploaded file: " +
          err.message
      );
      sourceFiles.push(err);
    });

    // If the file is too big, just skip the data and register a error.
    file.on("limit", () => {
      logger.warn("File uploaded is too big.");
      file.resume();
      sourceFiles.push(
        new Error(
          `Please reduce the size of the image. The maximum file size is ${MAX_MB}mb.`
        )
      );
    });

    file.on("end", async () => {
      sourceFiles.push(sourceFile);
    });
  }
}

module.exports = router;
