const express = require("express");
const { apiAuth } = require("config");
const { Organization } = require("../models");
const { requireLogin } = require("../middlewares/api-protect");
const { isUndefined, isNull } = require("lodash");
const errorFormat = require("../helpers/errorFormat");
const { validMongoId } = require("../helpers/mongoose");

const router = express.Router();

// Grab existing by slug OR id.
router.get("/:id", async (req, res) => {
  let org;

  if (req.params.id === "me") {
    if (req.user) {
      org = await Organization.findById(req.user.organization);
    }
  } else if (validMongoId(req.params.id)) {
    org = await Organization.findById(req.params.id);
  } else {
    org = await Organization.findOne({
      slug: req.params.id
    });
  }

  if (!org) {
    res.status(404).json(errorFormat("Could not find organization."));
  }

  res.json(org.toJSON());
});

// Update existing. This will onlu update the logged in user's organizaton if
// they have the correct role.
router.put(
  "/",
  requireLogin(apiAuth.crud.organizations.PUT),
  async (req, res) => {
    const allowedToUpdate = [
      "name",
      "social_google",
      "social_facebook",
      "social_yelp",
      "social_houzz",
      "social_bbb",
      "social_angies_list",
      "social_home_advisor"
    ];

    const org = req.organization;

    if (!org) {
      res.status(404).json(errorFormat("Could not find organization."));
    }

    allowedToUpdate.forEach(prop => {
      if (!isUndefined(req.body[prop]) && !isNull(req.body[prop])) {
        org.set(prop, req.body[prop]);
      }
    });

    try {
      await org.save();
      res.json(org.toJSON());
    } catch (err) {
      res.status(400).json(errorFormat(err));
    }
  }
);

module.exports = router;
