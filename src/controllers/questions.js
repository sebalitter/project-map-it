const express = require("express");
const router = express.Router();
const Question = require("../models/question");
const protect = require("../middlewares/api-protect");
const errorFormat = require("../helpers/errorFormat");

router.get("/", protect.forceUserOrganization, async function(req, res) {
  try {
    const questions = await Question.find(
      {
        organization: req.user.organization
      },
      null,
      {
        sort: {
          order: 1
        }
      }
    );

    res.status(200).json({
      results: questions
    });
  } catch (err) {
    return res.status(400).json(errorFormat(err));
  }
});

router.put("/", protect.forceUserOrganization, async function(req, res) {
  try {
    const docs = await Question.apiUpdate(
      req.user.organization,
      req.body.questions || []
    );
    res.status(200).json({
      response: {
        results: docs
      }
    });
  } catch (err) {
    res.status(400).json(errorFormat(err));
  }
});

module.exports = router;
