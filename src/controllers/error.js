const logger = require('../helpers/logger');
module.exports = (err, req, res, next) => {
  logger.error(err);
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: process.env.NODE_ENV === 'development' ? err : null
  });
};
