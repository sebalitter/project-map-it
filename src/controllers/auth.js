const express = require("express");
const router = express.Router();
const passport = require("passport");
const RememberToken = require("../models/remember-token");
const Organization = require("../models/organization");
const User = require("../models/user");
const Project = require("../models/project");
const emailHelper = require("../helpers/email");
const _ = require("lodash");
const config = require("config");
const logger = require("../helpers/logger");
const protect = require("../middlewares/api-protect");
const errorFormat = require("../helpers/errorFormat");

const { OWNER } = config.roles;

function renderLoginView(req, res, props) {
  return res.status(props && props.error ? 400 : 200).render(
    "login",
    _.defaults(props || {}, {
      title: "Sign In | Project Map It",
      description: "",
      error: req.flash("error"),
      info: req.flash("info"),
      fields: {}
    })
  );
}

function renderRegisterView(req, res, props) {
  return res.status(props && props.error ? 400 : 200).render(
    "register",
    _.defaults(props || {}, {
      title: "Create an Account | Project Map It",
      description: "",
      error: req.flash("error"),
      info: req.flash("info"),
      fields: {}
    })
  );
}

function renderForgotView(req, res, props) {
  return res.status(props && props.error ? 400 : 200).render(
    "forgot",
    _.defaults(props || {}, {
      title: "I forgot my password | Project Map It",
      description: "",
      error: req.flash("error"),
      info: req.flash("info"),
      fields: {}
    })
  );
}

function renderResetView(req, res, props) {
  return res.status(props && props.error ? 400 : 200).render(
    "reset",
    _.defaults(props || {}, {
      title: "Reset Password | Project Map It",
      description: "",
      error: req.flash("error"),
      info: req.flash("info"),
      fields: {}
    })
  );
}

/**
 * Send out a GAF alert email.
 */
function gafbucksAlert(user, org) {
  const emailSettings = {
    to: config.gafbucksRecipients,
    subject: "Project Map It | GAF Bucks alert"
  };

  const viewArgs = {
    name: user.name,
    email: user.email,
    cell: user.cell,
    orgname: org.name
  };

  emailHelper.send("gafbucksalert", emailSettings, viewArgs);
}

router.get("/login", (req, res) => {
  renderLoginView(req, res);
});

router.get("/logout", (req, res) => {
  req.logout();
  process.nextTick(() => res.redirect("/"));
});

router.get("/register", (req, res) => {
  renderRegisterView(req, res);
});

router.get("/forgot", (req, res) => {
  renderForgotView(req, res);
});

router.post(
  "/login",
  function(req, res, next) {
    passport.authenticate("local", async function(err, user, info) {
      if (err) {
        return next(err);
      }

      if (!user) {
        return res.redirect("/login");
      }

      const organization = await Organization.findOne(
        {
          _id: user.organization
        },
        "active name"
      );

      req.logIn(user, function(err) {
        if (err) {
          return next(err);
        }

        // If the organization is inactive, bring them straigh to the
        // registration page so the owner can upgrade.
        if (user.role === OWNER && !organization.active) {
          return res.redirect("/account/subscription");
        }

        // Otherwise, go straight to the map.
        return res.redirect("/");
      });
    })(req, res, next);
  },
  function(req, res) {
    res.redirect("/");
  }
);

router.post("/register", async (req, res) => {
  const data = req.body;

  const userObj = {
    name: data.name,
    email: data.email,
    password: data.password,
    cell: data.cell,
    unconfirmed: false,
    role: OWNER
  };

  const orgObj = {
    name: data.organization
  };

  if (!_.isEmpty(data.gafbucks)) {
    orgObj.gafbucks = true;
  }

  try {
    if (_.isEmpty(data.password)) {
      throw new Error("You must provide a password.");
    }

    if (data.password !== data.password_confirm) {
      throw new Error("Passwords do not match.");
    }

    if (!data.agree) {
      throw new Error("You must agree to the terms and conditions.");
    }

    const organization = await new Organization(orgObj).save();

    userObj.organization = organization._id;

    // Remove the organization if the user failed. This prevents a zombie
    // organization.
    let user;

    try {
      user = await new User(userObj).save();
    } catch (err) {
      organization.remove();
      throw err;
    }

    const customer = await organization.initStripeWithUser(user);

    organization.createPresetQuestions();

    try {
      if (data.promo) {
        await organization.applyCoupon(data.promo);
      }
    } catch (err) {
      logger.warn(`${organization.name}'s coupon did not work: ${data.promo}`);
    }

    if (organization.gafbucks) {
      gafbucksAlert(user, organization);
    }

    req.flash("info", "Account created! Please sign in.");
    res.redirect("/login");
  } catch (err) {
    return renderRegisterView(req, res, {
      fields: data,
      error: errorFormat(err).errors
    });
  }
});

router.get(/\/reset\/(.+)/, (req, res) => {
  User.findOne(
    {
      resetHash: req.params[0]
    },
    function(err, user) {
      if (err) {
        req.flash("error", "Error retrieving user.");
        res.redirect("/forgot");
        return;
      }

      if (!user) {
        req.flash("error", "User not found.");
        res.redirect("/forgot");
        return;
      }

      renderResetView(req, res, {
        resetUrl: `${config.url}/reset/${user.resetHash}`
      });
    }
  );
});

router.post("/forgot", async (req, res) => {
  const email = _.get(req, "body.email", "");

  const user = await User.findOne({ email });

  if (!user) {
    return renderForgotView(req, res, {
      error: errorFormat(new Error("Could not find user.")).errors
    });
  }

  await user.sendInvitation();

  return renderForgotView(req, res, {
    fields: {},
    info: ["Success! Please check your email for the next step."]
  });
});

router.post(/\/reset\/(.+)/, (req, res, next) => {
  const data = req.body;

  User.findOne(
    {
      resetHash: req.params[0]
    },
    function(err, user) {
      if (err) {
        req.flash("error", "Err retrieving user.");
        res.redirect("/forgot");
        return;
      }
      if (!user) {
        req.flash("error", "User not found.");
        res.redirect("/forgot");
        return;
      }
      if (data.password !== data.password_confirm) {
        return renderResetView(req, res, {
          resetUrl: `${config.url}/reset/${user.resetHash}`,
          error: ["Passwords do not match."]
        });
      }
      user.unconfirmed = false;
      user.password = data.password;
      user.resetHash = null;
      user.resetExpire = null;
      user.save(function(err) {
        if (err) {
          console.log(err);
          return renderResetView(req, res, {
            error: [
              "Please create a stronger password with at least 6 characters."
            ]
          });
        }
        req.flash("info", "Password successfully updated! Please login.");
        res.redirect("/login");
      });
    }
  );
});

module.exports = router;
