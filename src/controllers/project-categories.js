'use strict';

const express     = require('express');
const router      = express.Router();
const Model       = require('../models/project-category');
const Project     = require('../models/project');
const protect     = require('../middlewares/api-protect');
const async       = require('async');
const _           = require('lodash');
const crud        = require('../helpers/crud')('ProjectCategory', {});
const errorFormat = require('../helpers/errorFormat');

router.get('/', crud.get);
router.get('/:id', protect.forceUserOrganization, crud.getOne);
router.post('/', protect.forceUserOrganization, crud.post);
router.put('/:id', protect.forceUserOrganization, crud.put);
router.delete('/:id', protect.forceUserOrganization, function(req, res) {

  async.waterfall([
    // get category and children
    function(callback) {
      const query = crud._getQuery(req);
      _.merge(query, {
        $or: [{
          _id: req.params.id
        }, {
          parent: req.params.id
        }]
      });
      Model.find(query, function(err, categories) {
        if (err) {
          return callback(err);
        }
        callback(null, categories);
      });
    },
    // updates projects removing categories from the array
    function(categories, callback) {
      const query = crud._getQuery(req);
      _.merge(query, {
        categories: {$in: _.map(categories, '_id')}
      });
      Project.find(query, function (err, projects) {
        if (err) {
          return callback(err);
        }
        if (!projects) {
          return callback('Error finding projects related to this category');
        }
        if (projects.length == 0) {
          return callback(null, categories);
        }
        async.forEachOf(projects, function(proj, key, cb) {
          Project.update({
            _id: proj._id
          }, {
            $pullAll: {
              categories: categories
            }
          }, cb)
        }, function(err) {
          if (err) {
            return callback(err);
          }
          return callback(null, categories);
        });
      });
    },
    function(categories, callback) {
      const query = crud._getQuery(req);
      _.merge(query, {
        _id: {$in: _.map(categories, '_id')}
      });
      Model.remove(query, function(err, doc) {
        if (err) {
          return res.status(400).json(errorFormat(err));
        }
        callback(null, 'done');
      });
    }
  ], function (err, result) {
    if (err) {
      return res.status(400).json(errorFormat(err));
    }
    res.status(200).json({
      message: "Record(s) removed"
    });
  });
});

module.exports = router;
