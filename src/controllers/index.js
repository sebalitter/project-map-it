const _ = require("lodash");
const express = require("express");
const config = require("config");
const clientState = require("../helpers/client-state");
const apiAuth = require("../middlewares/api-auth").apiAuth;
const User = require("../models/user");
const Project = require("../models/project");
const AccountRemoval = require("../models/account-remove");
const Organization = require("../models/organization");
const moment = require("moment");
const mongoose = require("mongoose");
const basicAuth = require("basic-auth-connect");
const async = require("async");
const kue = require("kue");
const stripe = require("stripe")(config.stripeAPISecreKey);
const logger = require("../helpers/logger");

const router = express.Router();
const { roles } = config;

if (process.env.NODE_ENV !== "production") {
  router.get("/_drop", async (req, res) => {
    async function remove() {
      const customers = await stripe.customers.list({});

      for (let i = 0; i < customers.data.length; i++) {
        logger.warn("Removing stripe customer: " + customers.data[i].id);
        await stripe.customers.del(customers.data[i].id);
      }

      if (customers.has_more) {
        return await remove();
      }
    }
    await remove();

    logger.warn("Dropping database");
    mongoose.connection.db.dropDatabase();

    res.send("All data has been wiped.");
  });
}

router.use("/kue", basicAuth("projectmapit", "gogogo!"), kue.app);

router.use(require("./auth"));
router.use("/survey", require("./survey"));
router.use("/api/files", apiAuth, require("./files"));
router.use("/api/project-categories", apiAuth, require("./project-categories"));
router.use("/api/organizations", apiAuth, require("./organizations"));
router.use("/api/projects", apiAuth, require("./projects"));
router.use("/api/users", apiAuth, require("./users"));
router.use("/api/questions", apiAuth, require("./questions"));
router.use("/api/subscriptions", apiAuth, require("./subscriptions"));

router.get("/admin", basicAuth("projectmapit", "gogogo!"), function(req, res) {
  const userRoles = _.invert(roles);
  async.waterfall([
    function(cb) {
      AccountRemoval.find({})
        .lean()
        .exec(cb);
    },
    function(removals, cb) {
      User.find({
        role: 1
      })
        .populate("organization")
        .lean()
        .exec(function(err, users) {
          async.each(
            users,
            function(user, callback) {
              user.role = _.capitalize(userRoles[user.role]);
              Project.find({
                organization: user.organization
              })
                .count()
                .exec(function(err, projectCount) {
                  user.projectCount = projectCount;
                  callback();
                });
            },
            function() {
              res.render("admin", {
                users: users,
                removals: removals || [],
                moment: moment
              });
            }
          );
        });
    }
  ]);
});

router.get("/admin/remove/:id", function(req, res) {
  User.findOne(
    {
      _id: mongoose.Types.ObjectId(req.params.id)
    },
    function(err, user) {
      if (user) {
        return user.remove(function() {
          res.redirect("/admin");
        });
      }
      res.redirect("/admin");
    }
  );
});

router.get("/public/:slug", (req, res) => {
  Organization.findOne({
    slug: req.params.slug
  }).exec((err, doc) => {
    if (err) {
      return res.render("app-error", {
        error: "Organization not found"
      });
    }

    if (!doc) {
      return res.render("app-error", {
        error: "No projects to display for this organization"
      });
    }

    if (!doc.active) {
      return res.render("app-error", {
        error: "Account deactivated."
      });
    }

    res.render("app", {
      error: false,
      title: doc.name + " | Reviews & Previous Work",
      description: `See ${doc.name}'s public map on ProjectMap.it.`,
      pubOrgId: doc._id.toString(),
      initialState: clientState(false)
    });
  });
});

router.get(/.*/, (req, res) => {
  if (req.user) {
    req.user.populate("organization", (err, doc) => {
      res.render("app", {
        title: doc.organization.name + " | Project Map It",
        description: "",
        pubOrgId: doc.organization._id.toString(),
        initialState: clientState(doc)
      });
    });
  } else {
    res.redirect("/login");
  }
});

module.exports = router;
