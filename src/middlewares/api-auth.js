const _ = require('lodash');
const { apiAuth, roles } = require('config');
const { ANNONYMOUS } = roles;

function getRoutes() {
  const routes = apiAuth.static;
   _.each(apiAuth.crud, function(rules, endpoint){
    if (_.isInteger(rules)) {
      const roles = rules === ANNONYMOUS
        ? _.merge(_.range(-1, 5), [ANNONYMOUS])
        : _.range(0, rules+1);
      rules = {GET: roles, POST: roles, PUT: roles, DELETE: roles};
    }
    if (_.isArray(rules)) {
      rules = {GET: rules, POST: rules, PUT: rules, DELETE: rules};
    }
    _.each(rules, function(roles, method) {
      const rt = `/api/${endpoint}`;
      if (!routes[rt]) {
        routes[rt] = {};
      }
      if (_.isInteger(roles)) {
        roles = roles === ANNONYMOUS
              ? _.merge(_.range(-1, 5), [ANNONYMOUS])
              : _.range(-1, roles+1);
      }
      routes[rt][method] = roles;
    });
  });
  return routes;
}

function checkRoute(path, method, user) {
  const routes = getRoutes();
  if (routes[path] && routes[path][method]) {
    return user.inRoles(routes[path][method]);
  }
  return false;
}

function allowAnnonymous(path, method) {
  const routes = getRoutes();
  if (routes[path] && routes[path][method]) {
    return _.indexOf(routes[path][method], ANNONYMOUS) !== -1;
  }
  return false;
}

function _apiAuth(req, res, next) {
  if (allowAnnonymous(req.baseUrl, req.method)) {
    return next();
  }

  if (!req.user) {
    return res.status(403).json({
      errors: ['Login required Auth']
    });
  }

  if (checkRoute(req.baseUrl, req.method, req.user)) {
    return next();
  }

  return res.status(403).json({
    errors: ['Access not allowed']
  });
};

module.exports = {
  apiAuth: _apiAuth,
  allowAnnonymous: allowAnnonymous
}
