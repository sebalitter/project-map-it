const { roles } = require("config");
const { Organization } = require("../models");
const _ = require("lodash");
const allowAnnonymous = require("./api-auth").allowAnnonymous;

/**
 * Middleware to force login.
 * @param {array} rules[]<int>  An array of rules
 * @return {function}
 */
function requireLogin(rules = []) {
  return async (req, res, next) => {
    if (!req.user || (rules || []).indexOf(req.user.role) === -1) {
      return res.status(403).json({ errors: ["Insufficient permissions"] });
    }

    const org = await Organization.findOne({
      _id: req.user.organization
    });

    req.organization = org;

    next();
  };
}

function exact(role) {
  return function(req, res, next) {
    if (!req.user) {
      return res.status(403).json({ errors: ["Login required"] });
    }
    role = !_.isUndefined(roles[role]) ? roles[role] : role;
    if (req.user.role != role) {
      return res.status(403).json({ errors: ["Access not allowed"] });
    }
    next();
  };
}

function min(role) {
  return function(req, res, next) {
    if (allowAnnonymous(req.baseUrl, req.method)) {
      return next();
    }
    if (!req.user) {
      return res.status(403).json({ errors: ["Login required"] });
    }
    role = !_.isUndefined(roles[role]) ? roles[role] : role;
    if (req.user.role > role) {
      return res.status(403).json({ errors: ["Access not allowed"] });
    }
    next();
  };
}

function forceUserOrganization(req, res, next) {
  if (!req.user) {
    if (allowAnnonymous(req.baseUrl, req.method)) {
      return next();
    }
    return res.status(403).json({ errors: ["Login required"] });
  }

  if (req.user.role > roles.SUPERADMIN) {
    const filter = req.query.filter ? JSON.parse(req.query.filter) : {};

    req.query.filter = JSON.stringify(
      _.assign({}, filter, {
        organization: req.user.organization
      })
    );
  }
  next();
}

function forceUser(req, res, next) {
  if (!req.user) {
    return res.status(403).json({ errors: ["Login required"] });
  }

  // Deprecated.
  if (req.user.role >= roles.OWNER) {
    const filter = req.query.filter ? JSON.parse(req.query.filter) : {};
    req.query.filter = JSON.stringify(_.merge(filter, { user: req.user.id }));
  }

  next();
}

module.exports = {
  requireLogin,
  roles: roles,
  exact: exact,
  min: min,
  forceUserOrganization: forceUserOrganization,
  forceUser: forceUser
};
