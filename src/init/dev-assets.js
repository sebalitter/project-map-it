const path = require("path");
const express = require("express");
const babelify = require("express-babelify-middleware");
const stylus = require("stylus");

const publicDir = path.resolve(__dirname, "../", "public");
const clientDir = path.resolve(__dirname, "../", "client");

const browserifyOptions = {
  debug: true
};

const babelifyOptions = {
  presets: ["es2015", "react"]
};

function compile(str, p) {
  const nib = require("nib");
  const jeet = require("jeet");
  const rupture = require("rupture");

  return stylus(str)
    .set("include css", true)
    .set("filename", p)
    .use(nib())
    .use(jeet())
    .use(rupture());
}

// Handy for development (without nginx).
module.exports = function(app) {
  if (process.env.NODE_ENV === "development") {
    app.get(
      "/projectmapit.js",
      babelify(
        path.join(clientDir, "js/main.js"),
        browserifyOptions,
        babelifyOptions
      )
    );

    app.use(
      "/static",
      stylus.middleware({
        src: path.join(clientDir, "stylus/"),
        dest: path.join(publicDir, "stylesheets"),
        compress: false,
        compile: compile,
        force: true
      })
    );
  }

  app.use("/static", express.static(publicDir));
};
