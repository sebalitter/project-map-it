'use strict';

const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const RememberMeStrategy = require('passport-remember-me').Strategy;
const User = require('../models/user');
const RememberToken = require('../models/remember-token');

/**
 * Configure Passport Strategy.
 */
passport.use(new LocalStrategy(
  {
    usernameField: 'email',
    passwordField: 'password'
  },
  function(email, password, done) {
    User.authenticate(email, password, function (err, user) {
      if (err) {
        return done(null, false, err.message);
      }
      return done(null, user);
    });
  }
));

/// Need to implement.
passport.use(new RememberMeStrategy(
  function(token, done) {

    function onRemove(err, doc) {
      if (err) {
        return done(err);
      }
      if (!doc || !doc.user) {
        return done(null, false);
      }
      done(null, doc.user);
    }

    RememberToken.findOneAndRemove({ token: token })
      .populate('user')
      .exec(onRemove);;
  },
  function(user, done) {
    const token = new RememberToken({ user: user.id });
    token.save((err, doc) => done(err, doc.token));
  }
));

passport.serializeUser(function(user, done) {
  done(null, user._id);
});

passport.deserializeUser(function(id, done) {
  User.findById(id, function(err, user) {
    done(err, user);
  });
});

module.exports = function(app) {
  app.use(passport.initialize());
  app.use(passport.session());
  app.use(passport.authenticate('remember-me'));
};
