const config = require("config");
const path = require("path");

// Build out express application as usual.
const express = require("express");
const morgan = require("morgan");
const logger = require("./helpers/logger");
const cookieParser = require("cookie-parser");
const bodyParser = require("body-parser");
const session = require("express-session");
const MongoStore = require("connect-mongo")(session);
const flash = require("connect-flash");

const app = express();
const server = require("http").createServer(app);

// Because it's running behind nginx in prod.
// https://github.com/expressjs/morgan/issues/114
app.enable("trust proxy");
app.set("trust proxy", () => true);

// Establish connection with MongoDB.
require("./helpers/mongoose");

// Forward morgan logs to our logger.
app.use(
  morgan("combined", {
    stream: {
      write: message => logger.info(message.trim())
    }
  })
);

// Force https in production. This is necessary as we're using the GCP load
// balancer and not something like nginx.
if (process.env.NODE_ENV === "production") {
  app.all("*", (req, res, next) => {
    const proto = req.headers["x-forwarded-proto"];

    if (proto === "http") {
      res.redirect("https://" + req.headers.host + req.url);
    } else {
      next();
    }
  });
}

// Configure views and view rendering engine.
app.locals = Object.assign({}, { env: process.env }, config);

app.set("views", path.join(__dirname, "views"));
app.set("view engine", "jade");

// Google/kubernetes health check.
app.get("/healthz", (req, res) => res.send("healthy"));

// Init the web socket listener.
require("./helpers/socket")(server);

// Session configuration.
app.use(
  session({
    secret: "secretpasswordforever1",
    resave: false,
    saveUninitialized: false,
    store: new MongoStore({
      url: config.mongoConnectionString,
      ttl: 7 * 24 * 60 * 60 // = 7 days.
    })
  })
);
app.use(flash());

// Request parsing.
app.use(
  bodyParser.json({
    limit: "100mb"
  })
);
app.use(
  bodyParser.urlencoded({
    extended: false
  })
);
app.use(cookieParser());

// Initialize other middleware.
require("./init/dev-assets")(app);
require("./init/passport")(app);

// Setup routing.
app.use(require("./controllers"));
app.use(require("./controllers/404"));
app.use(require("./controllers/error"));

module.exports = server;
