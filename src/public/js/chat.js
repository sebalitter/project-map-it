setTimeout(function() {
  if (window.DESK) {
    new DESK.Widget({
      version: 1,
      site: 'support.projectmap.it',
      port: '80',
      type: 'chat',
      displayMode: 1,  //0 for popup, 1 for lightbox
      features: {
        offerAlways: false,
        offerAgentsOnline: false,
        offerRoutingAgentsAvailable: false,
        offerEmailIfChatUnavailable: false
      },
      fields: {
      ticket: {
        // desc: &#x27;&#x27;,
        // labels_new: &#x27;&#x27;,
        // priority: &#x27;&#x27;,
        // subject: &#x27;&#x27;
      },
      interaction: {
        // email: &#x27;&#x27;,
        // name: &#x27;&#x27;
      },
      chat: {
        subject: 'Hey!'
      },
      customer: {
        // company: &#x27;&#x27;,
        // desc: &#x27;&#x27;,
        // first_name: &#x27;&#x27;,
        // last_name: &#x27;&#x27;,
        // locale_code: &#x27;&#x27;,
        // title: &#x27;&#x27;
      }
      }
    }).render();
  }
}, 0);
