#!/usr/bin/env node

// Inject an immutable question for every organization that doesn't already have
// one.

require("../../helpers/mongoose");
const { Organization, Question } = require("../../models");

async function main() {
  const orgs = await Organization.find();

  for (let i = 0; i < orgs.length; i++) {
    const qs = await Question.count({
      organization: orgs[i]._id
    });

    // If they only have one question we know it's the default. Update them.
    if (qs === 1) {
      console.log("Creating new questions for " + orgs[i].name);
      // await Question.remove({ organization: orgs[i]._id });
      // await orgs[i].createPresetQuestions();
    } else {
      console.log(orgs[i].name + " has more than one question already");
    }
  }
}

main();
