/**
 * Remove all stripe customers.
 */

require('../helpers/mongoose');

const config = require('config');
const stripe = require('stripe')(config.stripeAPISecreKey);

async function main() {
  const customers = await stripe.customers.list({});

  if (process.env.MUTATE) {
    for (let i = 0; i < customers.data.length; i++) {
      console.log('Removing ' + customers.data[i].id);
      await stripe.customers.del(customers.data[i].id);
    }

    if (customers.has_more) {
      main();
    } else {
      console.info('Done!');
    }
  }
}

main();
