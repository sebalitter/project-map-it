/**
 * This is to profile the spatial query for the map.
 */

if (!process.env.NODE_ENV) {
  throw new Error("You must explicitly set NODE_ENV.");
}

require('../helpers/mongoose');

const config = require('config');
const stripe = require('stripe')(config.stripeAPISecreKey);
const { get } = require('lodash');
const moment = require('moment');

const File = require('../models/file');
const User = require('../models/user');
const Project = require('../models/project');

const TEST_USER = get(process, 'env.TEST_USER', '5a6a85e94218bc21b4ddc769');
const TEST_ZOOM = get(process, 'env.TEST_ZOOM', 8);

async function main() {

  console.time('simple query');
  const doc = await Project.findOne({});
  console.timeEnd('simple query');

  const user = await User.findById(TEST_USER);

  const zoom = TEST_ZOOM;

  const bllng = -107.31700319999993;
  const bllat = 40.13477091444459;
  const urlng = -53.17637819999993;
  const urlat = 56.08888026812078;
  const cats = 'none';
  const photos = false;
  const surveys = false;

  const result = await Project.spatialFind(
    user.organization,
    zoom,
    bllng,
    bllat,
    urlng,
    urlat,
    cats,
    photos ? true : false,
    surveys ? true : false
  );
  console.log(result);

  process.exit();
}

setTimeout(() => {
  main();
}, 100);
