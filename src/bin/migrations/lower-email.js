// Change all user's emails to lowercase.

const { toLower } = require("lodash");
require("../../helpers/mongoose");
const User = require("../../models/user");

async function main() {
  const users = await User.find();

  for (let i = 0; i < users.length; i++) {
    users[i].email = toLower(users[i].email);
    await users[i].save();
  }

  const updated = await User.find();
  console.log("---------------------------- SAVED -------------------------");
  console.log(updated);
}

main();
