const path = require('path');
process.env.NODE_CONFIG_DIR = path.resolve(__dirname, "../../../", "config");

require('../../helpers/mongoose');

const config = require('config');
const stripe = require('stripe')(config.stripeAPISecreKey);
const { get } = require('lodash');
const moment = require('moment');

const File = require('../../models/file');
const User = require('../../models/user');
const Project = require('../../models/project');
const Question = require('../../models/question');
const Organization = require('../../models/organization');

const mutate = process.env.MUTATE || false;

async function find(Model, ops = {}) {
  return new Promise((resolve, reject) => {
    Model.collection.find(ops, function(err, docs) {
      if (err) {
        reject(err);
      } else {
        resolve(docs.toArray());
      }
    });
  });
}

async function main() {
  const orgs = await find(Organization, {});

  for (let i = 0; i < orgs.length; i++) {
    const questions = await Question.find({
      organization: orgs[i]._id
    });
    for (let j = 0; j < questions.length; j++) {
      questions[j].set('order', j);
      const doc = await questions[j].save();
      console.log('saved', doc.toJSON());
    }
  }
}

main();
