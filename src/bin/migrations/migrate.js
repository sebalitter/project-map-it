/**
 * 1. Why do organizations not have an owner? Breaking problem.
 * 2. Why do organizations not have a stripe id? Breaking problem.
 * 3. Why do owners not have an organization? Breaking problem.
 */

require('../helpers/mongoose');

const config = require('config');
const stripe = require('stripe')(config.stripeAPISecreKey);
const { get } = require('lodash');
const moment = require('moment');

const File = require('../models/file');
const User = require('../models/user');
const Project = require('../models/project');
const Organization = require('../models/organization');

const mutate = process.env.MUTATE || false;

async function find(Model, ops = {}) {
  return new Promise((resolve, reject) => {
    Model.collection.find(ops, function(err, docs) {
      if (err) {
        reject(err);
      } else {
        resolve(docs.toArray());
      }
    });
  });
}

async function main() {

  // Owners without stripeId.
  // const owners = await User.find({});
  const orgs = await find(Organization, {});

  // Orgs without an owner.
  for (let i = 0; i < orgs.length; i++) {
    const org = orgs[i];
    const owners = await find(User, {
      organization: org._id,
      role: 1
    });

    console.log('CHECKING ORGANIZATION -----------------------------');

    if (!owners.length) {
      console.log(`${org._id.toString()} org does not have a owner.`);

      if (mutate) {
        console.log('Deleting them...');
        const doc = await Organization.findById(org._id);
        await doc.remove();
        continue;
      }
    }

    if (!org.stripeId) {
      console.log(`${org._id.toString()} org does not have a stripe id`);
    }
  }

  // Users without organizations (remove them).
  const users = await find(User, {});

  for (let i = 0; i < users.length; i++) {
    console.log('CHECKING USER -----------------------------');
    const org = await Organization.findById(users[i].organization);

    if (!org) {
      console.log(`${users[i]._id.toString()} does not have an organization (${users[i].email})`);

      if (mutate) {
        console.log('Deleting them...');
        const doc = await User.findById(users[i]._id);
        await doc.remove();
      }
    }
  }

  // Populate orgs with owner's stripeId's.
  // for (let i = 0; i < owners.length; i++) {
  //   const user = owners[i];
  //
  //   if (process.env.MUTATE) {
  //     const customer = await stripe.customers.create({
  //       email: user.email,
  //       description: `Customer: ${user.name} -- Organization: ${this.name}`,
  //       metadata: {
  //         full_name: user.name,
  //         first_name: nameSplit[0],
  //         last_name: nameSplit[1],
  //       }
  //     });
  //
  //     const subscription = await stripe.subscriptions.create({
  //       customer: customer.id,
  //       plan: 'basic',
  //       trial_end: moment().add(1, 'months').format('X'),
  //     });
  //
  //     await User.updateOne({ _id: user._id }, { $set: { stripeId: customer.id } });
  //   } else {
  //     console.log('WOULD UPDATE CUST ID FOR USER:', user.email);
  //   }
  // }

  process.exit();
}

main();
