const SUPERADMIN = 0;
const OWNER = 1;
const ADMIN = 2;
const EDITOR = 3;
const GUEST = 4;
const ANNONYMOUS = 999;

module.exports = {
  mongo: {
    name: "projectmapit_dev",
    host: "localhost"
  },
  mongoConnectionString: "mongodb://localhost/projectmapit_dev",
  demoId: "57a0770c96d146d42dcf5579",
  sysFromAddress: "noreply@projectmap.it",
  imageup: {
    host: "http://localhost",
    port: 31111
  },
  smtp: {
    port: 587,
    host: "smtp.mailgun.org",
    auth: {
      user: "postmaster@lev-interactive.com",
      pass: "1236ed233d1376f7bfba5e831a644a96"
    }
  },
  stripeAPISecreKey: "sk_test_cEZiAuE4mxEM1kxVES1xdrgQ",
  plans: {
    basic: 250,
    pro: 500,
    enterprise: 150000
  },
  redis: {
    host: "localhost"
  },
  appPort: 3000,
  url: "http://localhost:3000",
  googleServerApiKey: "AIzaSyBQiReTsahIMSUsx-PaOMuuX4aVpsCL56Q",
  gafbucksRecipients: "rafael@lev-interactive.com",
  roles: { SUPERADMIN, OWNER, ADMIN, EDITOR, GUEST, ANNONYMOUS },
  apiAuth: {
    crud: {
      users: [SUPERADMIN, OWNER, ADMIN, EDITOR, GUEST],
      subscriptions: OWNER,
      organizations: {
        GET: [SUPERADMIN, OWNER, ADMIN],
        PUT: [SUPERADMIN, OWNER, ADMIN]
      },
      removes: SUPERADMIN,
      projects: {
        GET: [SUPERADMIN, OWNER, ADMIN, EDITOR, GUEST, ANNONYMOUS],
        POST: [SUPERADMIN, OWNER, ADMIN, EDITOR],
        DELETE: [SUPERADMIN, OWNER, ADMIN, EDITOR],
        PUT: [SUPERADMIN, OWNER, ADMIN, EDITOR]
      },
      organizations: {
        GET: [SUPERADMIN, OWNER, ADMIN, EDITOR, GUEST, ANNONYMOUS],
        POST: [SUPERADMIN, OWNER, ADMIN, EDITOR],
        DELETE: [SUPERADMIN, OWNER, ADMIN, EDITOR],
        PUT: [SUPERADMIN, OWNER, ADMIN, EDITOR]
      },
      "project-categories": {
        GET: [SUPERADMIN, OWNER, ADMIN, EDITOR, GUEST, ANNONYMOUS],
        POST: [SUPERADMIN, OWNER, ADMIN, EDITOR],
        DELETE: [SUPERADMIN, OWNER, ADMIN, EDITOR],
        PUT: [SUPERADMIN, OWNER, ADMIN, EDITOR]
      },
      files: [SUPERADMIN, OWNER, ADMIN, EDITOR, GUEST],
      questions: EDITOR,
      account: GUEST,
      anwsers: { POST: ANNONYMOUS, GET: EDITOR }
    },
    static: {
      "/deactivate": { POST: OWNER }
    }
  }
};
